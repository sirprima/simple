<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rekam extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_user', 'id_caleg', 'tahun', 'keterangan'
    ];
}
