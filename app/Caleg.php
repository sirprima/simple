<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Caleg extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_user', 'id_partai', 'id_dapil', 'pemilihan', 'nama_caleg', 'tanggal_lahir', 'alamat', 'agama', 'status_perkawinan', 'pekerjaan', 'pendidikan', 'motivasi', 'sasaran', 'jenis_kelamin', 'foto'
    ];
}
