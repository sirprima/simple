<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dpt extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_user', 'id_dapil', 'id_tps', 'nik', 'nama_dpt', 'jenis_kelamin'
    ];
}
