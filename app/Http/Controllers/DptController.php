<?php

namespace App\Http\Controllers;

use Request as req;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use Auth;
use App\Dpt;
use App\Dapil;
use App\Tps;
use DB, Exception, Datatables, Response;
use App\Dpt as AppDpt;

class DptController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        if (req::segment(1) == 'dpt') { //cek url segment terakhir, jika url = dpt
            $this->middleware('auth'); //jalankan middleware auth jika url = dpt
        }
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (req::segment(1) == 'dpt') {
            return view('modul.dpt.admin'); //mengarahakan view ke directory modul/dpt dan membuka file admin.blade.php jika url = dpt
        }
        else {
            return view('modul.dpt.index'); //mengarahakan view ke directory modul/dpt dan membuka file index.blade.php jika url != dpt
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $dapils = Dapil::all(); //deklarasi variable dapils untuk menampung data semua dapil, fungsi all() digunakan untuk memanggil semua data dapil

        return view('modul.dpt.create', compact('dapils')); // mengarahkan view ke directory modul/dpt dan membuka file create.blade.php, dengan mengirimkan variable dapils
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [ //variable message berguna untuk menampung pesan error ketika melakukan kesalahan saat input data
            'dapil.required'                                    => 'Anda belum memilih Dapil',
            'tps.required'                                      => 'Anda belum memilih TPS',
            'nik.required'                                      => 'Anda belum mengisi NIK',
            'nama.required'                                     => 'Anda belum mengisi Nama',
            'jenis_kelamin.required'                            => 'Anda belum memilih Jenis Kelamin',
        ];
        $rules = [ //variable rules digunakan untuk menampul aturan pada setiap kolom input
            'dapil'                            => ['required', 'string', 'max:255'], //berarti = untuk kolom input dengan nama dapil harus diisi (required), dengan type data string dan maksimal karakter 225
            'tps'                              => ['required', 'string'],
            'nik'                              => ['required', 'string'],
            'nama'                             => ['required', 'string'],
            'jenis_kelamin'                    => ['required', 'string'],
        ];

        $validator = Validator::make($request->all(), $rules, $messages); //variable validator digunakan untuk memeriksa apakah input yang dimasukkan sesuai

        if ($validator->fails()) { //memeriksa apakah input yang dimasukkan benar atau salah berdasarkan auturan pada variable $rules
            return redirect('dpt/tambah') //jika fails, diarahkan kembali (redirect) ke directory dpt dan membuka file tambah.blade.php
                        ->withErrors($validator) //dengan menampilkan error sesuai dengan variable $message
                        ->withInput();
        }

        $dpt = Dpt::create([//digunakan untuk menyimpan data baru ke database
            'id_user'                   => Auth::id(), //sisi kiri merupakan nama kolom di database, sementara sisi kanan merupakan nama kolom input pada form input. command Auth::id() digunakan untuk mengambil id user yang melakukan login saat menginput data
            'id_dapil'                  => $request->dapil,
            'id_tps'                    => $request->tps,
            'nik'                       => $request->nik,
            'nama_dpt'                  => $request->nama,
            'jenis_kelamin'             => $request->jenis_kelamin,
        ]);

        return redirect('/dpt'); // mengarahakan kembali ke view dpt (dpt.blade.php)
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dapils = Dapil::all(); //digunakan untuk menampung data dapil secara keseluruhan
        $data = Dpt::find($id); //digunakan untuk menampung data dpt dengan id dpt = $id, find($id) adalah fungsi untuk mencari data yang sama dengan $id

        return view('modul.dpt.edit', compact('dapils', 'data')); //mengarahkan view ke directory modul/dpt dan membuka file edit.blade.php dan mengirim variable dapils dan data yang sudah di deklarasikan sebelumnya
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $messages = [ //penjelasan sama dengan fungsi store
            'dapil.required'                                    => 'Anda belum memilih Dapil',
            'tps.required'                                      => 'Anda belum memilih TPS',
            'nik.required'                                      => 'Anda belum mengisi NIK',
            'nama.required'                                     => 'Anda belum mengisi Nama',
            'jenis_kelamin.required'                            => 'Anda belum memilih Jenis Kelamin',
        ];
        $rules = [//penjelasan sama dengan fungsi store
            'dapil'                            => ['required', 'string', 'max:255'],
            'tps'                              => ['required', 'string'],
            'nik'                              => ['required', 'string'],
            'nama'                             => ['required', 'string'],
            'jenis_kelamin'                    => ['required', 'string'],
        ];

        $validator = Validator::make($request->all(), $rules, $messages);//penjelasan sama dengan fungsi store

        if ($validator->fails()) {//penjelasan sama dengan fungsi store
            return redirect('dpt/ubah/'.$id)
                        ->withErrors($validator)
                        ->withInput();
        }

        $dpt = Dpt::find($id); // digunakan untuk mencari data dpt yang id nya = $id untuk di update
        $dpt->id_user                   = Auth::id(); //penjelasan sama dengan fungsi store
        $dpt->id_dapil                  = $request->dapil;
        $dpt->id_tps                    = $request->tps;
        $dpt->nik                       = $request->nik;
        $dpt->nama_dpt                  = $request->nama;
        $dpt->jenis_kelamin             = $request->jenis_kelamin;
        $dpt->update(); //fungsi update() digunakan untuk mengupdate data yang sudah dicari sebelumnya pada saat deklarasi variable $dpt dengan data baru

        return redirect('/dpt'); //mengarahkan kembali ke url dpt
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $dpt = Dpt::find($id); //deklarasi variable untuk mendapatkan data dpt dengan id dpt = $id

        $dpt->forceDelete(); //fungsi yang digunakan untuk menghapus data

        return redirect('/dpt'); //mengarahkan kembali ke url dpt
    }

    public function cari($nik, $nama) { //fungsi yang dijalankan saat mencari data
        $data = Dpt::where('nik', $nik) //kondisi where
                    ->orWhere('nama_dpt', $nama) //kondisi atau
                    ->leftJoin('tpses', 'tpses.id', '=', 'dpts.id_tps') //melakukan left join table tpses ke table dpt
                    ->leftJoin('dapils', 'dapils.id', '=', 'tpses.id_dapil')
                    ->leftJoin('kecamatans', 'kecamatans.id', '=', 'dapils.kecamatan')
                    ->leftJoin('kelurahans', 'kelurahans.id', '=', 'dapils.kelurahan')
                    ->select('dpts.*', 'tpses.nama_tps as tps', 'kecamatans.nama_kecamatan as kecamatan', 'kelurahans.nama_kelurahan as kelurahan') //select beberapa field yang akan ditampilkan
                    ->first(); //mengambil data yang sesuai saja

        return Response::json($data); //mengirimkan data dengan format json ke view
    }

    public function getTps($id = 0) { //fungsi untuk mengambil data tps dengan parameter $id
        $tps['data'] = Tps::where('id_dapil', $id)->get(); //variable array untuk menampung data tps yang id_dapil = $id

        echo json_encode($tps); //mengirimkan kembali file json ke view
        exit;
    }

    public function getDetail($id = 0) { //fungsi yang digunakan untuk mengambil detail data dpt sesuai id
        $details = Dpt::where('dpts.id', $id)
                      ->leftJoin('users', 'users.id', '=', 'dpts.id_user')
                      ->leftJoin('tpses', 'tpses.id', '=', 'dpts.id_tps')
                      ->leftJoin('dapils', 'dapils.id', '=', 'tpses.id_dapil')
                      ->select('dpts.*', 'dapils.nama_dapil as dapil', 'tpses.nama_tps as tps')
                      ->orderBy('dpts.created_at', 'DESC')->first();

        return Response::json($details); //memparsing kembali data dpt pada variable $details ke view dengan format json
    }

    public function getDpts() { //digunakan untuk mendapatkan keseluruhan data dpt serta relasi dengan table lainnya
        $dpts = Dpt::leftJoin('users', 'users.id', '=', 'dpts.id_user')
                     ->leftJoin('tpses', 'tpses.id', '=', 'dpts.id_tps')
                     ->leftJoin('dapils', 'dapils.id', '=', 'tpses.id_dapil')
                     ->select('dpts.*', 'dapils.nama_dapil as dapil', 'tpses.nama_tps as tps')
                     ->orderBy('dpts.created_at', 'DESC'); //sql order

        return datatables()->of($dpts) //format default untuk penggunaan library datatable untuk menampilkan data $dpts pada table
                    ->editColumn('jenis_kelamin', function($dpt) { //mengedit kolom jenis_kelamin
                        if ($dpt->jenis_kelamin == '1') {
                            return 'Laki - Laki'; //jika jenis_kelamin = 1 maka tampilkan data sebagai Laki Laki
                        }
                        else {
                            return 'Perempuan'; //jika tidak tampilkan data sebagai perempuan
                        }
                    })
                    ->addColumn('aksi', function($dpt) { //menambahkan kolom aksi pada table
                        $detail = '<a href="#detail" data-id="'. $dpt->id .'" class="waves-effect waves-light modal-trigger btn bc-blue-bg btn-add tooltipped btn-detail" data-position="bottom" data-tooltip="Detail" style="margin-right: 10px"><i class="material-icons">visibility</i></a>'; //tombol detail
                        $edit = '<a href="/dpt/ubah/'. $dpt->id .'" class="waves-effect waves-light modal-trigger btn bc-green-bg btn-add tooltipped btn-detail" data-position="bottom" data-tooltip="Edit" style="margin-right: 10px"><i class="material-icons">edit</i></a>'; //tombol edit
                        $hapus = '<a href="/dpt/hapus/'. $dpt->id .'" class="waves-effect waves-light modal-trigger btn bc-red-bg btn-add tooltipped btn-detail" data-position="bottom" data-tooltip="Hapus"><i class="material-icons">delete</i></a>'; //tombol hapus

                        return $detail . $edit . $hapus;
                    })
                    ->addIndexColumn() //fungsi untuk menampilkan no urut pada table
                    ->rawColumns(['aksi'])
                    ->make(true);
    }
}
