<?php

namespace App\Http\Controllers;

use Request as req;
use Auth;
use App\Caleg;
use App\Partai;
use DB, Exception, Datatables, Response;

use Illuminate\Support\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Dapil;
use Illuminate\Support\Facades\File;

class CalegController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        if (req::segment(1) == 'caleg') {
            $this->middleware('auth');
        }
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (req::segment(1) == 'caleg') {
            return view('modul.caleg.admin');
        }
        else {
            $dapil = Dapil::all();
            $caleg = Caleg::all();
            return view('modul.caleg.index', compact('dapil', 'caleg'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $partai = Partai::all();
        $dapils = Dapil::all();
        return view('modul.caleg.create', compact('partai', 'dapils'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'partai.required'                        => 'Anda belum memilih Partai',
            'dapil.required'                         => 'Anda belum memilih Dapil',
            'pendidikan.required'                    => 'Anda belum mengisi Pendidikan',
            'nama_caleg.required'                    => 'Anda belum mengisi Nama',
            'pekerjaan.required'                     => 'Anda belum mengisi Pekerjaan',
            'jenis_kelamin.required'                 => 'Anda belum memilih Jenis Kelamin',
            'tanggal_lahir.required'                 => 'Anda belum memilih Tanggal Lahir',
            'motivasi.required'                      => 'Anda belum mengisi Motivasi',
            'agama.required'                         => 'Anda belum memilih Agama',
            'status_pernikahan.required'             => 'Anda belum memilih Status Pernikahan',
            'sasaran.required'                       => 'Anda belum mengisi Sasaran',
            'alamat.required'                        => 'Anda belum mengisi Alamat',
            'foto.required'                          => 'Anda belum mengisi Alamat',
            'pemilihan.required'                     => 'Anda belum memilih Pemilihan',
        ];
        $rules = [
            'partai'                            => ['required', 'integer'],
            'dapil'                             => ['required', 'string'],
            'pendidikan'                        => ['required', 'string'],
            'nama_caleg'                        => ['required', 'string'],
            'pekerjaan'                         => ['required', 'string'],
            'jenis_kelamin'                     => ['required', 'integer'],
            'tanggal_lahir'                     => ['required', 'string'],
            'motivasi'                          => ['required', 'string'],
            'agama'                             => ['required', 'string'],
            'status_pernikahan'                 => ['required', 'string'],
            'sasaran'                           => ['required', 'string'],
            'alamat'                            => ['required', 'string'],
            'pemilihan'                         => ['required', 'string'],
            'foto'                              => ['required', 'image', 'mimes:jpeg,png,jpg,gif,svg', 'max:2048'],
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect('caleg/tambah')
                        ->withErrors($validator)
                        ->withInput();
        }

        $foto = $request->foto;
        if ($foto->isValid()) {
            $originalName = $foto->getClientOriginalName();
            $name = 'FOTO-CALEG-' . Carbon::now()->format('dmy') . rand(100, 900) . '.' . $foto->getClientOriginalExtension();
            $destination = 'assets/images/content/';
            $foto->move($destination, $name);
        }

        $caleg = Caleg::create([
            'id_user'               => Auth::id(),
            'id_partai'             => $request->partai,
            'id_dapil'              => $request->dapil,
            'nama_caleg'            => $request->nama_caleg,
            'tanggal_lahir'         => Carbon::parse($request->tanggal_lahir),
            'jenis_kelamin'         => $request->jenis_kelamin,
            'alamat'                => $request->alamat,
            'agama'                 => $request->agama,
            'status_perkawinan'     => $request->status_pernikahan,
            'pekerjaan'             => $request->pekerjaan,
            'pendidikan'            => $request->pendidikan,
            'motivasi'              => $request->motivasi,
            'sasaran'               => $request->sasaran,
            'foto'                  => $name,
            'pemilihan'             => $request->pemilihan,
        ]);

        return redirect('/caleg');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('modul.caleg.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $partai = Partai::all();
        $data = Caleg::find($id);
        $dapils = Dapil::all();
        return view('modul.caleg.edit', compact('partai', 'data', 'dapils'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $messages = [
            'partai.required'                        => 'Anda belum memilih Partai',
            'dapil.required'                         => 'Anda belum memilih Dapil',
            'pendidikan.required'                    => 'Anda belum mengisi Pendidikan',
            'nama_caleg.required'                    => 'Anda belum mengisi Nama',
            'pekerjaan.required'                     => 'Anda belum mengisi Pekerjaan',
            'jenis_kelamin.required'                 => 'Anda belum memilih Jenis Kelamin',
            'tanggal_lahir.required'                 => 'Anda belum memilih Tanggal Lahir',
            'motivasi.required'                      => 'Anda belum mengisi Motivasi',
            'agama.required'                         => 'Anda belum memilih Agama',
            'status_pernikahan.required'             => 'Anda belum memilih Status Pernikahan',
            'sasaran.required'                       => 'Anda belum mengisi Sasaran',
            'alamat.required'                        => 'Anda belum mengisi Alamat',
            'foto.required'                          => 'Anda belum mengisi Alamat',
            'pemilihan.required'                     => 'Anda belum memilih Pemilihan',
        ];
        $rules = [
            'partai'                            => ['required', 'integer'],
            'dapil'                             => ['required', 'integer'],
            'pendidikan'                        => ['required', 'string'],
            'nama_caleg'                        => ['required', 'string'],
            'pekerjaan'                         => ['required', 'string'],
            'jenis_kelamin'                     => ['required', 'integer'],
            'tanggal_lahir'                     => ['required', 'string'],
            'motivasi'                          => ['required', 'string'],
            'agama'                             => ['required', 'string'],
            'status_pernikahan'                 => ['required', 'string'],
            'sasaran'                           => ['required', 'string'],
            'alamat'                            => ['required', 'string'],
            'pemilihan'                         => ['required', 'string'],
            'foto'                              => ['required', 'image', 'mimes:jpeg,png,jpg,gif,svg', 'max:2048'],
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect('caleg/edit')
                        ->withErrors($validator)
                        ->withInput();
        }

        $foto = $request->foto;
        if ($foto) {
            $caleg = Caleg::find($id);

            File::delete('assets/images/content/'.$caleg->foto);
            if ($foto->isValid()) {
                $originalName = $foto->getClientOriginalName();
                $name = 'FOTO-CALEG-' . Carbon::now()->format('dmy') . rand(100, 900) . '.' . $foto->getClientOriginalExtension();
                $destination = 'assets/images/content/';
                $foto->move($destination, $name);
            }
        }

        $caleg = Caleg::find($id);
        $caleg->id_user               = Auth::id();
        $caleg->id_partai             = $request->partai;
        $caleg->id_dapil              = $request->dapil;
        $caleg->nama_caleg            = $request->nama_caleg;
        $caleg->tanggal_lahir         = Carbon::parse($request->tanggal_lahir);
        $caleg->jenis_kelamin         = $request->jenis_kelamin;
        $caleg->alamat                = $request->alamat;
        $caleg->agama                 = $request->agama;
        $caleg->status_perkawinan     = $request->status_pernikahan;
        $caleg->pekerjaan             = $request->pekerjaan;
        $caleg->pendidikan            = $request->pendidikan;
        $caleg->motivasi              = $request->motivasi;
        $caleg->sasaran               = $request->sasaran;
        $caleg->permilihan            = $request->permilihan;
        if ($request->foto) {
            $caleg->foto    = $name;
        }
        $caleg->update();

        return redirect('/caleg');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $caleg = Caleg::find($id);

        $caleg->forceDelete();

        return redirect('/caleg');
    }

    public function cari($provinsi, $dapil, $pemilihan) {
        // DB::enableQueryLog();
        $data = Caleg::leftJoin('users', 'users.id', '=', 'calegs.id_user')
                    ->where(function ($query) use($dapil) {
                        if ($dapil != 'null') {
                            $query->where('calegs.id_dapil', $dapil);
                        }
                    })
                    ->where(function ($query) use($provinsi) {
                        if ($provinsi != 'null') {
                            $query->where('dapils.provinsi', $provinsi);
                        }
                    })
                    ->where(function ($query) use($pemilihan) {
                        if ($pemilihan != 'null') {
                            $query->where('calegs.pemilihan', $pemilihan);
                        }
                    })
                    ->leftJoin('partais', 'partais.id', '=', 'calegs.id_partai')
                    ->leftJoin('dapils', 'dapils.id', '=', 'calegs.id_dapil')
                    ->select('calegs.*', 'partais.nama_partai as partai', 'dapils.nama_dapil as dapil')
                    ->orderBy('calegs.created_at', 'DESC')->get();

        return Response::json($data);
    }

    public function getDetail($id = 0) {
        $details = Caleg::where('calegs.id', $id)
                        ->leftJoin('users', 'users.id', '=', 'calegs.id_user')
                        ->leftJoin('partais', 'partais.id', '=', 'calegs.id_partai')
                        ->leftJoin('dapils', 'dapils.id', '=', 'calegs.id_dapil')
                        ->select('calegs.*', 'partais.nama_partai as partai', 'dapils.nama_dapil as dapil')
                        ->orderBy('calegs.created_at', 'DESC')->first();

        return Response::json($details);
    }

    public function getCalegs() {
        $calegs = Caleg::leftJoin('users', 'users.id', '=', 'calegs.id_user')
                        ->leftJoin('partais', 'partais.id', '=', 'calegs.id_partai')
                        ->leftJoin('dapils', 'dapils.id', '=', 'calegs.id_dapil')
                        ->select('calegs.*', 'partais.nama_partai as partai', 'dapils.nama_dapil as dapil')
                        ->orderBy('calegs.created_at', 'DESC');

        return datatables()->of($calegs)
                    ->editColumn('jenis_kelamin', function($caleg) {
                        if ($caleg->jenis_kelamin == 1) {
                            return 'Laki - Laki';
                        }
                        else {
                            return 'Perempuan';
                        }
                    })
                    ->editColumn('alamat', function($caleg) {
                        $alamat = substr($caleg->alamat, 0, 100) . '....';

                        return $alamat;
                    })
                    ->editColumn('motivasi', function($caleg) {
                        $motivasi = substr($caleg->motivasi, 0, 100) . '....';

                        return $motivasi;
                    })
                    ->editColumn('sasaran', function($caleg) {
                        $sasaran = substr($caleg->sasaran, 0, 100) . '....';

                        return $sasaran;
                    })
                    ->editColumn('status_perkawinan', function($caleg) {
                        if ($caleg->status_perkawinan == 'lajang') {
                            return 'Lajang';
                        }
                        else {
                            return 'Menikah';
                        }
                    })
                    ->editColumn('tanggal_lahir', function($caleg) {
                        return Carbon::parse($caleg->tanggal_lahir)->format('d-m-Y');
                    })
                    ->addColumn('aksi', function($caleg) {
                        $detail = '<a href="#detail" data-id="'. $caleg->id .'" class="waves-effect waves-light modal-trigger btn bc-blue-bg btn-add tooltipped btn-detail" data-position="bottom" data-tooltip="Detail" style="margin-right: 10px"><i class="material-icons">visibility</i></a>';
                        $edit = '<a href="/caleg/ubah/'. $caleg->id .'" class="waves-effect waves-light modal-trigger btn bc-green-bg btn-add tooltipped btn-detail" data-position="bottom" data-tooltip="Edit" style="margin-right: 10px"><i class="material-icons">edit</i></a>';
                        $hapus = '<a href="/caleg/hapus/'. $caleg->id .'" class="waves-effect waves-light modal-trigger btn bc-red-bg btn-add tooltipped btn-detail" data-position="bottom" data-tooltip="Hapus"><i class="material-icons">delete</i></a>';

                        return $detail . $edit . $hapus;
                    })
                    ->addIndexColumn()
                    ->rawColumns(['aksi', 'jenis_kelamin', 'status_perkawinan'])
                    ->make(true);
    }
}
