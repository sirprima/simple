<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use Auth;
use App\Dapil;
use App\Kecamatan;
use App\Kelurahan;
use DB, Exception, Datatables, Response;

class DapilController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('modul.dapil.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kecamatans = Kecamatan::all();
        return view('modul.dapil.create', compact('kecamatans'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'nama_dapil.required'                                   => 'Anda belum mengisi Nama Dapil',
            'provinsi.required'                                     => 'Anda belum memilih Provinsi',
            'kota.required'                                         => 'Anda belum memilih Kabupaten/Kota',
            'kecamatan.required'                                    => 'Anda belum memilih Kecamatan',
            'kelurahan.required'                                    => 'Anda belum memilih Kelurahan',
        ];
        $rules = [
            'nama_dapil'                            => ['required', 'string', 'max:255'],
            'provinsi'                              => ['required', 'string'],
            'kota'                                  => ['required', 'string'],
            'kecamatan'                             => ['required', 'string'],
            'kelurahan'                             => ['required', 'string'],
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect('dapil/tambah')
                        ->withErrors($validator)
                        ->withInput();
        }

        $dapil = Dapil::create([
            'id_user'               => Auth::id(),
            'nama_dapil'            => $request->nama_dapil,
            'provinsi'              => $request->provinsi,
            'kabupaten_kota'        => $request->kota,
            'kecamatan'             => $request->kecamatan,
            'kelurahan'             => $request->kelurahan,
        ]);

        return redirect('/dapil');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kecamatans = Kecamatan::all();
        $data = Dapil::find($id);
        return view('modul.dapil.edit', compact('kecamatans', 'data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $messages = [
            'nama_dapil.required'                                   => 'Anda belum mengisi Nama Dapil',
            'provinsi.required'                                     => 'Anda belum memilih Provinsi',
            'kota.required'                                         => 'Anda belum memilih Kabupaten/Kota',
            'kecamatan.required'                                    => 'Anda belum memilih Kecamatan',
            'kelurahan.required'                                    => 'Anda belum memilih Kelurahan',
        ];
        $rules = [
            'nama_dapil'                            => ['required', 'string', 'max:255'],
            'provinsi'                              => ['required', 'string'],
            'kota'                                  => ['required', 'string'],
            'kecamatan'                             => ['required', 'string'],
            'kelurahan'                             => ['required', 'string'],
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect('dapil/ubah/'.$id)
                        ->withErrors($validator)
                        ->withInput();
        }

        $dapil = Dapil::find($id);
        $dapil->id_user               = Auth::id();
        $dapil->nama_dapil            = $request->nama_dapil;
        $dapil->provinsi              = $request->provinsi;
        $dapil->kabupaten_kota        = $request->kota;
        $dapil->kecamatan             = $request->kecamatan;
        $dapil->kelurahan             = $request->kelurahan;
        $dapil->update();

        return redirect('/dapil');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $dapil = Dapil::find($id);

        $dapil->forceDelete();

        return redirect('/dapil');
    }

    public function getKelurahans($id = 0) {
        $kelurahans['data'] = Kelurahan::where('id_kecamatan', $id)->get();

        echo json_encode($kelurahans);
        exit;
    }

    public function getDetail($id = 0) {
        $details = Dapil::where('dapils.id', $id)
                      ->leftJoin('users', 'users.id', '=', 'dapils.id_user')
                      ->leftJoin('kecamatans', 'kecamatans.id', '=', 'dapils.kecamatan')
                      ->leftJoin('kelurahans', 'kelurahans.id', '=', 'dapils.kelurahan')
                      ->select('dapils.*', 'kelurahans.nama_kelurahan as nama_kelurahan', 'kecamatans.nama_kecamatan as nama_kecamatan')
                      ->orderBy('dapils.created_at', 'DESC')->first();

        return Response::json($details);
    }

    public function getDapils() {
        $dapils = Dapil::leftJoin('users', 'users.id', '=', 'dapils.id_user')
                       ->leftJoin('kecamatans', 'kecamatans.id', '=', 'dapils.kecamatan')
                       ->leftJoin('kelurahans', 'kelurahans.id', '=', 'dapils.kelurahan')
                       ->select('dapils.*', 'kelurahans.nama_kelurahan as nama_kelurahan', 'kecamatans.nama_kecamatan as nama_kecamatan')
                       ->orderBy('dapils.created_at', 'DESC');

        return datatables()->of($dapils)
                    ->editColumn('provinsi', function($dapil) {
                        if ($dapil->provinsi == '1') {
                            return 'Sumatera Utara';
                        }
                    })
                    ->editColumn('kabupaten_kota', function($dapil) {
                        if ($dapil->kabupaten_kota == '1') {
                            return 'Medan';
                        }
                    })
                    ->addColumn('aksi', function($dapil) {
                        $detail = '<a href="#detail" data-id="'. $dapil->id .'" class="waves-effect waves-light modal-trigger btn bc-blue-bg btn-add tooltipped btn-detail" data-position="bottom" data-tooltip="Detail" style="margin-right: 10px"><i class="material-icons">visibility</i></a>';
                        $edit = '<a href="/dapil/ubah/'. $dapil->id .'" class="waves-effect waves-light modal-trigger btn bc-green-bg btn-add tooltipped btn-detail" data-position="bottom" data-tooltip="Edit" style="margin-right: 10px"><i class="material-icons">edit</i></a>';
                        $hapus = '<a href="/dapil/hapus/'. $dapil->id .'" class="waves-effect waves-light modal-trigger btn bc-red-bg btn-add tooltipped btn-detail" data-position="bottom" data-tooltip="Hapus"><i class="material-icons">delete</i></a>';

                        return $detail . $edit . $hapus;
                    })
                    ->addIndexColumn()
                    ->rawColumns(['aksi', 'provinsi', 'kabupaten_kota'])
                    ->make(true);
    }
}
