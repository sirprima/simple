<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;

use App\User;
use DB, Exception;
use Illuminate\Support\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Validation\Rule;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo;

    protected function redirectTo()
    {
        /* generate URL dynamicaly */
        session()->flash('success', 'Anda sudah berhasil mendaftarkan diri.');
        return '/login'; // return dynamicaly generated URL.
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {

        return view('auth.register');
    }

    /** add by brocyber
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        return $this->registered($request, $user)
                        ?: redirect($this->redirectPath());

    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'nama_user'                 => ['required', 'string', 'max:255'],
            'agama'                     => ['required', 'string', 'max:255'],
            'tempat_lahir'              => ['required', 'string', 'max:255'],
            'status_pernikahan'         => ['required', 'string', 'max:255'],
            'tanggal_lahir'             => ['required', 'string', 'max:255'],
            'pekerjaan'                 => ['required', 'string', 'max:255'],
            'jenis_kelamin'             => ['required', 'string', 'max:255'],
            'ktp'                       => ['required', 'image', 'mimes:jpeg,png,jpg,gif,svg', 'max:2048'],
            'alamat'                    => ['required', 'string'],
            'email'                     => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'username'                  => ['required', 'string', 'max:255'],
            'password'                  => ['required', 'string', 'confirmed'],
            'password_confirmation'     => ['required', 'string'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $pic = $data['ktp'];
        if ($pic->isValid()) {
            $originalName = $pic->getClientOriginalName();
            $name = 'KTP' . Carbon::now()->format('dmy') . rand(100, 900) . '.' . $pic->getClientOriginalExtension();
            $destination = 'assets/images/content/';
            $pic->move($destination, $name);

            $user = User::create([
                'nama_user' => $data['nama_user'],
                'agama' => $data['agama'],
                'tempat_lahir' => $data['tempat_lahir'],
                'status_pernikahan' => $data['status_pernikahan'],
                'tanggal_lahir' => Carbon::parse($data['tanggal_lahir']),
                'pekerjaan' => $data['pekerjaan'],
                'jenis_kelamin' => $data['jenis_kelamin'],
                'ktp' => $destination . $name,
                'alamat' => $data['alamat'],
                'level_user' => 'visitor',
                'email' => $data['email'],
                'username' => $data['username'],
                'password' => Hash::make($data['password']),
            ]);
        }

        return $user;
    }
}
