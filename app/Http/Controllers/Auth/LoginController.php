<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
// adding by brocyber
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo;

    protected function redirectTo()
    {
        if (Auth::check()) {
            if (Auth::user()->level_user == 'visitor') {
                $this->redirectTo = '/lapor';
            }
            else if (Auth::user()->level_user == 'admin') {
                $this->redirectTo = '/caleg';
            }
            else if (Auth::user()->level_user == 'kpps') {
                $this->redirectTo = '/lapor';
            }
            else if (Auth::user()->level_user == 'saksi') {
                $this->redirectTo = '/lapor';
            }
        }
        else {
            $this->redirectTo = '/info-dpt';
        }

        return $this->redirectTo;
    }

    /**
     * Login username to be used by the controller.
     * add by brocyber
     * @var string
     */
    protected $username;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['guest', 'visitor'])->except('logout');
    }

    // adding by brocyber
    /**
     * Get the needed authorization credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function credentials(Request $request)
    {
        $field = filter_var($request->get($this->username()), FILTER_VALIDATE_EMAIL)
            ? $this->username()
            : 'username';
        return [
            $field => $request->get($this->username()),
            'password' => $request->password,
        ];
    }
    // end
}
