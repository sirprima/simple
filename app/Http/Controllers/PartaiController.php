<?php

namespace App\Http\Controllers;

use Auth;
use App\Partai;
use DB, Exception, Datatables, Response;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;

class PartaiController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('modul.partai.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('modul.partai.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'no_urut.required'                              => 'Anda belum mengisi Nomor Urut',
            'nama.required'                                 => 'Anda belum mengisi Nama Partai',
            'lambang.required'                              => 'Anda belum Upload Lambang Partai',
            'alamat.required'                               => 'Anda belum mengisi Alamat',
            'visi.required'                                 => 'Anda belum mengisi Visi',
            'misi.required'                                 => 'Anda belum mengisi Misi'
        ];
        $rules = [
            'no_urut'                           => ['required', 'integer', 'max:255'],
            'nama'                              => ['required', 'string'],
            'lambang'                           => ['required', 'image', 'mimes:jpeg,png,jpg,gif,svg', 'max:2048'],
            'alamat'                            => ['required', 'string'],
            'visi'                              => ['required', 'string'],
            'misi'                              => ['required', 'string'],
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect('partai/tambah')
                        ->withErrors($validator)
                        ->withInput();
        }

        $lambang = $request->lambang;
        if ($lambang->isValid()) {
            $originalName = $lambang->getClientOriginalName();
            $name = 'LAMBANG-PARTAI-' . Carbon::now()->format('dmy') . rand(100, 900) . '.' . $lambang->getClientOriginalExtension();
            $destination = 'assets/images/content/';
            $lambang->move($destination, $name);
        }

        $partai = Partai::create([
            'id_user'           => Auth::id(),
            'no_urut'           => $request->no_urut,
            'nama_partai'       => $request->nama,
            'lambang_partai'    => $name,
            'alamat'            => $request->alamat,
            'visi'              => $request->visi,
            'misi'              => $request->misi
        ]);

        return redirect('/partai');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Partai::find($id);
        return view('modul.partai.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $messages = [
            'no_urut.required'                              => 'Anda belum mengisi Nomor Urut',
            'nama.required'                                 => 'Anda belum mengisi Nama Partai',
            // 'lambang.required'                              => 'Anda belum Upload Lambang Partai',
            'alamat.required'                               => 'Anda belum mengisi Alamat',
            'visi.required'                                 => 'Anda belum mengisi Visi',
            'misi.required'                                 => 'Anda belum mengisi Misi'
        ];
        $rules = [
            'no_urut'                           => ['required', 'integer', 'max:255'],
            'nama'                              => ['required', 'string'],
            // 'lambang'                           => ['required', 'image', 'mimes:jpeg,png,jpg,gif,svg', 'max:2048'],
            'alamat'                            => ['required', 'string'],
            'visi'                              => ['required', 'string'],
            'misi'                              => ['required', 'string'],
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect('partai/ubah/'.$id)
                        ->withErrors($validator)
                        ->withInput();
        }

        $lambang = $request->lambang;
        if ($lambang) {
            $partai = Partai::find($id);

            File::delete('assets/images/content/'.$partai->lambang_partai);
            if ($lambang->isValid()) {
                $originalName = $lambang->getClientOriginalName();
                $name = 'LAMBANG-PARTAI-' . Carbon::now()->format('dmy') . rand(100, 900) . '.' . $lambang->getClientOriginalExtension();
                $destination = 'assets/images/content/';
                $lambang->move($destination, $name);
            }
        }

        $partai = Partai::find($id);
        $partai->id_user           = Auth::id();
        $partai->no_urut           = $request->no_urut;
        $partai->nama_partai       = $request->nama;
        if ($request->lambang) {
            $partai->lambang_partai    = $name;
        }
        $partai->alamat            = $request->alamat;
        $partai->visi              = $request->visi;
        $partai->misi              = $request->misi;
        $partai->update();

        return redirect('/partai');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $partai = Partai::find($id);

        File::delete('assets/images/content/'.$partai->lambang_partai);

        $partai->forceDelete();

        return redirect('/partai');
    }

    public function getDetail($id = 0) {
        $details = Partai::where('partais.id', $id)
                      ->leftJoin('users', 'users.id', '=', 'partais.id_user')
                      ->select('partais.*')
                      ->orderBy('partais.created_at', 'DESC')->first();

        return Response::json($details);
    }

    public function getPartais() {
        $partais = Partai::leftJoin('users', 'users.id', '=', 'partais.id_user')
                     ->select('partais.*')
                     ->orderBy('partais.created_at', 'DESC');

        return datatables()->of($partais)
                    ->editColumn('lambang_partai', function($partai) {
                        $lambang = '<img src="assets/images/content/'. $partai->lambang_partai .'" alt="" width="100px">';

                        return $lambang;
                    })
                    ->editColumn('visi', function($partai) {
                        $visi = substr($partai->visi, 0, 100) . '....';

                        return $visi;
                    })
                    ->editColumn('misi', function($partai) {
                        $visi = substr($partai->misi, 0, 100) . '....';

                        return $visi;
                    })
                    ->addColumn('aksi', function($partai) {
                        $detail = '<a href="#detail" data-id="'. $partai->id .'" class="waves-effect waves-light modal-trigger btn bc-blue-bg btn-add tooltipped btn-detail" data-position="bottom" data-tooltip="Detail" style="margin-right: 10px"><i class="material-icons">visibility</i></a>';
                        $edit = '<a href="/partai/ubah/'. $partai->id .'" class="waves-effect waves-light modal-trigger btn bc-green-bg btn-add tooltipped btn-detail" data-position="bottom" data-tooltip="Edit" style="margin-right: 10px"><i class="material-icons">edit</i></a>';
                        $hapus = '<a href="/partai/hapus/'. $partai->id .'" class="waves-effect waves-light modal-trigger btn bc-red-bg btn-add tooltipped btn-detail" data-position="bottom" data-tooltip="Hapus"><i class="material-icons">delete</i></a>';

                        return $detail . $edit . $hapus;
                    })
                    ->addIndexColumn()
                    ->rawColumns(['aksi', 'lambang_partai'])
                    ->make(true);
    }
}
