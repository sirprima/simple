<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use Auth;
use App\User;
use DB, Exception, Datatables, Response;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('modul.user.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('modul.user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'level_user.required'                               => 'Anda belum memilih Level User',
            'nama_user.required'                                => 'Anda belum mengisi Nama User',
            'email.required'                                    => 'Anda belum mengisi Email',
            'username.required'                                 => 'Anda belum mengisi Username',
            'password.required'                                 => 'Anda belum mengisi password',
        ];
        $rules = [
            'level_user'                            => ['required', 'string'],
            'nama_user'                             => ['required', 'string', 'max:255'],
            'email'                                 => ['required', 'string'],
            'username'                              => ['required', 'string'],
            'password'                              => ['required', 'string'],
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect('user/tambah')
                        ->withErrors($validator)
                        ->withInput();
        }

        $user = User::create([
            'level_user'            => $request->level_user,
            'nama_user'             => $request->nama_user,
            'email'                 => $request->email,
            'username'              => $request->username,
            'password'              => Hash::make($request->password),
        ]);

        return redirect('/user');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = User::find($id);
        return view('modul.user.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $messages = [
            'level_user.required'                               => 'Anda belum memilih Level User',
            'nama_user.required'                                => 'Anda belum mengisi Nama User',
            'email.required'                                    => 'Anda belum mengisi Email',
            'username.required'                                 => 'Anda belum mengisi Username',
        ];
        $rules = [
            'level_user'                            => ['required', 'string'],
            'nama_user'                             => ['required', 'string', 'max:255'],
            'email'                                 => ['required', 'string'],
            'username'                              => ['required', 'string'],
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect('user/edit')
                        ->withErrors($validator)
                        ->withInput();
        }

        $user = User::find($id);
        $user->level_user            = $request->level_user;
        $user->nama_user             = $request->nama_user;
        $user->email                 = $request->email;
        $user->username              = $request->username;
        $user->update();

        return redirect('/user');
    }

    public function password(Request $request)
    {

        $user = User::find($request->id);
        $user->password              = Hash::make($request->password);
        $user->update();

        return redirect('/user');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);

        $user->forceDelete();

        return redirect('/user');
    }

    public function getDetail($id = 0) {
        $details = User::select('*')
                       ->orderBy('created_at', 'DESC')->first();

        return Response::json($details);
    }

    public function getUsers() {
        $users = User::select('*')
                     ->orderBy('created_at', 'DESC');

        return datatables()->of($users)
                    ->addColumn('aksi', function($user) {
                        $detail = '<a href="#detail" data-id="'. $user->id .'" class="waves-effect waves-light modal-trigger btn bc-blue-bg btn-add tooltipped btn-detail" data-position="bottom" data-tooltip="Detail" style="margin-right: 10px"><i class="material-icons">visibility</i></a>';
                        $pass = '<a href="#password" data-id="'. $user->id .'" class="waves-effect waves-light modal-trigger btn bc-yellow-bg btn-add tooltipped btn-password" data-position="bottom" data-tooltip="Ganti Password" style="margin-right: 10px"><i class="material-icons">vpn_key</i></a>';
                        $edit = '<a href="/user/ubah/'. $user->id .'" class="waves-effect waves-light modal-trigger btn bc-green-bg btn-add tooltipped btn-detail" data-position="bottom" data-tooltip="Edit" style="margin-right: 10px"><i class="material-icons">edit</i></a>';
                        $hapus = '<a href="/user/hapus/'. $user->id .'" class="waves-effect waves-light modal-trigger btn bc-red-bg btn-add tooltipped btn-detail" data-position="bottom" data-tooltip="Hapus"><i class="material-icons">delete</i></a>';

                        return $detail . $pass . $edit . $hapus;
                    })
                    ->addIndexColumn()
                    ->rawColumns(['aksi'])
                    ->make(true);
    }
}
