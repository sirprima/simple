<?php

namespace App\Http\Controllers;

use Request as req;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use Auth;
use App\Rekam;
use App\Caleg;
use DB, Exception, Datatables, Response;

class RekamController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('modul.rekam.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $caleg = Caleg::all();
        return view('modul.rekam.create', compact('caleg'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'caleg.required'                                    => 'Anda belum memilih Caleg',
            'start_year.required'                               => 'Anda belum mengisi Tahun Awal',
            'end_year.required'                                 => 'Anda belum mengisi Tahun Akhir',
            'keterangan.required'                               => 'Anda belum mengisi Keterangan',
        ];
        $rules = [
            'caleg'                                 => ['required', 'string', 'max:255'],
            'start_year'                            => ['required', 'string'],
            'end_year'                              => ['required', 'string'],
            'keterangan'                            => ['required', 'string'],
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect('rekam-jejak/tambah')
                        ->withErrors($validator)
                        ->withInput();
        }

        $rekam = Rekam::create([
            'id_user'               => Auth::id(),
            'id_caleg'              => $request->caleg,
            'tahun'                 => $request->start_year. ' - ' .$request->end_year,
            'keterangan'            => $request->keterangan,
        ]);

        return redirect('/rekam-jejak');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $caleg = Caleg::all();

        $data = Rekam::find($id);

        $start_year = substr($data->tahun, 0, 4);
        $end_year = substr($data->tahun, 7, 4);

        return view('modul.rekam.edit', compact('caleg', 'data', 'start_year', 'end_year'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $messages = [
            'caleg.required'                                    => 'Anda belum memilih Caleg',
            'start_year.required'                               => 'Anda belum mengisi Tahun Awal',
            'end_year.required'                                 => 'Anda belum mengisi Tahun Akhir',
            'keterangan.required'                               => 'Anda belum mengisi Keterangan',
        ];
        $rules = [
            'caleg'                                 => ['required', 'string', 'max:255'],
            'start_year'                            => ['required', 'string'],
            'end_year'                              => ['required', 'string'],
            'keterangan'                            => ['required', 'string'],
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect('rekam-jejak/edit')
                        ->withErrors($validator)
                        ->withInput();
        }

        $rekam = Rekam::find($id);
        $rekam->id_user               = Auth::id();
        $rekam->id_caleg              = $request->caleg;
        $rekam->tahun                 = $request->start_year. ' - ' .$request->end_year;
        $rekam->keterangan            = $request->keterangan;
        $rekam->update();

        return redirect('/rekam-jejak');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $rekam = Rekam::find($id);

        $rekam->forceDelete();

        return redirect('/rekam-jejak');
    }

    public function getDetail($id = 0) {
        $details = Rekam::where('rekams.id', $id)
                      ->leftJoin('users', 'users.id', '=', 'rekams.id_user')
                      ->leftJoin('calegs', 'calegs.id', '=', 'rekams.id_caleg')
                      ->select('rekams.*', 'calegs.nama_caleg as caleg')
                      ->orderBy('rekams.created_at', 'DESC')->first();

        return Response::json($details);
    }

    public function getRekams() {
        $rekams = Rekam::leftJoin('users', 'users.id', '=', 'rekams.id_user')
                     ->leftJoin('calegs', 'calegs.id', '=', 'rekams.id_caleg')
                     ->select('rekams.*', 'calegs.nama_caleg as caleg')
                     ->orderBy('rekams.created_at', 'DESC');

        return datatables()->of($rekams)
                    ->editColumn('keterangan', function($rekam) {
                        $keterangan = substr($rekam->keterangan, 0, 100) . '....';

                        return $keterangan;
                    })
                    ->addColumn('aksi', function($rekam) {
                        $detail = '<a href="#detail" data-id="'. $rekam->id .'" class="waves-effect waves-light modal-trigger btn bc-blue-bg btn-add tooltipped btn-detail" data-position="bottom" data-tooltip="Detail" style="margin-right: 10px"><i class="material-icons">visibility</i></a>';
                        $edit = '<a href="/rekam-jejak/ubah/'. $rekam->id .'" class="waves-effect waves-light modal-trigger btn bc-green-bg btn-add tooltipped btn-detail" data-position="bottom" data-tooltip="Edit" style="margin-right: 10px"><i class="material-icons">edit</i></a>';
                        $hapus = '<a href="/rekam-jejak/hapus/'. $rekam->id .'" class="waves-effect waves-light modal-trigger btn bc-red-bg btn-add tooltipped btn-detail" data-position="bottom" data-tooltip="Hapus"><i class="material-icons">delete</i></a>';

                        return $detail . $edit . $hapus;
                    })
                    ->addIndexColumn()
                    ->rawColumns(['aksi'])
                    ->make(true);
    }
}
