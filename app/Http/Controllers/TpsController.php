<?php

namespace App\Http\Controllers;

use Request as req;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use Auth;
use App\Tps;
use App\Dapil;
use Mapper;
use DB, Exception, Datatables, Response;
use FarhanWazir\GoogleMaps\GMaps;
use App\Kecamatan;
use App\Kelurahan;

class TpsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

        if (req::segment(1) == 'tps') {
            $this->middleware('auth');
        }
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kecamatans = Kecamatan::all();
        $kelurahans = Kelurahan::all();
        if (req::segment(1) == 'tps') {
            return view('modul.tps.admin');
        }
        else {
            $config['center'] = '3.595196, 98.672226'; //Medan
            // $config['center'] = 'auto';
            $config['zoom'] = '13';
            $config['map_height'] = '100%';

            $config['onboundschanged'] = 'if (!centreGot) {
                var mapCentre = map.getCenter();
                marker_0.setOptions({
                    position: new google.maps.LatLng(mapCentre.lat(), mapCentre.lng())
                });
            }
            centreGot = true;

            createMarker_map({ map: map, position:new google.maps.LatLng()});
            $("#lat").val(map.getCenter().lat());
            $("#long").val(map.getCenter().lng());';

            // $config['directions'] = true;
            // $config['directionsStart'] = 'new google.maps.LatLng()';
            // $config['directionsEnd'] = '-6.178954, 106.877306';
            // $config['directionsDivID'] =  'directionsDiv';

            $gmap = new GMaps();
            $gmap->initialize($config);

            $tpses = Tps::leftJoin('dapils', 'dapils.id', '=', 'tpses.id_dapil')
                      ->leftJoin('kelurahans', 'kelurahans.id', '=', 'dapils.kelurahan')
                      ->leftJoin('kecamatans', 'kecamatans.id', '=', 'dapils.kecamatan')
                      ->select('tpses.*', 'dapils.nama_dapil as dapil', 'kelurahans.nama_kelurahan as kelurahan', 'kecamatans.nama_kecamatan as kecamatan')
                      ->get();

            $marker['draggable'] = true;
            // $marker['icon'] = 'assets/images/template/marker2white.png';
            $marker['ondragend'] = '$("#lat").val(event.latLng.lat());$("#long").val(event.latLng.lng());';
            $marker['position'] = 'new google.maps.LatLng()';
            $marker['infowindow_content'] = 'Lokasi Anda';
            $gmap->add_marker($marker);

            foreach($tpses as $t) {
                $marker['position'] = $t->latitude . ',' . $t->longitude;
                $marker['infowindow_content'] = $t->nama_tps;
                $gmap->add_marker($marker);
            }

            $map = $gmap->create_map();
            return view('modul.tps.index', compact('map', 'kecamatans', 'kelurahans', 'tpses'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $dapils = Dapil::all();
        return view('modul.tps.create', compact('dapils'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'dapil.required'                                    => 'Anda belum memilih Dapil',
            'nama_tps.required'                                 => 'Anda belum mengisi Nama TPS',
            'longitude.required'                                => 'Anda belum mengisi Longitude',
            'latitude.required'                                 => 'Anda belum mengisi Latitude',
            'alamat.required'                                   => 'Anda belum mengisi Alamat',
        ];
        $rules = [
            'dapil'                                 => ['required', 'string', 'max:255'],
            'nama_tps'                              => ['required', 'string'],
            'longitude'                             => ['required', 'string'],
            'latitude'                              => ['required', 'string'],
            'alamat'                                => ['required', 'string'],
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect('tps/tambah')
                        ->withErrors($validator)
                        ->withInput();
        }

        $tps = Tps::create([
            'id_user'               => Auth::id(),
            'id_dapil'              => $request->dapil,
            'nama_tps'              => $request->nama_tps,
            'alamat'                => $request->alamat,
            'longitude'             => $request->longitude,
            'latitude'              => $request->latitude,
        ]);

        return redirect('/tps');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Tps::find($id);
        $dapils = Dapil::all();

        return view('modul.tps.edit', compact('data', 'dapils'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $messages = [
            'dapil.required'                                    => 'Anda belum memilih Dapil',
            'nama_tps.required'                                 => 'Anda belum mengisi Nama TPS',
            'longitude.required'                                => 'Anda belum mengisi Longitude',
            'latitude.required'                                 => 'Anda belum mengisi Latitude',
            'alamat.required'                                   => 'Anda belum mengisi Alamat',
        ];
        $rules = [
            'dapil'                                 => ['required', 'string', 'max:255'],
            'nama_tps'                              => ['required', 'string'],
            'longitude'                             => ['required', 'string'],
            'latitude'                              => ['required', 'string'],
            'alamat'                                => ['required', 'string'],
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect('tps/ubah/'.$id)
                        ->withErrors($validator)
                        ->withInput();
        }

        $tps = Tps::find($id);
        $tps->id_user               = Auth::id();
        $tps->id_dapil              = $request->dapil;
        $tps->nama_tps              = $request->nama_tps;
        $tps->alamat                = $request->alamat;
        $tps->longitude             = $request->longitude;
        $tps->latitude              = $request->latitude;
        $tps->update();

        return redirect('/tps');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tps = Tps::find($id);

        $tps->forceDelete();

        return redirect('/tps');
    }

    public function cari(Request $request)
    {
        $kecamatans = Kecamatan::all();
        $kelurahans = Kelurahan::all();
        $lat = '';
        $long = '';

        if ($request->action == 'arah') {
            if ($request->arahtps != null) {
                $data = Tps::find($request->arahtps);

                $tps = Tps::where('id', $request->arahtps)
                      ->get();

                $lat = $request->lat;
                $long = $request->long;

                // $config['onboundschanged'] = '$("#lat").val(map.getCenter().lat());$("#long").val(map.getCenter().lng());';

                $config['directions'] = true;
                $config['directionsDraggable'] = true;
                $config['directionsStart'] = $request->lat . ',' . $request->long;
                $config['directionsEnd'] = $data->latitude . ',' . $data->longitude;
                $config['directionsDivID'] =  'directionsDiv';
                $config['directionsChanged'] =  '
                function computeTotalDistance(result) {
                    var total = 0;
                    var myroute = result.routes[0];
                    for (var i = 0; i < myroute.legs.length; i++) {
                      total += myroute.legs[i].distance.value;
                    }
                    total = total / 1000;
                    $("#lat").val(myroute.legs[0].start_location.lat());
                    $("#long").val(myroute.legs[0].start_location.lng());

                    console.log(myroute.legs[0].start_location.lat());
                    console.log(myroute.legs[0].start_location.lng());
                }

                computeTotalDistance(directionsDisplay.getDirections())';
            }
            else {
                $lat = '3.595196';
                $long = '98.672226';

                $tps = Tps::all();
            }
        }
        else if ($request->action == 'reset')
        {
            return $this->index();
        }
        else {
            if ($request->kecamatan != null && $request->kelurahan == null) {
                $data = Kecamatan::find($request->kecamatan);

                $tps = Tps::leftJoin('dapils', 'dapils.id', '=', 'tpses.id_dapil')
                      ->leftJoin('kecamatans', 'kecamatans.id', '=', 'dapils.kecamatan')
                      ->where('dapils.kecamatan', $request->kecamatan)
                      ->select('tpses.*')
                      ->get();

                $lat = $data->latitude;
                $long = $data->longitude;
            }
            else if ($request->kecamatan != null && $request->kelurahan != null) {
                $data = Kelurahan::find($request->kelurahan);

                $tps = Tps::leftJoin('dapils', 'dapils.id', '=', 'tpses.id_dapil')
                      ->leftJoin('kelurahans', 'kelurahans.id', '=', 'dapils.kelurahan')
                      ->where('dapils.kelurahan', $request->kelurahan)
                      ->select('tpses.*')
                      ->get();

                $lat = $data->latitude;
                $long = $data->longitude;
            }
            else if ($request->tps != null) {
                $data = Tps::find($request->tps);

                $tps = Tps::where('id', $request->tps)
                      ->get();

                $lat = $data->latitude;
                $long = $data->longitude;
            }
            else {
                $lat = '3.595196';
                $long = '98.672226';

                $tps = Tps::all();
            }
        }

        $config['center'] = $lat . ',' . $long;
        $config['zoom'] = '15';
        $config['map_height'] = '100%';

        $gmap = new GMaps();
        $gmap->initialize($config);


        // $marker['draggable'] = true;
        // // $marker['icon'] = 'assets/images/template/marker2white.png';
        // $marker['ondragend'] = 'alert();$("#lat").val(event.latLng.lat());$("#long").val(event.latLng.lng());';
        // $marker['position'] = $request->lat . ',' . $request->long;
        // $marker['infowindow_content'] = 'Lokasi Anda';
        // $gmap->add_marker($marker);

        foreach($tps as $t) {
            $marker['position'] = $t->latitude . ',' . $t->longitude;
            $marker['infowindow_content'] = $t->nama_tps;
            $gmap->add_marker($marker);
        }

        $map = $gmap->create_map();

        $tpses = Tps::leftJoin('dapils', 'dapils.id', '=', 'tpses.id_dapil')
                      ->leftJoin('kelurahans', 'kelurahans.id', '=', 'dapils.kelurahan')
                      ->leftJoin('kecamatans', 'kecamatans.id', '=', 'dapils.kecamatan')
                      ->select('tpses.*', 'dapils.nama_dapil as dapil', 'kelurahans.nama_kelurahan as kelurahan', 'kecamatans.nama_kecamatan as kecamatan')
                      ->get();

        return view('modul.tps.index', compact('map', 'kecamatans', 'kelurahans', 'tpses'));
    }

    public function getKelurahans($id = 0) {
        $kelurahans['data'] = Kelurahan::where('id_kecamatan', $id)->get();

        echo json_encode($kelurahans);
        exit;
    }

    public function getDetail($id = 0) {
        $details = Tps::where('tpses.id', $id)
                      ->leftJoin('users', 'users.id', '=', 'tpses.id_user')
                      ->leftJoin('dapils', 'dapils.id', '=', 'tpses.id_dapil')
                      ->select('tpses.*', 'dapils.nama_dapil as dapil')
                      ->orderBy('tpses.created_at', 'DESC')->first();

        return Response::json($details);
    }

    public function getTpses() {
        $tpses = Tps::leftJoin('users', 'users.id', '=', 'tpses.id_user')
                     ->leftJoin('dapils', 'dapils.id', '=', 'tpses.id_dapil')
                     ->select('tpses.*', 'dapils.nama_dapil as dapil')
                     ->orderBy('tpses.created_at', 'DESC');

        return datatables()->of($tpses)
                    ->editColumn('keterangan', function($tps) {
                        $alamat = substr($tps->alamat, 0, 100) . '....';

                        return $alamat;
                    })
                    ->addColumn('aksi', function($tps) {
                        $detail = '<a href="#detail" data-id="'. $tps->id .'" class="waves-effect waves-light modal-trigger btn bc-blue-bg btn-add tooltipped btn-detail" data-position="bottom" data-tooltip="Detail" style="margin-right: 10px"><i class="material-icons">visibility</i></a>';
                        $edit = '<a href="/tps/ubah/'. $tps->id .'" class="waves-effect waves-light modal-trigger btn bc-green-bg btn-add tooltipped btn-detail" data-position="bottom" data-tooltip="Edit" style="margin-right: 10px"><i class="material-icons">edit</i></a>';
                        $hapus = '<a href="/tps/hapus/'. $tps->id .'" class="waves-effect waves-light modal-trigger btn bc-red-bg btn-add tooltipped btn-detail" data-position="bottom" data-tooltip="Hapus"><i class="material-icons">delete</i></a>';

                        return $detail . $edit . $hapus;
                    })
                    ->addIndexColumn()
                    ->rawColumns(['aksi'])
                    ->make(true);
    }
}
