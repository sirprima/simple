<?php

namespace App\Http\Controllers;

use Auth;
use App\Kecamatan;
use App\Kelurahan;
use App\Lapor;
use App\Kecurangan;
use App\Dpt_tidak_terdaftar;
use DB, Exception, Datatables, Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use App\Confirm;
use App\Dpt;
use App\Tps;

class LaporController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('modul.lapor.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kecamatans = Kecamatan::all();
        return view('modul.lapor.create', compact('kecamatans'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'jenis_laporan.required'                    => 'Anda belum memilih Jenis Laporan',
            'provinsi.required'                         => 'Anda belum memilih Provinsi',
            'kota.required'                             => 'Anda belum memilih Kabupaten/Kota',
            'kecamatan.required'                        => 'Anda belum memilih Kecamatan',
            'kelurahan.required'                        => 'Anda belum memilih Kelurahan',
            'tps.required'                              => 'Anda belum memilih Lokasi TPS',
            'tanggal_laporan.required'                  => 'Anda belum memilih Tanggal Laporan',
            'foto.required'                             => 'Anda belum mengupload Bukti Foto',
            'video.required'                            => 'Anda belum mengupload Bukti Video',
            'kk.required'                               => 'Anda belum mengupload Kartu Keluarga',
            'ktp.required'                              => 'Anda belum mengupload KTP',
            'kronologi.required'                        => 'Anda belum mengisi Kronologi',
        ];
        $rules = [
            'jenis_laporan'                 => ['required', 'string', 'max:255'],
            'provinsi'                      => ['required', 'integer'],
            'kota'                          => ['required', 'integer'],
            'kecamatan'                     => ['required', 'integer'],
            'kelurahan'                     => ['required', 'integer'],
            'tps'                           => ['required', 'integer'],
            'tanggal_laporan'               => ['required', 'date'],
            'kronologi'                     => ['required', 'string'],
        ];

        if ($request->jenis_laporan == 'Kecurangan') {
            $rules += [
                'foto'                          => ['required', 'image', 'mimes:jpeg,png,jpg,gif,svg', 'max:2048'],
                'video'                         => ['required', 'file', 'mimes:mp4,mov,ogg'],
            ];
        }
        else {
            $rules += [
                'kk'                            => ['required', 'image', 'mimes:jpeg,png,jpg,gif,svg', 'max:2048'],
                'ktp'                           => ['required', 'image', 'mimes:jpeg,png,jpg,gif,svg', 'max:2048'],
            ];
        }

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return redirect('lapor/tambah')
                        ->withErrors($validator)
                        ->withInput();
        }

        DB::beginTransaction();
        try {
            $lapor = Lapor::create([
                'jenis_laporan'                 => $request->jenis_laporan,
                'id_user'                       => Auth::id(),
                'id_provinsi'                   => $request->provinsi,
                'id_kabupaten'                  => $request->kota,
                'id_kecamatan'                  => $request->kecamatan,
                'id_kelurahan'                  => $request->kelurahan,
                'id_tps'                        => $request->tps,
                'tanggal_lapor'                 => Carbon::parse($request->tanggal_laporan)->format('Y-m-d'),
                'kronologi'                     => $request->kronologi,
                'status'                        => 'Belum Diproses'
            ]);

            if ($request->jenis_laporan == 'Kecurangan') {
                $foto = $request->foto;
                if ($foto->isValid()) {
                    $originalName = $foto->getClientOriginalName();
                    $name = 'BUKTI-FOTO-' . Carbon::now()->format('dmy') . rand(100, 900) . '.' . $foto->getClientOriginalExtension();
                    $destination = 'assets/images/content/';
                    $foto->move($destination, $name);
                }

                $video = $request->video;
                if ($video->isValid()) {
                    $originalName = $video->getClientOriginalName();
                    $nameV = 'BUKTI-VIDEO-' . Carbon::now()->format('dmy') . rand(100, 900) . '.' . $video->getClientOriginalExtension();
                    $destinationV = 'assets/images/content/';
                    $video->move($destinationV, $nameV);
                }

                $jenis_laporan = Kecurangan::create([
                    'id_lapor'      => $lapor->id,
                    'bukti_foto'    => $name,
                    'bukti_video'   => $nameV
                ]);
            }
            else {
                $kk = $request->kk;
                if ($kk->isValid()) {
                    $originalName = $kk->getClientOriginalName();
                    $name = 'KARTU-KELUARGA-' . Carbon::now()->format('dmy') . rand(100, 900) . '.' . $kk->getClientOriginalExtension();
                    $destination = 'assets/images/content/';
                    $kk->move($destination, $name);
                }

                $ktp = $request->ktp;
                if ($ktp->isValid()) {
                    $originalName = $ktp->getClientOriginalName();
                    $nameK = 'KTP-' . Carbon::now()->format('dmy') . rand(100, 900) . '.' . $ktp->getClientOriginalExtension();
                    $destinationK = 'assets/images/content/';
                    $ktp->move($destinationK, $nameK);
                }

                $jenis_laporan = Dpt_tidak_terdaftar::create([
                    'id_lapor'          => $lapor->id,
                    'kartu_keluarga'    => $name,
                    'ktp'               => $nameK
                ]);
            }
        }
        catch (ValidationException $e) {
            DB::rollBack();
            throw new ValidationException($e->validator, $e->getResponse());
        }
        catch (Exception $e) {
            DB::rollBack();
            dd($e);
            return unknownError($e, 'Tambah Laporan Gagal! Silahkan Coba Lagi!');
        }

        DB::commit();

        return redirect('/lapor');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('modul.lapor.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('modul.lapor.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $update = Lapor::find($request->id_detail);
        $update->status             = $request->action;
        $update->keterangan         = $request->keterangan;
        $update->update();

        $id_confirm = Confirm::where('id_user', Auth::id())->first();
        if ($id_confirm) {
            $confirm = Confirm::find($id_confirm->id);
            $confirm->status        = $request->action;
            $confirm->update();
        }
        else {
            $confirm = Confirm::create([
                'id_lapor'      => $request->id_detail,
                'id_user'       => Auth::id(),
                'status'        => $request->action
            ]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getKelurahans($id = 0) {
        $kelurahans['data'] = Kelurahan::where('id_kecamatan', $id)->get();

        echo json_encode($kelurahans);
        exit;
    }

    public function getTps($id = 0) {
        $tps['data'] = Tps::leftJoin('dapils', 'dapils.id', '=', 'tpses.id_dapil')
                          ->leftJoin('kelurahans', 'kelurahans.id', '=', 'dapils.kelurahan')
                          ->where('kelurahans.id', $id)->get();

        echo json_encode($tps);
        exit;
    }

    public function getDetail($id = 0) {
        $details = Lapor::where('lapors.id', $id)
                                ->leftJoin('kecurangans', 'kecurangans.id_lapor', '=', 'lapors.id')
                                ->leftJoin('dpt_tidak_terdaftars', 'dpt_tidak_terdaftars.id_lapor', '=', 'lapors.id')
                                ->leftJoin('kecamatans', 'kecamatans.id', '=', 'lapors.id_kecamatan')
                                ->leftJoin('kelurahans', 'kelurahans.id', '=', 'lapors.id_kelurahan')
                                ->leftJoin('tpses', 'tpses.id', '=', 'lapors.id_tps')
                                ->leftJoin('users', 'users.id', '=', 'lapors.id_user')
                                ->select('lapors.*', 'kecurangans.bukti_foto as bukti_foto', 'kecurangans.bukti_video as bukti_video', 'dpt_tidak_terdaftars.kartu_keluarga as kartu_keluarga', 'dpt_tidak_terdaftars.ktp as ktp', 'kecamatans.nama_kecamatan as kecamatan', 'kelurahans.nama_kelurahan as kelurahan', 'tpses.nama_tps as tps', 'users.nama_user as pelapor')->first();

        return Response::json($details);
    }

    public function getLaporans() {
        $lapors = Lapor::leftJoin('kecurangans', 'kecurangans.id_lapor', '=', 'lapors.id')
                    ->leftJoin('dpt_tidak_terdaftars', 'dpt_tidak_terdaftars.id_lapor', '=', 'lapors.id')
                    ->leftJoin('kecamatans', 'kecamatans.id', '=', 'lapors.id_kecamatan')
                    ->leftJoin('kelurahans', 'kelurahans.id', '=', 'lapors.id_kelurahan')
                    ->leftJoin('tpses', 'tpses.id', '=', 'lapors.id_tps')
                    ->leftJoin('users', 'users.id', '=', 'lapors.id_user')
                    ->select('lapors.*', 'kecurangans.bukti_foto as bukti_foto', 'kecurangans.bukti_video as bukti_video', 'dpt_tidak_terdaftars.kartu_keluarga as kartu_keluarga', 'dpt_tidak_terdaftars.ktp as ktp', 'kecamatans.nama_kecamatan as kecamatan', 'kelurahans.nama_kelurahan as kelurahan', 'tpses.nama_tps as tps', 'users.nama_user as pelapor')
                    ->orderBy('lapors.tanggal_lapor', 'DESC');

        return datatables()->of($lapors)
                    ->editColumn('provinsi', function($lapor) {
                        if ($lapor->id_provinsi == 1) {
                            return 'Sumatera Utara';
                        }
                    })
                    ->editColumn('kota', function($lapor) {
                        if ($lapor->id_kabupaten == 1) {
                            return 'Medan';
                        }
                    })
                    ->editColumn('bukti_foto', function($lapor) {
                        if ($lapor->jenis_laporan == 'Kecurangan') {
                            $link = '<a href="assets/images/content/'.$lapor->bukti_foto.'" download>Bukti Foto</a>';
                        }
                        else {
                            $link = '<a href="assets/images/content/'.$lapor->kartu_keluarga.'" download>Kartu Keluarga</a>';
                        }

                        return $link;
                    })
                    ->editColumn('bukti_video', function($lapor) {
                        if ($lapor->jenis_laporan == 'Kecurangan') {
                            $link = '<a href="assets/images/content/'.$lapor->bukti_video.'" download>Bukti Video</a>';
                        }
                        else {
                            $link = '<a href="assets/images/content/'.$lapor->ktp.'" download>KTP</a>';
                        }

                        return $link;
                    })
                    ->editColumn('kronologi', function($lapor) {
                        $kronologi = substr($lapor->kronologi, 0, 100) . '....';

                        return $kronologi;
                    })
                    ->addColumn('aksi', function($lapor) {
                        $detail = '<a href="#detail" data-id="'. $lapor->id .'" class="waves-effect waves-light modal-trigger btn bc-blue-bg btn-add tooltipped btn-detail" data-position="bottom" data-tooltip="Detail" style="margin-right: 10px"><i class="material-icons">visibility</i></a>';
                        if (Auth::user()->level_user != 'visitor') {
                            $hapus = '<a href="/lapor/hapus/'. $lapor->id .'" class="waves-effect waves-light modal-trigger btn bc-red-bg btn-add tooltipped btn-detail" data-position="bottom" data-tooltip="Hapus"><i class="material-icons">delete</i></a>';

                            return $detail . $hapus;
                        }
                        else {
                            return $detail;
                        }


                    })
                    ->addIndexColumn()
                    ->rawColumns(['aksi', 'provinsi', 'kota', 'bukti_foto', 'bukti_video'])
                    ->make(true);
    }
}
