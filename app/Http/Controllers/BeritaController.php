<?php

namespace App\Http\Controllers;

use App\Kecamatan;
use App\Kelurahan;
use App\Lapor;
use App\Kecurangan;
use App\Dpt_tidak_terdaftar;
use DB, Exception, Datatables, Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use App\Confirm;

class BeritaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = $this->getBeritas();
        // dd($data->toArray());
        return view('modul.berita.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = $this->getBerita($id);
        $confirm_s = Confirm::where('confirms.id_lapor', $data->id)
                            ->where('confirms.status', 'Disetujui')
                            ->leftJoin('users', 'users.id', '=', 'confirms.id_user')
                            ->select('confirms.*','users.nama_user as nama')
                            ->get();
        $confirm_t = Confirm::where('confirms.id_lapor', $data->id)
                            ->where('confirms.status', 'Ditolak')
                            ->leftJoin('users', 'users.id', '=', 'confirms.id_user')
                            ->select('confirms.*','users.nama_user as nama')
                            ->get();

        return view('modul.berita.show', compact('data', 'confirm_s', 'confirm_t'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function getBerita($id) {
        $beritas = Lapor::where('lapors.id', $id)
                        ->leftJoin('kecurangans', 'kecurangans.id_lapor', '=', 'lapors.id')
                        ->leftJoin('dpt_tidak_terdaftars', 'dpt_tidak_terdaftars.id_lapor', '=', 'lapors.id')
                        ->leftJoin('kecamatans', 'kecamatans.id', '=', 'lapors.id_kecamatan')
                        ->leftJoin('kelurahans', 'kelurahans.id', '=', 'lapors.id_kelurahan')
                        ->leftJoin('tpses', 'tpses.id', '=', 'lapors.id')
                        ->leftJoin('users', 'users.id', '=', 'lapors.id_user')
                        ->select('lapors.*', 'kecurangans.bukti_foto as bukti_foto', 'kecurangans.bukti_video as bukti_video', 'kecamatans.nama_kecamatan as kecamatan', 'kelurahans.nama_kelurahan as kelurahan', 'tpses.nama_tps as tps', 'users.nama_user as pelapor', 'dpt_tidak_terdaftars.kartu_keluarga as kk', 'dpt_tidak_terdaftars.ktp as ktp')->first();

        return $beritas;
    }

    private function getBeritas() {
        $beritas = Lapor::leftJoin('kecurangans', 'kecurangans.id_lapor', '=', 'lapors.id')
                        ->leftJoin('dpt_tidak_terdaftars', 'dpt_tidak_terdaftars.id_lapor', '=', 'lapors.id')
                        ->leftJoin('kecamatans', 'kecamatans.id', '=', 'lapors.id_kecamatan')
                        ->leftJoin('kelurahans', 'kelurahans.id', '=', 'lapors.id_kelurahan')
                        ->leftJoin('tpses', 'tpses.id', '=', 'lapors.id')
                        ->leftJoin('users', 'users.id', '=', 'lapors.id_user')
                        ->select('lapors.*', 'kecurangans.bukti_foto as bukti_foto', 'kecurangans.bukti_video as bukti_video', 'kecamatans.nama_kecamatan as kecamatan', 'kelurahans.nama_kelurahan as kelurahan', 'tpses.nama_tps as tps', 'users.nama_user as pelapor', 'dpt_tidak_terdaftars.kartu_keluarga as kk', 'dpt_tidak_terdaftars.ktp as ktp')->get();

        return $beritas;
    }
}
