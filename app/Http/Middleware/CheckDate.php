<?php

namespace App\Http\Middleware;

use Closure;
use Carbon\Carbon;

class CheckDate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Carbon::now()->format('dmY') == Carbon::parse("2019-08-10")->format('dmY')) {
            return abort('404');
        }

        return $next($request);
    }
}
