<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dapil extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_user', 'nama_dapil', 'provinsi', 'kabupaten_kota', 'kecamatan', 'kelurahan'
    ];
}
