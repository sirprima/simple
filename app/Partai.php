<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Partai extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_user', 'no_urut', 'nama_partai', 'lambang_partai', 'alamat', 'visi', 'misi'
    ];
}
