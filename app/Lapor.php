<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lapor extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'jenis_laporan', 'id_dapil', 'id_user', 'id_provinsi', 'id_kabupaten', 'id_kecamatan', 'id_kelurahan', 'id_tps', 'tanggal_lapor', 'kronologi', 'status', 'keterangan'
    ];
}
