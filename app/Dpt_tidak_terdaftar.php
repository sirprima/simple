<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dpt_tidak_terdaftar extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_lapor', 'kartu_keluarga', 'ktp'
    ];
}
