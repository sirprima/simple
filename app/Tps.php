<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tps extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_user', 'id_dapil', 'nama_tps', 'alamat', 'longitude', 'latitude'
    ];

    protected $table = 'tpses';
}
