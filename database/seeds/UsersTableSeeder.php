<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'nama_user' => 'Administrator',
                'username' => 'admin',
                'email' => 'admin@mail.id',
                'password' => bcrypt('123123'),
                'level_user' => 'admin',
            ],
            [
                'nama_user' => 'Petugas KPPS',
                'username' => 'kpps',
                'email' => 'kpps@mail.id',
                'password' => bcrypt('123123'),
                'level_user' => 'kpps',
            ],
            [
                'nama_user' => 'Saksi',
                'username' => 'saksi',
                'email' => 'saksi@mail.id',
                'password' => bcrypt('123123'),
                'level_user' => 'saksi',
            ],
            [
                'nama_user' => 'Visitor',
                'username' => 'visitor',
                'email' => 'visitor@mail.id',
                'password' => bcrypt('123123'),
                'level_user' => 'visitor',
            ]
        ]);
    }
}
