<?php

use Illuminate\Database\Seeder;

class KelurahansTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('kelurahans')->insert([
            [
                'nama_kelurahan'    => 'Amplas',
                'id_kecamatan'      => 1
            ],
            [
                'nama_kelurahan'    => 'Bangun Mulia',
                'id_kecamatan'      => 1
            ],
            [
                'nama_kelurahan'    => 'Harjosari I',
                'id_kecamatan'      => 1
            ],
            [
                'nama_kelurahan'    => 'Harjosari II',
                'id_kecamatan'      => 1
            ],
            [
                'nama_kelurahan'    => 'Sitirejo I',
                'id_kecamatan'      => 1
            ],
            [
                'nama_kelurahan'    => 'Sitirejo II',
                'id_kecamatan'      => 1
            ],
            [
                'nama_kelurahan'    => 'Sitirejo III',
                'id_kecamatan'      => 1
            ],
            [
                'nama_kelurahan'    => 'Timbang Deli',
                'id_kecamatan'      => 1
            ],
            [
                'nama_kelurahan'    => 'Kotamatsum I',
                'id_kecamatan'      => 2
            ],
            [
                'nama_kelurahan'    => 'Kotamatsum II',
                'id_kecamatan'      => 2
            ],
            [
                'nama_kelurahan'    => 'Kotamatsum IV',
                'id_kecamatan'      => 2
            ],
            [
                'nama_kelurahan'    => 'Tegal Sari I',
                'id_kecamatan'      => 2
            ],
            [
                'nama_kelurahan'    => 'Tegal Sari II',
                'id_kecamatan'      => 2
            ],
            [
                'nama_kelurahan'    => 'Tegal Sari III',
                'id_kecamatan'      => 2
            ],
            [
                'nama_kelurahan'    => 'Pandau Hulu II',
                'id_kecamatan'      => 2
            ],
            [
                'nama_kelurahan'    => 'Sei Rengas II',
                'id_kecamatan'      => 2
            ],
            [
                'nama_kelurahan'    => 'Sei Rengas Permata',
                'id_kecamatan'      => 2
            ],
            [
                'nama_kelurahan'    => 'Pasar Merah Timur',
                'id_kecamatan'      => 2
            ],
            [
                'nama_kelurahan'    => 'Sukaramai I',
                'id_kecamatan'      => 2
            ],
            [
                'nama_kelurahan'    => 'Sukaramai II',
                'id_kecamatan'      => 2
            ],
            [
                'nama_kelurahan'    => 'Glugur Kota',
                'id_kecamatan'      => 3
            ],
            [
                'nama_kelurahan'    => 'Karang Barombak',
                'id_kecamatan'      => 3
            ],
            [
                'nama_kelurahan'    => 'Kesawan',
                'id_kecamatan'      => 3
            ],
            [
                'nama_kelurahan'    => 'Pulo Brayan Kota',
                'id_kecamatan'      => 3
            ],
            [
                'nama_kelurahan'    => 'Sei Agul',
                'id_kecamatan'      => 3
            ],
            [
                'nama_kelurahan'    => 'Silalas',
                'id_kecamatan'      => 3
            ],
            [
                'nama_kelurahan'    => 'Babura',
                'id_kecamatan'      => 4
            ],
            [
                'nama_kelurahan'    => 'Darat',
                'id_kecamatan'      => 4
            ],
            [
                'nama_kelurahan'    => 'Merdeka',
                'id_kecamatan'      => 4
            ],
            [
                'nama_kelurahan'    => 'Padang Bulan',
                'id_kecamatan'      => 4
            ],
            [
                'nama_kelurahan'    => 'Petisah Hulu',
                'id_kecamatan'      => 4
            ],
            [
                'nama_kelurahan'    => 'Titi Rantai',
                'id_kecamatan'      => 4
            ],
            [
                'nama_kelurahan'    => 'Bagan Deli',
                'id_kecamatan'      => 5
            ],
            [
                'nama_kelurahan'    => 'Belawan Bahagia',
                'id_kecamatan'      => 5
            ],
            [
                'nama_kelurahan'    => 'Belawan Bahari',
                'id_kecamatan'      => 5
            ],
            [
                'nama_kelurahan'    => 'Belawan Sicanang',
                'id_kecamatan'      => 5
            ],
            [
                'nama_kelurahan'    => 'Belawan I',
                'id_kecamatan'      => 5
            ],
            [
                'nama_kelurahan'    => 'Belawan II',
                'id_kecamatan'      => 5
            ],
            [
                'nama_kelurahan'    => 'Kota Bangun',
                'id_kecamatan'      => 6
            ],
            [
                'nama_kelurahan'    => 'Mabar',
                'id_kecamatan'      => 6
            ],
            [
                'nama_kelurahan'    => 'Mabar Hilir',
                'id_kecamatan'      => 6
            ],
            [
                'nama_kelurahan'    => 'Tanjung Mulia',
                'id_kecamatan'      => 6
            ],
            [
                'nama_kelurahan'    => 'Tanjung Mulia Hilir',
                'id_kecamatan'      => 6
            ],
            [
                'nama_kelurahan'    => 'Titi Papan',
                'id_kecamatan'      => 6
            ],
            [
                'nama_kelurahan'    => 'Binjai',
                'id_kecamatan'      => 7
            ],
            [
                'nama_kelurahan'    => 'Denai',
                'id_kecamatan'      => 7
            ],
            [
                'nama_kelurahan'    => 'Medan Tenggara',
                'id_kecamatan'      => 7
            ],
            [
                'nama_kelurahan'    => 'Tegalsari Mandala I',
                'id_kecamatan'      => 7
            ],
            [
                'nama_kelurahan'    => 'Tegalsari Mandala II',
                'id_kecamatan'      => 7
            ],
            [
                'nama_kelurahan'    => 'Tegalsari Mandala III',
                'id_kecamatan'      => 7
            ],
            [
                'nama_kelurahan'    => 'Cinta Damai',
                'id_kecamatan'      => 8
            ],
            [
                'nama_kelurahan'    => 'Dwikora',
                'id_kecamatan'      => 8
            ],
            [
                'nama_kelurahan'    => 'Helvetia',
                'id_kecamatan'      => 8
            ],
            [
                'nama_kelurahan'    => 'Helvetia Tengah',
                'id_kecamatan'      => 8
            ],
            [
                'nama_kelurahan'    => 'Helvetia Timur',
                'id_kecamatan'      => 8
            ],
            [
                'nama_kelurahan'    => 'Sei Sikambing C II',
                'id_kecamatan'      => 8
            ],
            [
                'nama_kelurahan'    => 'Tanjung Gusta',
                'id_kecamatan'      => 8
            ],
            [
                'nama_kelurahan'    => 'Gedung Johor',
                'id_kecamatan'      => 9
            ],
            [
                'nama_kelurahan'    => 'Kedai Durian',
                'id_kecamatan'      => 9
            ],
            [
                'nama_kelurahan'    => 'Kwala Bekala',
                'id_kecamatan'      => 9
            ],
            [
                'nama_kelurahan'    => 'Pangkalan Mansyur',
                'id_kecamatan'      => 9
            ],
            [
                'nama_kelurahan'    => 'Sukamaju',
                'id_kecamatan'      => 9
            ],
            [
                'nama_kelurahan'    => 'Titi Kuning',
                'id_kecamatan'      => 9
            ],
            [
                'nama_kelurahan'    => 'Kotamatsum III',
                'id_kecamatan'      => 10
            ],
            [
                'nama_kelurahan'    => 'Mesjid',
                'id_kecamatan'      => 10
            ],
            [
                'nama_kelurahan'    => 'Pandau Hulu I',
                'id_kecamatan'      => 10
            ],
            [
                'nama_kelurahan'    => 'Pasar Baru',
                'id_kecamatan'      => 10
            ],
            [
                'nama_kelurahan'    => 'Pasar Merah Barat',
                'id_kecamatan'      => 10
            ],
            [
                'nama_kelurahan'    => 'Pusat Pasar',
                'id_kecamatan'      => 10
            ],
            [
                'nama_kelurahan'    => 'Sei Rengas I',
                'id_kecamatan'      => 10
            ],
            [
                'nama_kelurahan'    => 'Sitirejo I',
                'id_kecamatan'      => 10
            ],
            [
                'nama_kelurahan'    => 'Sudirejo I',
                'id_kecamatan'      => 10
            ],
            [
                'nama_kelurahan'    => 'Sudirejo II',
                'id_kecamatan'      => 10
            ],
            [
                'nama_kelurahan'    => 'Teladan Barat',
                'id_kecamatan'      => 10
            ],
            [
                'nama_kelurahan'    => 'Teladan Timur',
                'id_kecamatan'      => 10
            ],
            [
                'nama_kelurahan'    => 'Besar',
                'id_kecamatan'      => 11
            ],
            [
                'nama_kelurahan'    => 'Martubung',
                'id_kecamatan'      => 11
            ],
            [
                'nama_kelurahan'    => 'Nelayan Indah',
                'id_kecamatan'      => 11
            ],
            [
                'nama_kelurahan'    => 'Pekan Labuhan',
                'id_kecamatan'      => 11
            ],
            [
                'nama_kelurahan'    => 'Sei Mati',
                'id_kecamatan'      => 11
            ],
            [
                'nama_kelurahan'    => 'Tangkahan',
                'id_kecamatan'      => 11
            ],
            [
                'nama_kelurahan'    => 'Aur',
                'id_kecamatan'      => 12
            ],
            [
                'nama_kelurahan'    => 'Hamdan',
                'id_kecamatan'      => 12
            ],
            [
                'nama_kelurahan'    => 'Jati',
                'id_kecamatan'      => 12
            ],
            [
                'nama_kelurahan'    => 'Kampung Baru',
                'id_kecamatan'      => 12
            ],
            [
                'nama_kelurahan'    => 'Sei Mati',
                'id_kecamatan'      => 12
            ],
            [
                'nama_kelurahan'    => 'Sukaraja',
                'id_kecamatan'      => 12
            ],
            [
                'nama_kelurahan'    => 'Labuhan Deli',
                'id_kecamatan'      => 13
            ],
            [
                'nama_kelurahan'    => 'Paya Pasir',
                'id_kecamatan'      => 13
            ],
            [
                'nama_kelurahan'    => 'Rengas Pulau',
                'id_kecamatan'      => 13
            ],
            [
                'nama_kelurahan'    => 'Tanah Enam Ratus',
                'id_kecamatan'      => 13
            ],
            [
                'nama_kelurahan'    => 'Terjun',
                'id_kecamatan'      => 13
            ],
            [
                'nama_kelurahan'    => 'Tegal Rejo',
                'id_kecamatan'      => 14
            ],
            [
                'nama_kelurahan'    => 'Sidorame Barat I',
                'id_kecamatan'      => 14
            ],
            [
                'nama_kelurahan'    => 'Sidorame Barat II',
                'id_kecamatan'      => 14
            ],
            [
                'nama_kelurahan'    => 'Sidorame Timur',
                'id_kecamatan'      => 14
            ],
            [
                'nama_kelurahan'    => 'Sei Kera Hilir I',
                'id_kecamatan'      => 14
            ],
            [
                'nama_kelurahan'    => 'Sei Kera Hilir II',
                'id_kecamatan'      => 14
            ],
            [
                'nama_kelurahan'    => 'Sei Kera Hulu',
                'id_kecamatan'      => 14
            ],
            [
                'nama_kelurahan'    => 'Pahlawan',
                'id_kecamatan'      => 14
            ],
            [
                'nama_kelurahan'    => 'Pandau Hilir',
                'id_kecamatan'      => 14
            ],
            [
                'nama_kelurahan'    => 'Petisah Tengah',
                'id_kecamatan'      => 15
            ],
            [
                'nama_kelurahan'    => 'Sei Putih Barat',
                'id_kecamatan'      => 15
            ],
            [
                'nama_kelurahan'    => 'Sei Putih Tengah',
                'id_kecamatan'      => 15
            ],
            [
                'nama_kelurahan'    => 'Sei Putih Timur I',
                'id_kecamatan'      => 15
            ],
            [
                'nama_kelurahan'    => 'Sei Putih Timur II',
                'id_kecamatan'      => 15
            ],
            [
                'nama_kelurahan'    => 'Sei Sikambing D',
                'id_kecamatan'      => 15
            ],
            [
                'nama_kelurahan'    => 'Sekip',
                'id_kecamatan'      => 15
            ],
            [
                'nama_kelurahan'    => 'Anggrung',
                'id_kecamatan'      => 16
            ],
            [
                'nama_kelurahan'    => 'Maddras Hulu',
                'id_kecamatan'      => 16
            ],
            [
                'nama_kelurahan'    => 'Polonia',
                'id_kecamatan'      => 16
            ],
            [
                'nama_kelurahan'    => 'Sari Rejo',
                'id_kecamatan'      => 16
            ],
            [
                'nama_kelurahan'    => 'Suka Damai',
                'id_kecamatan'      => 16
            ],
            [
                'nama_kelurahan'    => 'Babura Sunggal',
                'id_kecamatan'      => 17
            ],
            [
                'nama_kelurahan'    => 'Lalang',
                'id_kecamatan'      => 17
            ],
            [
                'nama_kelurahan'    => 'Sai Sikambing B',
                'id_kecamatan'      => 17
            ],
            [
                'nama_kelurahan'    => 'Simpang Tanjung',
                'id_kecamatan'      => 17
            ],
            [
                'nama_kelurahan'    => 'Sunggal',
                'id_kecamatan'      => 17
            ],
            [
                'nama_kelurahan'    => 'Tanjung Rejo',
                'id_kecamatan'      => 17
            ],
            [
                'nama_kelurahan'    => 'Asam Kumbang',
                'id_kecamatan'      => 18
            ],
            [
                'nama_kelurahan'    => 'Beringin',
                'id_kecamatan'      => 18
            ],
            [
                'nama_kelurahan'    => 'Padang Bulan Selayang I',
                'id_kecamatan'      => 18
            ],
            [
                'nama_kelurahan'    => 'Padang Bulan Selayang II',
                'id_kecamatan'      => 18
            ],
            [
                'nama_kelurahan'    => 'Sampakata',
                'id_kecamatan'      => 18
            ],
            [
                'nama_kelurahan'    => 'Tanjung Sari',
                'id_kecamatan'      => 18
            ],
            [
                'nama_kelurahan'    => 'Bandar Selamat',
                'id_kecamatan'      => 19
            ],
            [
                'nama_kelurahan'    => 'Bantan',
                'id_kecamatan'      => 19
            ],
            [
                'nama_kelurahan'    => 'Bantan Timur',
                'id_kecamatan'      => 19
            ],
            [
                'nama_kelurahan'    => 'Indra Kasih',
                'id_kecamatan'      => 19
            ],
            [
                'nama_kelurahan'    => 'Sidorejo',
                'id_kecamatan'      => 19
            ],
            [
                'nama_kelurahan'    => 'Siderejo Hilir',
                'id_kecamatan'      => 19
            ],
            [
                'nama_kelurahan'    => 'Tembung',
                'id_kecamatan'      => 19
            ],
            [
                'nama_kelurahan'    => 'Baru Ladang Bambu',
                'id_kecamatan'      => 20
            ],
            [
                'nama_kelurahan'    => 'Kemenangan Tani',
                'id_kecamatan'      => 20
            ],
            [
                'nama_kelurahan'    => 'Lau Cih',
                'id_kecamatan'      => 20
            ],
            [
                'nama_kelurahan'    => 'Mangga',
                'id_kecamatan'      => 20
            ],
            [
                'nama_kelurahan'    => 'Namo Gajah',
                'id_kecamatan'      => 20
            ],
            [
                'nama_kelurahan'    => 'Sidomulyo',
                'id_kecamatan'      => 20
            ],
            [
                'nama_kelurahan'    => 'Simalingkar B',
                'id_kecamatan'      => 20
            ],
            [
                'nama_kelurahan'    => 'Simpang Selayang',
                'id_kecamatan'      => 20
            ],
            [
                'nama_kelurahan'    => 'Tanjung Selamat',
                'id_kecamatan'      => 20
            ],
            [
                'nama_kelurahan'    => 'Durian',
                'id_kecamatan'      => 21
            ],
            [
                'nama_kelurahan'    => 'Gaharu',
                'id_kecamatan'      => 21
            ],
            [
                'nama_kelurahan'    => 'Gang Buntu',
                'id_kecamatan'      => 21
            ],
            [
                'nama_kelurahan'    => 'Glugur Darat I',
                'id_kecamatan'      => 21
            ],
            [
                'nama_kelurahan'    => 'Glugur Darat II',
                'id_kecamatan'      => 21
            ],
            [
                'nama_kelurahan'    => 'Perintis',
                'id_kecamatan'      => 21
            ],
            [
                'nama_kelurahan'    => 'Pulo Brayan Bengkel',
                'id_kecamatan'      => 21
            ],
            [
                'nama_kelurahan'    => 'Pulo Brayan Bengkel Baru',
                'id_kecamatan'      => 21
            ],
            [
                'nama_kelurahan'    => 'Pulo Brayan Darat I',
                'id_kecamatan'      => 21
            ],
            [
                'nama_kelurahan'    => 'Pulo Brayan Darat II',
                'id_kecamatan'      => 21
            ],
            [
                'nama_kelurahan'    => 'Sidodadi',
                'id_kecamatan'      => 21
            ]
        ]);
    }
}
