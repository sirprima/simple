<?php

use Illuminate\Database\Seeder;

class KecamatansTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('kecamatans')->insert([
            [
                'nama_kecamatan' => 'Medan Amplas'
            ],
            [
                'nama_kecamatan' => 'Medan Area'
            ],
            [
                'nama_kecamatan' => 'Medan Barat'
            ],
            [
                'nama_kecamatan' => 'Medan Baru'
            ],
            [
                'nama_kecamatan' => 'Medan Belawan'
            ],
            [
                'nama_kecamatan' => 'Medan Deli'
            ],
            [
                'nama_kecamatan' => 'Medan Denai'
            ],
            [
                'nama_kecamatan' => 'Medan Helvetia'
            ],
            [
                'nama_kecamatan' => 'Medan Johor'
            ],
            [
                'nama_kecamatan' => 'Medan Kota'
            ],
            [
                'nama_kecamatan' => 'Medan Labuhan'
            ],
            [
                'nama_kecamatan' => 'Medan Maimun'
            ],
            [
                'nama_kecamatan' => 'Medan Marelan'
            ],
            [
                'nama_kecamatan' => 'Medan Perjuangan'
            ],
            [
                'nama_kecamatan' => 'Medan Petisah'
            ],
            [
                'nama_kecamatan' => 'Medan Polonia'
            ],
            [
                'nama_kecamatan' => 'Medan Sunggal'
            ],
            [
                'nama_kecamatan' => 'Medan Selayang'
            ],
            [
                'nama_kecamatan' => 'Medan Tembung'
            ],
            [
                'nama_kecamatan' => 'Medan Tuntungan'
            ],
            [
                'nama_kecamatan' => 'Medan Timur'
            ]
        ]);
    }
}
