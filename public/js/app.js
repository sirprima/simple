$(document).ready(function(){
    // sidenav
    $('.sidenav-fixed').sidenav();

    // sidenav
    $('.sidenav.right').sidenav({
        edge: 'right',
        inDuration: 100
    });

    // colapsible
    $('.collapsible').collapsible();

    // Tooltips
    $('.tooltipped').tooltip();

    // Select
    $('select').formSelect();

    // modal
    $('.modal').modal();

    //picker
    $('.datepicker').datepicker();

    $('.sidenav-trigger#show').on('click', function() {
        $('.sidenav-trigger#show').hide();
        $('.sidenav-close#hide').show();
    });

    $('.sidenav-close#hide').on('click', function() {
        $('.sidenav.right').sidenav('close');
        $('.sidenav-trigger#show').show();
        $('.sidenav-close#hide').hide();
    });

    $('.sidenav-overlay').on('click', function() {
        $('.sidenav-trigger#show').show();
        $('.sidenav-close#hide').hide();
    })
});
