# README #

* project framework : Laravel V 5.8
* project database  : MySQL
* project CSS Framework   : materialize css -> materializecss.com
* other library    : JQuery, Yearpicker, fontawesome, datatables

### Setting XAMPP ###

* Open PHP.ini
* find memory_limit and change to memory_limit=256M
* find post_max_size and change to post_max_size=6G
* find upload_max_filesize and change to upload_max_filesize=4G

### Database ###

* create Database on PhpMyadmin
	- Database Name = brocyber_simple *you can use your own database name
* open .Env file
* find :
	- DB_CONNECTION=mysql
	- DB_HOST=127.0.0.1
	- DB_PORT=3306
	- DB_DATABASE=brocyber_simple -> your database name
	- DB_USERNAME=root -> your database username
	- DB_PASSWORD= ->your database password
	
### To Migrate Table to Database ###
* after setting DB in .Env file, run "php artisan migrate --seed" in console/CMD

### Step For Run Project At First Time ###
* Run CMD, change to project directory
* Run this command:
	- composer update
	- php artisan migrate --seed *if you not have the database*
	- php artisan serve *for run the website*

### Username & Password Default ###
* Admin
	- username : admin
	- password : 123123
	
* Visitor
	- username : visitor
	- password : 123123
	
* Saksi
	- username : saksi
	- password : 123123
	
* KPPS
	- username : kpps
	- password : 123123