<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function() {
    if (Auth::check()) {
        if (Auth::user()->level_user == 'visitor') {
            return redirect('/lapor');
        }
        else if (Auth::user()->level_user == 'admin') {
            return redirect('/caleg');
        }
    }
    else {
        return redirect('/info-dpt');
    }
});

Route::prefix('/info-dpt')->group(function() {
    Route::get('/', 'DptController@index');
    Route::get('/detil/{id}', 'DptController@show');
    Route::get('/cari/{nik}/{nama}', 'DptController@cari');
});

Route::prefix('/info-caleg')->group(function() {
    Route::get('/', 'CalegController@index');
    Route::get('/detil/{id}', 'CalegController@show');
    Route::get('/cari/{provinsi}/{dapil}/{pemilihan}', 'CalegController@cari');
});

Route::prefix('/info-tps')->group(function() {
    Route::get('/', 'TpsController@index');
    Route::get('/detil/{id}', 'TpsController@show');
    Route::get('/getKelurahans/{id}', 'TpsController@getKelurahans');
    Route::post('/', 'TpsController@cari');
});

Route::prefix('/berita')->group(function() {
    Route::get('/', 'BeritaController@index');
    Route::get('/detil/{id}', 'BeritaController@show');
});

Auth::routes();

Route::prefix('/lapor')->group(function() {
    Route::get('/', 'LaporController@index');
    Route::get('/tambah', 'LaporController@create');
    Route::get('/ubah/{id}', 'LaporController@edit');
    Route::get('/detil/{id}', 'LaporController@show');
    Route::post('/simpan', 'LaporController@store');
    Route::post('/perbarui', 'LaporController@update');
    Route::delete('/hapus/{id}', 'LaporController@destroy');
    Route::get('/getKelurahans/{id}', 'LaporController@getKelurahans');
    Route::get('/getTps/{id}', 'LaporController@getTps');
    Route::get('/getLaporans', 'LaporController@getLaporans');
    Route::get('/getDetail/{id}', 'LaporController@getDetail');
});

Route::prefix('/caleg')->group(function() {
    Route::get('/', 'CalegController@index');
    Route::get('/tambah', 'CalegController@create');
    Route::get('/ubah/{id}', 'CalegController@edit');
    Route::get('/detil/{id}', 'CalegController@show');
    Route::post('/simpan', 'CalegController@store');
    Route::post('/perbarui/{id}', 'CalegController@update');
    Route::get('/hapus/{id}', 'CalegController@destroy');
    Route::get('/getCalegs', 'CalegController@getCalegs');
    Route::get('/getDetail/{id}', 'CalegController@getDetail');
});

Route::prefix('/tps')->group(function() {
    Route::get('/', 'TpsController@index');
    Route::get('/tambah', 'TpsController@create');
    Route::get('/ubah/{id}', 'TpsController@edit');
    Route::get('/detil/{id}', 'TpsController@show');
    Route::post('/simpan', 'TpsController@store');
    Route::post('/perbarui/{id}', 'TpsController@update');
    Route::get('/hapus/{id}', 'TpsController@destroy');
    Route::get('/getTpses', 'TpsController@getTpses');
    Route::get('/getDetail/{id}', 'TpsController@getDetail');
});

Route::prefix('/dpt')->group(function() {
    Route::get('/', 'DptController@index');
    Route::get('/tambah', 'DptController@create');
    Route::get('/ubah/{id}', 'DptController@edit');
    Route::get('/detil/{id}', 'DptController@show');
    Route::post('/simpan', 'DptController@store');
    Route::post('/perbarui/{id}', 'DptController@update');
    Route::get('/hapus/{id}', 'DptController@destroy');
    Route::get('/getTps/{id}', 'DptController@getTps');
    Route::get('/getDpts', 'DptController@getDpts');
    Route::get('/getDetail/{id}', 'DptController@getDetail');
});

Route::prefix('/rekam-jejak')->group(function() {
    Route::get('/', 'RekamController@index');
    Route::get('/tambah', 'RekamController@create');
    Route::get('/ubah/{id}', 'RekamController@edit');
    Route::get('/detil/{id}', 'RekamController@show');
    Route::post('/simpan', 'RekamController@store');
    Route::post('/perbarui/{id}', 'RekamController@update');
    Route::get('/hapus/{id}', 'RekamController@destroy');
    Route::get('/getRekams', 'RekamController@getRekams');
    Route::get('/getDetail/{id}', 'RekamController@getDetail');
});

Route::prefix('/user')->group(function() {
    Route::get('/', 'UserController@index');
    Route::get('/tambah', 'UserController@create');
    Route::get('/ubah/{id}', 'UserController@edit');
    Route::get('/detil/{id}', 'UserController@show');
    Route::post('/simpan', 'UserController@store');
    Route::post('/password', 'UserController@password');
    Route::post('/perbarui/{id}', 'UserController@update');
    Route::get('/hapus/{id}', 'UserController@destroy');
    Route::get('/getUsers', 'UserController@getUsers');
    Route::get('/getDetail/{id}', 'UserController@getDetail');
});

Route::prefix('/dapil')->group(function() {
    Route::get('/', 'DapilController@index');
    Route::get('/tambah', 'DapilController@create');
    Route::get('/ubah/{id}', 'DapilController@edit');
    Route::get('/detil/{id}', 'DapilController@show');
    Route::post('/simpan', 'DapilController@store');
    Route::post('/perbarui/{id}', 'DapilController@update');
    Route::get('/hapus/{id}', 'DapilController@destroy');
    Route::get('/getDapils', 'DapilController@getDapils');
    Route::get('/getDetail/{id}', 'DapilController@getDetail');
    Route::get('/getKelurahans/{id}', 'DapilController@getKelurahans');
});

Route::prefix('/partai')->group(function() {
    Route::get('/', 'PartaiController@index');
    Route::get('/tambah', 'PartaiController@create');
    Route::get('/ubah/{id}', 'PartaiController@edit');
    Route::get('/detil/{id}', 'PartaiController@show');
    Route::post('/simpan', 'PartaiController@store');
    Route::post('/perbarui/{id}', 'PartaiController@update');
    Route::get('/hapus/{id}', 'PartaiController@destroy');
    Route::get('/getPartais', 'PartaiController@getPartais');
    Route::get('/getDetail/{id}', 'PartaiController@getDetail');
});
