<nav class="bc-blue-bg">
    <div class="container">
        <div class="nav-wrapper">
            <a href="#" class="brand-logo">Sistem Informasi Pileg Kota Medan</a>
            @if(Auth::check())
            <a href="#" id="show" data-target="right-nav" class="sidenav-trigger right"><i class="material-icons">dehaze</i></a>
            <a href="#" id="hide" class="sidenav-close right" style="display:none; margin: 0 18px;"><i class="material-icons">clear</i></a>
            @endif

            @if (!Auth::check() || Auth::user()->level_user == 'visitor' || Auth::user()->level_user == 'kpps' || Auth::user()->level_user == 'saksi')
            <ul id="nav-mobile" class="right hide-on-med-and-down">
                <li class="{{ Request::segment(1) == 'info-dpt' ? 'active' : '' }}"><a href="/info-dpt">Info DPT</a></li>
                <li class="{{ Request::segment(1) == 'info-caleg' ? 'active' : '' }}"><a href="/info-caleg">Info CALEG</a></li>
                <li class="{{ Request::segment(1) == 'info-tps' ? 'active' : '' }}"><a href="/info-tps">Info TPS</a></li>
                <li class="{{ Request::segment(1) == 'berita' ? 'active' : '' }}"><a href="/berita">Info Berita</a></li>
                @if(!Auth::check())
                <li style="margin-left: 50px;">
                <a class="tooltipped"  data-position="bottom" data-tooltip="Login" href="/login"><i class="material-icons">input</i></a>
                </li>
                @endif
            </ul>
            @endif
        </div>
    </div>
</nav>

@if (Auth::check())
<ul id="right-nav" class="sidenav right">
@if (Auth::user()->level_user == 'visitor' || Auth::user()->level_user == 'kpps' || Auth::user()->level_user == 'saksi')
<li class="{{ Request::segment(1) == 'lapor' ? 'active' : '' }}"><a href="/lapor"><i class="material-icons left">keyboard_arrow_right</i>Lapor</a></li>
{{-- <li class="{{ Request::segment(1) == 'dpt/tambah' ? 'active' : '' }}"><a href="/dpt/tambah"><i class="material-icons left">keyboard_arrow_right</i>DPT</a></li> --}}
<li><div class="divider"></div></li>
@else
<li class="{{ Request::segment(1) == 'caleg' ? 'active' : '' }}"><a href="/caleg"><i class="material-icons left">keyboard_arrow_right</i>Caleg</a></li>
<li class="{{ Request::segment(1) == 'tps' ? 'active' : '' }}"><a href="/tps"><i class="material-icons left">keyboard_arrow_right</i>TPS</a></li>
<li class="{{ Request::segment(1) == 'dpt' ? 'active' : '' }}"><a href="/dpt"><i class="material-icons left">keyboard_arrow_right</i>DPT</a></li>
<li class="{{ Request::segment(1) == 'rekam-jejak' ? 'active' : '' }}"><a href="/rekam-jejak"><i class="material-icons left">keyboard_arrow_right</i>Rekam Jejak</a></li>
<li class="{{ Request::segment(1) == 'dapil' ? 'active' : '' }}"><a href="/dapil"><i class="material-icons left">keyboard_arrow_right</i>Dapil</a></li>
<li class="{{ Request::segment(1) == 'partai' ? 'active' : '' }}"><a href="/partai"><i class="material-icons left">keyboard_arrow_right</i>Partai</a></li>
<li><div class="divider"></div></li>
<li class="{{ Request::segment(1) == 'user' ? 'active' : '' }}"><a href="/user"><i class="material-icons left">keyboard_arrow_right</i>User</a></li>
@endif
<li>
    <a class="tooltipped"  data-position="bottom" data-tooltip="Keluar" href="{{ route('logout') }}"
        onclick="event.preventDefault();
        document.getElementById('logout-form').submit();"><i class="material-icons">power_settings_new</i>Logout
    </a>

    <form id="logout-form" action="{{ route('logout') }}" method="POST">
        @csrf
    </form>
</li>
</ul>
@endif