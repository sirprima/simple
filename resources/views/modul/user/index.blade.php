@extends('layouts.app')

@extends('navigation.navigation')

@section('css')
<style media="screen">
    select {
        display: block;
    }
</style>
@endsection

@section('content')
<main style="padding-left: 0px !important;">
    <div class="user">
        <div class="container">
            <div class="row">
                <div class="col s12">
                    <h5 class="left">User</h5>
                    <a href="/user/tambah" class="btn bc-blue-bg btn-add right">Tambah</a>
                </div>
                <div class="col s12">
                    <div class="card">
                        <div class="card-content">
                            <table id="user_datatable" class="display" style="width: 100%">
                                <thead>
                                    <tr>
                                        <th>No</td>
                                        <th>ID User</th>
                                        <th>Level User</th>
                                        <th>Nama</th>
                                        <th>Username</th>
                                        <th>Email</th>
                                        <th class="center">Aksi</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<div id="detail" class="modal modal-fixed-footer">
        <div class="modal-content">
            <h4>Detail</h4>
            <table class="table">
                <tr>
                    <td>ID User</td>
                    <td>:</td>
                    <td><span class="id"></span></td>
                </tr>
                <tr>
                    <td>Level User</td>
                    <td>:</td>
                    <td><span class="level"></span></td>
                </tr>
                <tr>
                    <td>Nama</td>
                    <td>:</td>
                    <td><span class="nama"></span></td>
                </tr>
                <tr>
                    <td>Username</td>
                    <td>:</td>
                    <td><span class="username"></span></td>
                </tr>
                <span>
                    <td>Email</td>
                    <td>:</td>
                    <td><span class="email"></span></td>
                </tr>
            </table>
        </div>
        <div class="modal-footer">
            <button class="btn red modal-close waves-effect waves-red">Kembali</button>
        </div>
    </form>
</div>

<div id="password" class="modal modal-fixed-footer">
    <form action="/user/password" method="POST">
        {{ csrf_field() }}
        <input type="hidden" name="id" id="id">
        <div class="modal-content">
            <h4>Ganti Password</h4>
            <div class="row">
                <div class="input-field col s12">
                    <input type="password" name="password" id="password" class="validate" value="{{ old('password') }}" placeholder=" ">
                    <label for="password">Masukkan Password Baru</label>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <a class="btn red modal-close waves-effect waves-red">Kembali</a>
            <button type="submit" class="btn bc-blue-bg modal-close waves-effect waves-red">Perbarui</button>
        </div>
    </form>
</div>
@endsection

@section('js')
<script type="text/javascript">
    $(document).ready( function () {
        $('#user_datatable').DataTable({
            processing: true,
            serverSide: true,
            scrollX: true,
            ajax: "{{ url('user/getUsers') }}",
            columns: [
                        { data: 'DT_RowIndex', name: 'DT_RowIndex', className: 'center', orderable: false, searchable: false },
                        { data: 'id', name: 'id', defaultContent: '-' },
                        { data: 'level_user', name: 'level_user', defaultContent: '-' },
                        { data: 'nama_user', name: 'nama_user', defaultContent: '-' },
                        { data: 'username', name: 'username', defaultContent: '-' },
                        { data: 'email', name: 'email', defaultContent: '-' },
                        { data: 'aksi', name: 'aksi', className: 'center', searchable: false, orderable: false }
                    ]
        });

        $(document).ajaxComplete(function() {
            $('.tooltipped').tooltip();
        });

        // get & set data detail to modal
        $(document).on('click', 'a.btn-detail', function() {
            var id =($(this).attr("data-id"));
            $.ajax({
                type:"GET",
                url:"/user/getDetail/"+id,
                success : function(data) {
                    $('.id').text(data.id);
                    $('.level').text(data.level_user);
                    $('.nama').text(data.nama_user);
                    $('.username').text(data.username);
                    $('.email').text(data.email);
                }
            });
        });

        $(document).on('click', 'a.btn-password', function() {
            var id =($(this).attr("data-id"));
            $('#id').val(id);
        });
    } );
</script>
@endsection
