
@extends('layouts.app')

@extends('navigation.navigation')

@section('css')
<style media="screen">
    /* select {
        display: block;
    } */
</style>
@endsection

@section('content')
<main style="padding-left: 0px !important;">
    <div class="user">
        <div class="container">
            <div class="row">
                <div class="col s12">
                    <h5 class="left">Tambah User</h5>
                </div>
                <div class="col s12">
                    <div class="card">
                        <div class="card-content">
                            <form class="" action="/user/perbarui/{{ $data->id }}" method="post" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col s12" style="padding: 0;">
                                        <div class="input-field col s12 m6">
                                            <select id="level_user" name="level_user">
                                                <option value="" disabled selected>Level User</option>
                                                <option value="admin" {{ $data->level_user == 'admin' ? 'selected' : '' }}>Admin</option>
                                                <option value="kpps" {{ $data->level_user == 'kpps' ? 'selected' : '' }}>KPPS</option>
                                                <option value="saksi" {{ $data->level_user == 'saksi' ? 'selected' : '' }}>Saksi</option>
                                                <option value="visitor" {{ $data->level_user == 'visitor' ? 'selected' : '' }}>Visitor</option>
                                            </select>
                                            <label for="level_user">Level User</label>
                                            @if ($errors->has('level_user'))
                                                <span class="invalid-feedback" role="alert">
                                                    <span class="helper-text red-text text-darken-1" data-error="wrong" data-success="right"><i>* {{ $errors->first('level_user') }}</i></span>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col s12" style="padding: 0;">
                                        <div class="input-field col s12 m6">
                                            <input type="text" class="validate" name="nama_user" value="{{ $data->nama_user }}" placeholder=" ">
                                            <label for="nama_user">Nama User</label>
                                            @if ($errors->has('nama_user'))
                                                <span class="invalid-feedback" role="alert">
                                                    <span class="helper-text red-text text-darken-1" data-error="wrong" data-success="right"><i>* {{ $errors->first('nama_user') }}</i></span>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="input-field col s12 m6">
                                        <input type="email" name="email" id="email" class="validate" value="{{ $data->email }}">
                                        <label for="email">Email</label>
                                        @if ($errors->has('email'))
                                            <span class="invalid-feedback" role="alert">
                                                <span class="helper-text red-text text-darken-1" data-error="wrong" data-success="right"><i>* {{ $errors->first('email') }}</i></span>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="input-field col s12 m6">
                                        <input type="text" name="username" id="username" class="validate" value="{{ $data->username }}" readonly>
                                        <label for="username">Username</label>
                                        @if ($errors->has('username'))
                                            <span class="invalid-feedback" role="alert">
                                                <span class="helper-text red-text text-darken-1" data-error="wrong" data-success="right"><i>* {{ $errors->first('username') }}</i></span>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="input-field col s12">
                                        <button type="submit" class="btn bc-blue-bg right">Simpan</button>
                                        <a href="/user" class="btn bc-red-bg right" style="margin-right: 10px">Kembali</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection

@section('js')
<script type="text/javascript">
    $(document).ready( function () {
        $('#email').focusout(function() {
            var val = this.value;
            val = val.substr(0, val.indexOf('@'));
            $('#username').val(val);
        });

        $('.datepicker').datepicker({
            format: 'yyyy'
        });

        $('.yearpicker').yearpicker();
    });
</script>
@endsection
