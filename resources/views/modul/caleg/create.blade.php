@extends('layouts.app')

@extends('navigation.navigation')

@section('css')
<style media="screen">
    /* select {
        display: block;
    } */
</style>
@endsection

@section('content')
<main style="padding-left: 0px !important;">
    <div class="user">
        <div class="container">
            <div class="row">
                <div class="col s12">
                    <h5 class="left">Tambah Caleg</h5>
                </div>
                <div class="col s12">
                    <div class="card">
                        <div class="card-content">
                            <form class="" action="/caleg/simpan" method="post" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col s12" style="padding: 0;">
                                        <div class="input-field col s12 m6">
                                            <select id="partai" name="partai">
                                                <option value="" disabled selected>Pilih Partai</option>
                                                @foreach ($partai as $p)
                                                <option value="{{ $p->id }}" {{ old('partai') == $p->id ? 'selected' : '' }}>{{ $p->nama_partai }}</option>
                                                @endforeach
                                            </select>
                                            <label for="partai">Partai</label>
                                            @if ($errors->has('partai'))
                                                <span class="invalid-feedback" role="alert">
                                                    <span class="helper-text red-text text-darken-1" data-error="wrong" data-success="right"><i>* {{ $errors->first('partai') }}</i></span>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="input-field col s12 m6">
                                            <input type="text" name="pendidikan" id="pendidikan" class="validate" value="{{ old('pendidikan') }}">
                                            <label for="pendidikan">Pendidikan</label>
                                            @if ($errors->has('pendidikan'))
                                                <span class="invalid-feedback" role="alert">
                                                    <span class="helper-text red-text text-darken-1" data-error="wrong" data-success="right"><i>* {{ $errors->first('pendidikan') }}</i></span>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col s12" style="padding: 0;">
                                        <div class="input-field col s12 m6">
                                            <select id="dapil" name="dapil">
                                                <option value="" disabled selected>Pilih Dapil</option>
                                                @foreach ($dapils as $d)
                                                    <option value="{{ $d->id }}" {{ old('dapil') == $d->id ? 'selected' : '' }}>{{ $d->nama_dapil }}</option>
                                                @endforeach
                                            </select>
                                            <label for="dapil">Dapil</label>
                                            @if ($errors->has('dapil'))
                                                <span class="invalid-feedback" role="alert">
                                                    <span class="helper-text red-text text-darken-1" data-error="wrong" data-success="right"><i>* {{ $errors->first('dapil') }}</i></span>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="input-field col s12 m6">
                                            <select id="pemilihan" name="pemilihan">
                                                <option value="" disabled selected>Pilih Pemilihan</option>
                                                <option value="1" {{ old('pemilihan') == '1' ? 'selected' : '' }}>DPR</option>
                                                <option value="2" {{ old('pemilihan') == '2' ? 'selected' : '' }}>DPD</option>
                                                <option value="3" {{ old('pemilihan') == '3' ? 'selected' : '' }}>DPRD Provinsi</option>
                                                <option value="4" {{ old('pemilihan') == '4' ? 'selected' : '' }}>DPRD Kabupaten/Kota</option>
                                            </select>
                                            <label for="pemilihan">Pemilihan</label>
                                            @if ($errors->has('pemilihan'))
                                                <span class="invalid-feedback" role="alert">
                                                    <span class="helper-text red-text text-darken-1" data-error="wrong" data-success="right"><i>* {{ $errors->first('pemilihan') }}</i></span>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col s12" style="padding: 0;">
                                        <div class="input-field col s12 m6">
                                            <input type="text" name="nama_caleg" id="nama_caleg" class="validate" value="{{ old('nama_caleg') }}">
                                            <label for="nama_caleg">Nama</label>
                                            @if ($errors->has('nama_caleg'))
                                                <span class="invalid-feedback" role="alert">
                                                    <span class="helper-text red-text text-darken-1" data-error="wrong" data-success="right"><i>* {{ $errors->first('nama_caleg') }}</i></span>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="input-field col s12 m6">
                                            <input type="text" name="pekerjaan" id="pekerjaan" class="validate" value="{{ old('pekerjaan') }}">
                                            <label for="pekerjaan">Pekerjaan</label>
                                            @if ($errors->has('pekerjaan'))
                                                <span class="invalid-feedback" role="alert">
                                                    <span class="helper-text red-text text-darken-1" data-error="wrong" data-success="right"><i>* {{ $errors->first('pekerjaan') }}</i></span>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col s12" style="padding: 0;">
                                        <div class="col s12 m6" style="padding: 0">
                                            <div class="input-field col s12">
                                                <select id="jenis_kelamin" name="jenis_kelamin">
                                                    <option value="" disabled selected>Pilih Jenis Kelamin</option>
                                                    <option value="1" {{ old('jenis_kelamin') == '1' ? 'selected' : '' }}>Laki - Laki</option>
                                                    <option value="2" {{ old('jenis_kelamin') == '2' ? 'selected' : '' }}>Perempuan</option>
                                                </select>
                                                <label for="jenis_kelamin">Jenis Kelamin</label>
                                                @if ($errors->has('jenis_kelamin'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <span class="helper-text red-text text-darken-1" data-error="wrong" data-success="right"><i>* {{ $errors->first('jenis_kelamin') }}</i></span>
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="input-field col s12">
                                                <input type="text" name="tanggal_lahir" id="tanggal_lahir" class="datepicker" value="{{ old('tanggal_lahir') }}">
                                                <label for="tanggal_lahir">Tanggal Lahir</label>
                                                @if ($errors->has('tanggal_lahir'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <span class="helper-text red-text text-darken-1" data-error="wrong" data-success="right"><i>* {{ $errors->first('tanggal_lahir') }}</i></span>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col s12 m6" style="padding: 0">
                                            <div class="input-field col s12">
                                                <textarea id="motivasi" name="motivasi" class="materialize-textarea">{{ old('motivasi') }}</textarea>
                                                <label for="motivasi">Motivasi</label>
                                                @if ($errors->has('motivasi'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <span class="helper-text red-text text-darken-1" data-error="wrong" data-success="right"><i>* {{ $errors->first('motivasi') }}</i></span>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col s12" style="padding: 0;">
                                        <div class="col s12 m6" style="padding: 0">
                                            <div class="input-field col s12">
                                                <select id="agama" name="agama">
                                                    <option value="" disabled selected>Pilih Agama</option>
                                                    <option value="islam" {{ old('agama') == 'islam' ? 'selected' : '' }}>Islam</option>
                                                    <option value="protestan" {{ old('agama') == 'protestan' ? 'selected' : '' }}>Protestan</option>
                                                    <option value="katolik" {{ old('agama') == 'katolik' ? 'selected' : '' }}>Katolik</option>
                                                    <option value="hindu" {{ old('agama') == 'hindu' ? 'selected' : '' }}>Hindu</option>
                                                    <option value="budha" {{ old('agama') == 'budha' ? 'selected' : '' }}>Buddha</option>
                                                    <option value="khonghucu" {{ old('agama') == 'khonghucu' ? 'selected' : '' }}>Khonghucu</option>
                                                </select>
                                                <label for="agama">Agama</label>
                                                @if ($errors->has('agama'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <span class="helper-text red-text text-darken-1" data-error="wrong" data-success="right"><i>* {{ $errors->first('agama') }}</i></span>
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="input-field col s12">
                                                <select id="status_pernikahan" name="status_pernikahan">
                                                    <option value="" disabled selected>Pilih Status Pernikahan</option>
                                                    <option value="lajang" {{ old('status_pernikahan') == 'lajang' ? 'selected' : '' }}>Lajang</option>
                                                    <option value="menikah" {{ old('status_pernikahan') == 'menikah' ? 'selected' : '' }}>Menikah</option>
                                                </select>
                                                <label for="status_pernikahan">Status Pernikahan</label>
                                                @if ($errors->has('status_pernikahan'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <span class="helper-text red-text text-darken-1" data-error="wrong" data-success="right"><i>* {{ $errors->first('status_pernikahan') }}</i></span>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col s12 m6" style="padding: 0">
                                            <div class="input-field col s12">
                                                <textarea id="sasaran" name="sasaran" class="materialize-textarea">{{ old('sasaran') }}</textarea>
                                                <label for="sasaran">Sasaran</label>
                                                @if ($errors->has('sasaran'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <span class="helper-text red-text text-darken-1" data-error="wrong" data-success="right"><i>* {{ $errors->first('sasaran') }}</i></span>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col s12" style="padding: 0;">
                                        <div class="input-field col s12 m6">
                                            <textarea id="alamat" name="alamat" class="materialize-textarea">{{ old('alamat') }}</textarea>
                                            <label for="alamat">Alamat</label>
                                            @if ($errors->has('alamat'))
                                                <span class="invalid-feedback" role="alert">
                                                    <span class="helper-text red-text text-darken-1" data-error="wrong" data-success="right"><i>* {{ $errors->first('alamat') }}</i></span>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="file-field input-field col s12 m6">
                                        <div class="btn bc-blue-bg">
                                            <span>Upload Foto</span>
                                            <input type="file" name="foto">
                                        </div>
                                        <div class="file-path-wrapper">
                                            <input class="file-path validate" type="text" placeholder="Upload Foto">
                                        </div>
                                        @if ($errors->has('foto'))
                                            <span class="invalid-feedback" role="alert">
                                                <span class="helper-text red-text text-darken-1" data-error="wrong" data-success="right"><i>* {{ $errors->first('foto') }}</i></span>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="input-field col s12">
                                        <button type="submit" class="btn bc-blue-bg right">Simpan</button>
                                        <a href="/caleg" class="btn bc-red-bg right" style="margin-right: 10px">Kembali</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection

@section('js')
<script type="text/javascript">
    $(document).ready( function () {

    });
</script>
@endsection
