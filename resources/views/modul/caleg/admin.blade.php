@extends('layouts.app')

@extends('navigation.navigation')

@section('css')
<style media="screen">
    select {
        display: block;
    }

    table {
        overflow: auto hidden !important;
        display: block !important;
        width: 100% !important;
    }
</style>
@endsection

@section('content')
<main style="padding-left: 0px !important;">
    <div class="user">
        <div class="container">
            <div class="row">
                <div class="col s12">
                    <h5 class="left">Caleg</h5>
                    <a href="/caleg/tambah" class="btn bc-blue-bg btn-add right">Tambah</a>
                </div>
                <div class="col s12">
                    <div class="card">
                        <div class="card-content">
                            <table id="caleg_datatable" class="display">
                                <thead>
                                    <tr>
                                        <th>No</td>
                                        <th>ID Caleg</td>
                                        <th>Partai</th>
                                        <th>Dapil</th>
                                        <th>Nama</th>
                                        <th>Jenis Kelamin</th>
                                        <th>Tanggal Lahir</th>
                                        <th>Agama</th>
                                        <th>Status Pernikahan</th>
                                        <th class="center" style="min-width: 200px !important">Alamat</th>
                                        <th>Pendidikan</th>
                                        <th>Pekerjaan</th>
                                        <th class="center" style="min-width: 200px !important">Motivasi</th>
                                        <th class="center" style="min-width: 200px !important">Target Sasaran</th>
                                        <th class="center" style="min-width: 200px !important">Aksi</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<div id="detail" class="modal modal-fixed-footer">
        <div class="modal-content">
            <h4>Detail</h4>
            <table class="table">
                <tr>
                    <td>ID Caleg</td>
                    <td>:</td>
                    <td><span class="id_caleg"></span></td>
                </tr>
                <tr>
                    <td>Partai</td>
                    <td>:</td>
                    <td><span class="partai"></span></td>
                </tr>
                <tr>
                    <td>Dapil</td>
                    <td>:</td>
                    <td><span class="dapil"></span></td>
                </tr>
                <tr>
                    <td>Nama Caleg</td>
                    <td>:</td>
                    <td><span class="nama_caleg"></span></td>
                </tr>
                <tr>
                    <td>Jenis Kelamin</td>
                    <td>:</td>
                    <td><span class="jk"></span></td>
                </tr>
                <tr>
                    <td>Tanggal Lahir</td>
                    <td>:</td>
                    <td><span class="tl"></span></td>
                </tr>
                <tr>
                    <td>Agama</td>
                    <td>:</td>
                    <td><span class="agama"></span></td>
                </tr>
                <tr>
                    <td>Status Pernikahan</td>
                    <td>:</td>
                    <td><span class="sp"></span></td>
                </tr>
                <tr>
                    <td>Alamat</td>
                    <td>:</td>
                    <td><span class="alamat"></span></td>
                </tr>
                <tr>
                    <td>Pendidikan</td>
                    <td>:</td>
                    <td><span class="pendidikan"></span></td>
                </tr>
                <tr>
                    <td>Pekerjaan</td>
                    <td>:</td>
                    <td><span class="pekerjaan"></span></td>
                </tr>
                <tr>
                    <td>Motivasi</td>
                    <td>:</td>
                    <td><span class="motivasi"></span></td>
                </tr>
                <tr>
                    <td>Target Sasaran</td>
                    <td>:</td>
                    <td><span class="sasaran"></span></td>
                </tr>
            </table>
        </div>
        <div class="modal-footer">
            <button class="btn red modal-close waves-effect waves-red">Kembali</button>
        </div>
    </form>
</div>
@endsection

@section('js')
<script type="text/javascript">
    $(document).ready( function () {
        $('#caleg_datatable').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ url('caleg/getCalegs') }}",
            columns: [
                        { data: 'DT_RowIndex', name: 'DT_RowIndex', className: 'center', orderable: false, searchable: false },
                        { data: 'id', name: 'id', defaultContent: '-' },
                        { data: 'partai', name: 'partais.nama_partai', defaultContent: '-' },
                        { data: 'dapil', name: 'dapils.nama_dapil', defaultContent: '-' },
                        { data: 'nama_caleg', name: 'nama_caleg', defaultContent: '-' },
                        { data: 'jenis_kelamin', name: 'jenis_kelamin', defaultContent: '-' },
                        { data: 'tanggal_lahir', name: 'tanggal_lahir', defaultContent: '-' },
                        { data: 'agama', name: 'agama', defaultContent: '-' },
                        { data: 'status_perkawinan', name: 'status_perkawinan', defaultContent: '-' },
                        { data: 'alamat', name: 'alamat', defaultContent: '-' },
                        { data: 'pendidikan', name: 'pendidikan', defaultContent: '-' },
                        { data: 'pekerjaan', name: 'pekerjaan', defaultContent: '-' },
                        { data: 'motivasi', name: 'motivasi', defaultContent: '-' },
                        { data: 'sasaran', name: 'sasaran', defaultContent: '-' },
                        { data: 'aksi', name: 'aksi', className: 'center', searchable: false, orderable: false, width: '200px' }
                    ]
        });

        $(document).ajaxComplete(function() {
            $('.tooltipped').tooltip();
        });

        // get & set data detail to modal
        $(document).on('click', 'a.btn-detail', function() {
            var id =($(this).attr("data-id"));
            $.ajax({
                type:"GET",
                url:"/caleg/getDetail/"+id,
                success : function(data) {
                    if (data.jenis_kelamin == 1) {
                        var jk = 'Laki - Laki';
                    }
                    else {
                        var jk = 'Perempuan';
                    }
                    $('.id_caleg').text(data.id);
                    $('.partai').text(data.partai);
                    $('.dapil').text(data.dapil);
                    $('.nama_caleg').text(data.nama_caleg);
                    $('.jk').text(jk);
                    $('.tl').text(data.tanggal_lahir);
                    $('.agama').text(data.agama);
                    $('.sp').text(data.status_perkawinan);
                    $('.alamat').text(data.alamat);
                    $('.pendidikan').text(data.pendidikan);
                    $('.pekerjaan').text(data.pekerjaan);
                    $('.motivasi').text(data.motivasi);
                    $('.sasaran').text(data.sasaran);
                }
            });
        });
    } );
</script>
@endsection
