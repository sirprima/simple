@extends('layouts.app')

@extends('navigation.navigation')

@section('css')
<style media="screen">
    /* select {
        display: block;
    } */
</style>
@endsection

@section('content')
<ul class="sidenav sidenav-fixed" id="mobile-demo">
    <div class="row" style="margin-top: 50px;">
        <div class="col s10 offset-s1">
            <form id="cariCaleg">
                {{-- {{ csrf_field() }} --}}
                <div class="input-field">
                    <select id="pemilihan">
                        <option value="" selected>Pilih Pemilihan</option>
                        <option value="1">DPR</option>
                        <option value="2">DPD</option>
                        <option value="3">DPRD Provinsi</option>
                        <option value="4">DPRD Kabupaten/Kota</option>
                    </select>
                </div>
                <div class="input-field">
                    <select id="provinsi">
                        <option value="" selected>Pilih Provinsi</option>
                        <option value="1">Sumatera Utara</option>
                    </select>
                </div>
                <div class="input-field">
                    <select id="dapil">
                        <option value="" selected>Pilih Dapil</option>
                        @foreach ($dapil as $d)
                            <option value="{{$d->id}}">{{$d->nama_dapil}}</option>
                        @endforeach
                    </select>
                </div>
                <div>
                    <button class="btn bc-blue-bg right btn-cari">Cari</button>
                </div>
            </form>
        </div>
    </div>
</ul>

<main>
    <div class="user">
        <div class="container">
            <div class="row">
                <div class="col s12">
                    <div class="card">
                        <div class="card-content">
                            <div class="row card-box">
                                @foreach ($caleg as $c)
                                    <div class="col s3">
                                        <div class="card" style="max-heigt: 100px;">
                                            <div class="card-image waves-effect waves-block waves-light">
                                                <img height="255px" class="activator" src="assets/images/content/{{ $c->foto }}">
                                            </div>
                                            <div class="card-content">
                                                <p class="center"><a href="/info-caleg/detil/{{ $c->id }}">{{ $c->nama_caleg }}</a></p>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection

@section('js')
<script type="text/javascript">
    $(document).ready( function () {
        $('.btn-cari').click(function(e){
            var provinsi =($('#provinsi').val());
            var dapil =($('#dapil').val());
            var pemilihan =($('#pemilihan').val());

            if (provinsi == "") {
                provinsi = 'null';
            }

            if (dapil == '') {
                dapil = 'null';
            }

            if (pemilihan == '') {
                pemilihan = 'null';
            }

            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });
            $.ajax({
                url: "/info-caleg/cari/"+provinsi+"/"+dapil+"/"+pemilihan,
                method: 'GET',
                success: function(data){

                    $('.card-box').empty();
                    if (data.length == 0) {
                        $('.card-box').append(
                            '<div class="col s12 bc-red-bg" style="padding: 10px; border-radius: 5px;">'+
                                '<p class="center white-text">Data tidak ditemukan.</p>'+
                            '</div>'
                        );
                    }
                    for (var i = 0; i < data.length; i++) {
                        $('.card-box').append(
                            '<div class="col s3">'+
                                '<div class="card" style="max-heigt: 100px;">'+
                                    '<div class="card-image waves-effect waves-block waves-light">'+
                                        '<img height="255px" class="activator" src="assets/images/content/'+ data[i].foto +'">'+
                                    '</div>'+
                                    '<div class="card-content">'+
                                        '<p class="center"><a href="/info-caleg/detil/'+ data[i].id +'">'+ data[i].nama_caleg +'</a></p>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'
                        )
                    }
                }
            });
        });
    } );
</script>
@endsection
