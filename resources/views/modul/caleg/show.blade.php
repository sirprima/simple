@extends('layouts.app')

@extends('navigation.navigation')

@section('css')
<style media="screen">
    .center-img {
        left: 50%;
        position: relative;
        transform: translateX(-50%);
        width: 200px;
        height: auto;
    }

    .tabs .tab a {
        color: #fff;
    }

    .tabs .tab a:hover, .tabs .tab a.active {
        color: #E6E6E6;
    }

    .tabs .indicator {
        background-color: #046D93;
    }
</style>
@endsection

@section('content')
<ul class="sidenav sidenav-fixed" id="mobile-demo">
    <div class="row" style="margin-top: 50px;">
        <div class="col s10 offset-s1">
            <form id="cariCaleg">
                {{-- {{ csrf_field() }} --}}
                <div class="input-field">
                    <select id="pemilihan">
                        <option value="" disabled selected>Pemilihan</option>
                        <option value="DPR">DPR</option>
                        <option value="DPD">DPD</option>
                        <option value="DPRD Provinsi">DPRD Provinsi</option>
                        <option value="DPRD Kabupaten/Kota">DPRD Kabupaten/Kota</option>
                    </select>
                </div>
                <div class="input-field">
                    <select id="provinsi">
                        <option value="" disabled selected>Provinsi</option>
                        <option value="1">Sumatera Utara</option>
                    </select>
                </div>
                <div class="input-field">
                    <select id="dapil">
                        <option value="" disabled selected>Daerah Pemilihan</option>
                        @foreach ($dapil as $d)
                            <option value="{{$d->id}}">{{$d->nama_dapil}}</option>
                        @endforeach
                    </select>
                </div>
                <div>
                    <button class="btn bc-blue-bg right btn-cari">Cari</button>
                </div>
            </form>
        </div>
    </div>
</ul>

<main>
    <div class="user">
        <div class="container">
            <div class="row">
                <div class="col s12">
                    <div class="card">
                        <div class="card-content">
                            <img height="255px" class="center-img" src="{{ asset('assets/images/nopic-01.jpg') }}">
                            <p class="center"><a href="/info-calegd/detil/1">Nama Caleg 1</a></p>

                            <div class="row" style="margin-top: 50px;">
                                <div class="col s8 offset-s2">
                                    <ul class="tabs bc-blue-bg">
                                        <li class="tab col s6"><a class="active" href="#data">Data Diri</a></li>
                                        <li class="tab col s6"><a href="#rekam">Rekam Jejak</a></li>
                                    </ul>
                                </div>

                                <div id="data" class="col s8 offset-s2">
                                    <table>
                                        <tr>
                                            <td style="width: 30%;">Pemilihan</td>
                                            <td style="width: 5%;">:</td>
                                            <td>test</td>
                                        </tr>
                                        <tr>
                                            <td style="width: 30%;">Distrik</td>
                                            <td style="width: 5%;">:</td>
                                            <td>test</td>
                                        </tr>
                                        <tr>
                                            <td style="width: 30%;">Usia</td>
                                            <td style="width: 5%;">:</td>
                                            <td>test</td>
                                        </tr>
                                    </table>
                                </div>

                                <div id="rekam" class="col s8 offset-s2">
                                    <table>
                                        <tr>
                                            <td style="width: 20%;">2003 - 2009</td>
                                            <td>:</td>
                                            <td>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Libero repellendus veniam dolor, error asperiores molestiae incidunt, tempore dolorum quos hic nihil ipsam provident harum excepturi vitae corrupti repellat illum? Animi.</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection

@section('js')
    <script>
        $(document).ready(function(){
            $('.tabs').tabs();
        });
    </script>
@endsection
