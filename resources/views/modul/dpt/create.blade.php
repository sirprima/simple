@extends('layouts.app')

@extends('navigation.navigation')

@section('css')
<style media="screen">
    /* select {
        display: block;
    } */
</style>
@endsection

@section('content')
<main style="padding-left: 0px !important;">
    <div class="user">
        <div class="container">
            <div class="row">
                <div class="col s12">
                    <h5 class="left">Tambah DPT</h5>
                </div>
                <div class="col s12">
                    <div class="card">
                        <div class="card-content">
                            <form class="" action="/dpt/simpan" method="post" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input type="hidden" name="old-tps" id="old-tps" value="{{ old('tps') }}">
                                <div class="row">
                                    <div class="col s12" style="padding: 0;">
                                        <div class="input-field col s12 m6">
                                            <select id="dapil" name="dapil">
                                                <option value="" disabled selected>Pilih Dapil</option>
                                                @foreach ($dapils as $d)
                                                    <option value="{{ $d->id }}" {{ old('dapil') == $d->id ? 'selected' : '' }}>{{ $d->nama_dapil }}</option>
                                                @endforeach
                                            </select>
                                            <label for="dapil">Dapil</label>
                                            @if ($errors->has('dapil'))
                                                <span class="invalid-feedback" role="alert">
                                                    <span class="helper-text red-text text-darken-1" data-error="wrong" data-success="right"><i>* {{ $errors->first('dapil') }}</i></span>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col s12" style="padding: 0;">
                                        <div class="input-field col s12 m6">
                                            <select id="tps" name="tps">
                                                <option value="" disabled selected>Pilih TPS</option>
                                            </select>
                                            <label for="tps">TPS</label>
                                            @if ($errors->has('tps'))
                                                <span class="invalid-feedback" role="alert">
                                                    <span class="helper-text red-text text-darken-1" data-error="wrong" data-success="right"><i>* {{ $errors->first('tps') }}</i></span>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col s12" style="padding: 0;">
                                        <div class="input-field col s12 m6">
                                            <input type="text" class="validate" name="nik" id="nik" value="{{ old('nik') }}">
                                            <label for="nik">NIK</label>
                                            @if ($errors->has('nik'))
                                                <span class="invalid-feedback" role="alert">
                                                    <span class="helper-text red-text text-darken-1" data-error="wrong" data-success="right"><i>* {{ $errors->first('nik') }}</i></span>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col s12" style="padding: 0;">
                                        <div class="input-field col s12 m6">
                                            <input type="text" class="validate" name="nama" id="nama" value="{{ old('nama') }}">
                                            <label for="nama">Nama</label>
                                            @if ($errors->has('nama'))
                                                <span class="invalid-feedback" role="alert">
                                                    <span class="helper-text red-text text-darken-1" data-error="wrong" data-success="right"><i>* {{ $errors->first('nama') }}</i></span>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col s12" style="padding: 0;">
                                        <div class="input-field col s12 m6">
                                            <select id="jenis_kelamin" name="jenis_kelamin">
                                                <option value="" disabled selected>Pilih Jenis Kelamin</option>
                                                <option value="1" {{ old('jenis_kelamin') == '1' ? 'selected' : '' }}>Laki - Laki</option>
                                                <option value="2" {{ old('jenis_kelamin') == '2' ? 'selected' : '' }}>Perempuan</option>
                                            </select>
                                            <label for="jenis_kelamin">Jenis Kelamin</label>
                                            @if ($errors->has('jenis_kelamin'))
                                                <span class="invalid-feedback" role="alert">
                                                    <span class="helper-text red-text text-darken-1" data-error="wrong" data-success="right"><i>* {{ $errors->first('jenis_kelamin') }}</i></span>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="input-field col s12">
                                        <button type="submit" class="btn bc-blue-bg right">Simpan</button>
                                        <a href="/dpt" class="btn bc-red-bg right" style="margin-right: 10px">Kembali</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection

@section('js')
<script type="text/javascript">
    $(document).ready( function () {
        $('#dapil').on('change', function() {
            $('#tps').children('option:not(:first)').remove();
            getTps(this.value);
        });

        if ($('#dapil').val() != 0) {
            getTps($('#dapil').val());
        }

        function getTps(id) {
            $.ajax({
                url: 'getTps/'+id,
                type: 'get',
                dataType: 'json',
                success: function(response) {
                    var length = 0;
                    if (response['data'] != null) {
                        length = response['data'].length;
                    }
                    var data = '';
                    if (length > 0) {
                        for(i = 0; i < length; i++) {
                            data = new Option(response['data'][i].nama_tps, response['data'][i].id);
                            if ($('#old-tps').val() == response['data'][i].id) {
                                data.setAttribute('selected', 'selected');
                            }
                            $("#tps").append(data);
                        }
                    }
                    $("#tps").formSelect();
                }
            });
        };
    });
</script>
@endsection
