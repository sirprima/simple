@extends('layouts.app')

@extends('navigation.navigation')

@section('css')
<style media="screen">
    select {
        display: block;
    }
</style>
@endsection

@section('content')
<ul class="sidenav sidenav-fixed" id="mobile-demo">
    <div class="row" style="margin-top: 50px;">
        <div class="col s10 offset-s1">
            <form id="cariDpt">
                <div class="input-field">
                    <input type="text" name="nik" class="validation" id="nik">
                    <label for="nik">NIK</label>
                </div><div class="input-field">
                    <input type="text" name="nama" class="validation" id="nama">
                    <label for="nama">Nama Lengkap</label>
                </div>
                <div>
                    <button class="btn bc-blue-bg btn-cari right">Cari</button>
                </div>
            </form>
        </div>
    </div>
</ul>

<main>
    <div class="user">
        <div class="container">
            <div class="row">
                <div class="col s12">
                    <div class="card">
                        <div class="card-content">
                            {{-- <h6 class="center">Silahkan isi NIK dan Nama Lengkap anda pada form yang tersedia, lalu klik tombol Cari.</h6> --}}
                            <div class="row">
                                <div class="col s4">
                                    <table>
                                        <tr>
                                            <td>Nama</td>
                                            <td>:</td>
                                            <td><span class="nama"></span></td>
                                        </tr>
                                        <tr>
                                            <td>Jenis Kelamin</td>
                                            <td>:</td>
                                            <td><span class="jk"></span></td>
                                        </tr>
                                        <tr>
                                            <td>Provinsi</td>
                                            <td>:</td>
                                            <td><span class="provinsi"></span></td>
                                        </tr>
                                        <tr>
                                            <td>Kabupaten/Kota</td>
                                            <td>:</td>
                                            <td><span class="kota"></span></td>
                                        </tr>
                                        <tr>
                                            <td>Kecamatan</td>
                                            <td>:</td>
                                            <td><span class="kecamatan"></span></td>
                                        </tr>
                                        <tr>
                                            <td>Kelurahan</td>
                                            <td>:</td>
                                            <td><span class="kelurahan"></span></td>
                                        </tr>
                                        <tr>
                                            <td>Lokasi TPS</td>
                                            <td>:</td>
                                            <td>
                                                <span class="tps"></span>
                                                <form action="/info-tps" style="margin-block-end: 0 !important" method="POST" id="view-tps">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="tps" value="" id="id_tps">
                                                    <button type="submit" class="btn bc-blue-bg btn-view" style="margin-left: 50px;">Lihat</button>
                                                </form>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection

@section('js')
<script type="text/javascript">
    $(document).ready( function () {
        $('.btn-cari').click(function(e){
            var nik =($('#nik').val());
            var nama =($('#nama').val());

            if (nik == "") {
                nik = 'null';
            }

            if (nama == '') {
                nama = 'null';
            }

            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });
            $.ajax({
                url: "/info-dpt/cari/"+nik+"/"+nama,
                method: 'GET',
                success: function(data){
                    if (data.nama_dpt != undefined) {
                        $('.nama').text(data.nama_dpt);
                        $('.jk').text(data.jenis_kelamin);
                        $('.provinsi').text('Sumatera Utara');
                        $('.kota').text('Medan');
                        $('.kecamatan').text(data.kecamatan);
                        $('.kelurahan').text(data.kelurahan);
                        $('.tps').text(data.tps);
                        $('#id_tps').val(data.id_tps);
                    }
                }
            });
        });
    } );
</script>
@endsection
