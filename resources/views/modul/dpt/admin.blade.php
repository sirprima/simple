@extends('layouts.app')

@extends('navigation.navigation')

@section('css')
<style media="screen">
    select {
        display: block;
    }
</style>
@endsection

@section('content')
<main style="padding-left: 0px !important;">
    <div class="user">
        <div class="container">
            <div class="row">
                <div class="col s12">
                    <h5 class="left">DPT</h5>
                    <a href="/dpt/tambah" class="btn bc-blue-bg btn-add right">Tambah</a>
                </div>
                <div class="col s12">
                    <div class="card">
                        <div class="card-content">
                            <table id="dpt_datatable" class="display" style="width: 100%">
                                <thead>
                                    <tr>
                                        <th>No</td>
                                        <th>ID DPT</td>
                                        <th>Dapil</th>
                                        <th>TPS</th>
                                        <th>ID User</th>
                                        <th>NIK</th>
                                        <th>Nama</th>
                                        <th>Jenis Kelamin</th>
                                        <th class="center">Aksi</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<div id="detail" class="modal modal-fixed-footer">
        <div class="modal-content">
            <h4>Detail</h4>
            <table class="table">
                <tr>
                    <td>ID DPT</td>
                    <td>:</td>
                    <td><span class="id_dpt"></span></td>
                </tr>
                <tr>
                    <td>Dapil</td>
                    <td>:</td>
                    <td><span class="dapil"></span></td>
                </tr>
                <tr>
                    <td>TPS</td>
                    <td>:</td>
                    <td><span class="tps"></span></td>
                </tr>
                <tr>
                    <td>ID User</td>
                    <td>:</td>
                    <td><span class="id_user"></span></td>
                </tr>
                <tr>
                    <td>NIK</td>
                    <td>:</td>
                    <td><span class="nik"></span></td>
                </tr>
                <tr>
                    <td>Nama</td>
                    <td>:</td>
                    <td><span class="nama"></span></td>
                </tr>
                <tr>
                    <td>Jenis Kelamin</td>
                    <td>:</td>
                    <td><span class="jenis_kelamin"></span></td>
                </tr>
            </table>
        </div>
        <div class="modal-footer">
            <button class="btn red modal-close waves-effect waves-red">Kembali</button>
        </div>
    </form>
</div>
@endsection

@section('js')
<script type="text/javascript">
    $(document).ready( function () {
        $('#dpt_datatable').DataTable({
            processing: true,
            serverSide: true,
            scrollX: true,
            ajax: "{{ url('dpt/getDpts') }}",
            columns: [
                        { data: 'DT_RowIndex', name: 'DT_RowIndex', className: 'center', orderable: false, searchable: false },
                        { data: 'id', name: 'id', defaultContent: '-' },
                        { data: 'dapil', name: 'dapils.nama_dapil', defaultContent: '-' },
                        { data: 'tps', name: 'tpses.nama_tps', defaultContent: '-' },
                        { data: 'id_user', name: 'id_user', defaultContent: '-' },
                        { data: 'nik', name: 'nik', defaultContent: '-' },
                        { data: 'nama_dpt', name: 'nama_dpt', defaultContent: '-' },
                        { data: 'jenis_kelamin', name: 'jenis_kelamin', defaultContent: '-' },
                        { data: 'aksi', name: 'aksi', className: 'center', searchable: false, orderable: false, width: '200px' }
                    ]
        });

        $(document).ajaxComplete(function() {
            $('.tooltipped').tooltip();
        });

        // get & set data detail to modal
        $(document).on('click', 'a.btn-detail', function() {
            var id =($(this).attr("data-id"));
            $.ajax({
                type:"GET",
                url:"/dpt/getDetail/"+id,
                success : function(data) {
                    $('.id_dpt').text(data.id);
                    $('.dapil').text(data.dapil);
                    $('.tps').text(data.tps);
                    if (data.jenis_kelamin == 1) {
                        var jk = 'Laki - Laki';
                    }

                    if (data.jenis_kelamin == 2) {
                        var jk = 'Perempuan';
                    }
                    $('.id_user').text(data.id_user);
                    $('.nik').text(data.nik);
                    $('.nama').text(data.nama_dpt);
                    $('.jenis_kelamin').text(jk);
                }
            });
        });
    } );
</script>
@endsection
