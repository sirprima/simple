@extends('layouts.app')

@extends('navigation.navigation')

@section('css')

@endsection

@section('content')
<main>
    <div class="dashboard">
        <div class="container">
            <div class="row">
                <div class="col s12">
                    <h5><i class="material-icons left" style="line-height: 30px;">dashboard</i> Dashboard</h5>
                    <div class="divider"></div>
                </div>
                <div class="col s12 m8">
                    <div class="card">
                        <div class="card-content">

                        </div>
                    </div>
                </div>
                <div class="col s12 m4">
                    <div class="card">
                        <div class="card-content">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection
