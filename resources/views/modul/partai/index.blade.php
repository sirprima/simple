@extends('layouts.app')

@extends('navigation.navigation')

@section('css')
<style media="screen">
    select {
        display: block;
    }
</style>
@endsection

@section('content')
<main style="padding-left: 0px !important;">
    <div class="user">
        <div class="container">
            <div class="row">
                <div class="col s12">
                    <h5 class="left">Partai</h5>
                    <a href="/partai/tambah" class="btn bc-blue-bg btn-add right">Tambah</a>
                </div>
                <div class="col s12">
                    <div class="card">
                        <div class="card-content">
                            <table id="partai_datatable" class="display" style="width: 100%">
                                <thead>
                                    <tr>
                                        <th>No</td>
                                        <th>ID Partai</td>
                                        <th>ID User</th>
                                        <th>No Urut</th>
                                        <th>Nama Partai</th>
                                        <th>Lambang Partai</th>
                                        <th class="center" style="min-width: 200px">Alamat</th>
                                        <th class="center" style="min-width: 200px">Visi</th>
                                        <th class="center" style="min-width: 200px">Misi</th>
                                        <th class="center" style="min-width: 200px">Aksi</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<div id="detail" class="modal modal-fixed-footer">
        <div class="modal-content">
            <h4>Detail</h4>
            <table class="table">
                <tr>
                    <td>ID Partai</td>
                    <td>:</td>
                    <td><span class="id_partai"></span></td>
                </tr>
                <tr>
                    <td>ID User</td>
                    <td>:</td>
                    <td><span class="id_user"></span></td>
                </tr>
                <tr>
                    <td>No Urut</td>
                    <td>:</td>
                    <td class="no_urut"></td>
                </tr>
                <tr>
                    <td>Nama Partai</td>
                    <td>:</td>
                    <td class="nama_partai"></td>
                </tr>
                <tr>
                    <td>Lambang Partai</td>
                    <td>:</td>
                    <td><img src="" class="lambang_partai" width="100px"></td>
                </tr>
                <tr>
                    <td>Alamat</td>
                    <td>:</td>
                    <td><span class="alamat"></span></td>
                </tr>
                <tr>
                    <td>Visi</td>
                    <td>:</td>
                    <td><span class="visi"></span></td>
                </tr>
                <tr>
                    <td>Misi</td>
                    <td>:</td>
                    <td><span class="misi"></span></td>
                </tr>
            </table>
        </div>
        <div class="modal-footer">
            <button class="btn red modal-close waves-effect waves-red">Kembali</button>
        </div>
    </form>
</div>
@endsection

@section('js')
<script type="text/javascript">
    $(document).ready( function () {
        $('#partai_datatable').DataTable({
            processing: true,
            serverSide: true,
            'scrollX': true,
            ajax: "{{ url('partai/getPartais') }}",
            columns: [
                        { data: 'DT_RowIndex', name: 'DT_RowIndex', className: 'center', orderable: false, searchable: false },
                        { data: 'id', name: 'id', defaultContent: '-' },
                        { data: 'id_user', name: 'id_user', defaultContent: '-' },
                        { data: 'no_urut', name: 'no_urut', defaultContent: '-' },
                        { data: 'nama_partai', name: 'nama_partai', defaultContent: '-' },
                        { data: 'lambang_partai', name: 'nama_partai', defaultContent: '-' },
                        { data: 'alamat', name: 'alamat', defaultContent: '-' },
                        { data: 'visi', name: 'visi', defaultContent: '-' },
                        { data: 'misi', name: 'misi', defaultContent: '-' },
                        { data: 'aksi', name: 'aksi', className: 'center', searchable: false, orderable: false, width: '200px' }
                    ]
        });

        $(document).ajaxComplete(function() {
            $('.tooltipped').tooltip();
        });

        // get & set data detail to modal
        $(document).on('click', 'a.btn-detail', function() {
            var id =($(this).attr("data-id"));
            $.ajax({
                type:"GET",
                url:"/partai/getDetail/"+id,
                success : function(data) {
                    $('.id_partai').text(data.id);
                    $('.id_user').text(data.id_user);
                    $('.no_urut').text(data.no_urut);
                    $('.nama_partai').text(data.nama_partai);
                    $('.lambang_partai').attr('src', 'assets/images/content/'+data.lambang_partai);
                    $('.alamat').text(data.alamat);
                    $('.visi').text(data.visi);
                    $('.misi').text(data.misi);
                }
            });
        });
    } );
</script>
@endsection
