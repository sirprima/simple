@extends('layouts.app')

@extends('navigation.navigation')

@section('css')
<style media="screen">
    /* select {
        display: block;
    } */
</style>
@endsection

@section('content')
<main style="padding-left: 0px !important;">
    <div class="user">
        <div class="container">
            <div class="row">
                <div class="col s12">
                    <h5 class="left">Edit Partai</h5>
                </div>
                <div class="col s12">
                    <div class="card">
                        <div class="card-content">
                            <form class="" action="/partai/perbarui/{{ $data->id }}" method="post" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col s12" style="padding: 0;">
                                        <div class="input-field col s12 m6">
                                            <input type="number" class="validate" name="no_urut" id="no_urut" value="{{ $data->no_urut }}">
                                            <label for="no_urut">Nomor Urut</label>
                                            @if ($errors->has('no_urut'))
                                                <span class="invalid-feedback" role="alert">
                                                    <span class="helper-text red-text text-darken-1" data-error="wrong" data-success="right"><i>* {{ $errors->first('no_urut') }}</i></span>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col s12" style="padding: 0;">
                                        <div class="input-field col s12 m6">
                                            <input type="text" class="validate" name="nama" id="nama" value="{{ $data->nama_partai }}">
                                            <label for="nama">Nama Partai</label>
                                            @if ($errors->has('nama'))
                                                <span class="invalid-feedback" role="alert">
                                                    <span class="helper-text red-text text-darken-1" data-error="wrong" data-success="right"><i>* {{ $errors->first('nama') }}</i></span>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="file-field input-field col s12 m6">
                                            <div class="btn bc-blue-bg">
                                                <span>Upload Lambang Partai</span>
                                                <input type="file" name="lambang">
                                            </div>
                                            <div class="file-path-wrapper">
                                                <input class="file-path validate" type="text" placeholder="Upload Lambang Partai" value="{{ $data->lambang_partai }}">
                                            </div>
                                            @if ($errors->has('lambang'))
                                                <span class="invalid-feedback" role="alert">
                                                    <span class="helper-text red-text text-darken-1" data-error="wrong" data-success="right"><i>* {{ $errors->first('lambang') }}</i></span>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="input-field col s12">
                                        <textarea id="alamat" name="alamat" class="materialize-textarea">{{ $data->alamat }}</textarea>
                                        <label for="alamat">Alamat</label>
                                        @if ($errors->has('alamat'))
                                            <span class="invalid-feedback" role="alert">
                                                <span class="helper-text red-text text-darken-1" data-error="wrong" data-success="right"><i>* {{ $errors->first('alamat') }}</i></span>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="input-field col s12">
                                        <textarea id="visi" name="visi" class="materialize-textarea">{{ $data->visi }}</textarea>
                                        <label for="visi">Visi</label>
                                        @if ($errors->has('visi'))
                                            <span class="invalid-feedback" role="alert">
                                                <span class="helper-text red-text text-darken-1" data-error="wrong" data-success="right"><i>* {{ $errors->first('visi') }}</i></span>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="input-field col s12">
                                        <textarea id="misi" name="misi" class="materialize-textarea">{{ $data->misi }}</textarea>
                                        <label for="misi">Misi</label>
                                        @if ($errors->has('misi'))
                                            <span class="invalid-feedback" role="alert">
                                                <span class="helper-text red-text text-darken-1" data-error="wrong" data-success="right"><i>* {{ $errors->first('misi') }}</i></span>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="input-field col s12">
                                        <button type="submit" class="btn bc-blue-bg right">Perbarui</button>
                                        <a href="/partai" class="btn bc-red-bg right" style="margin-right: 10px">Kembali</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection

@section('js')
<script type="text/javascript">
    $(document).ready( function () {

    });
</script>
@endsection
