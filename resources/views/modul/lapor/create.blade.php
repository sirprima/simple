@extends('layouts.app')

@extends('navigation.navigation')

@section('css')
<style media="screen">
    /* select {
        display: block;
    } */
</style>
@endsection

@section('content')
<main style="padding-left: 0px !important;">
    <div class="user">
        <div class="container">
            <div class="row">
                <div class="col s12">
                    <h5 class="left">Tambah Laporan</h5>
                </div>
                <div class="col s12">
                    <div class="card">
                        <div class="card-content">
                            <form class="" action="/lapor/simpan" method="post" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input type="hidden" id="old-kelurahan" value="{{ old('kelurahan') }}">
                                <input type="hidden" id="old-jenis_laporan" value="{{ old('jenis_laporan') }}">
                                <div class="row">
                                    <div class="col s12" style="padding: 0;">
                                        <div class="input-field col s12 m6">
                                            <select id="jenis_laporan" name="jenis_laporan">
                                                <option value="" disabled selected>Pilih Jenis Laporan</option>
                                                <option value="DPT Tidak Terdaftar" {{ old('jenis_laporan') == 'DPT Tidak Terdaftar' ? 'selected' : '' }}>DPT Tidak Terdaftar</option>
                                                <option value="Kecurangan" {{ old('jenis_laporan') == 'Kecurangan' ? 'selected' : '' }}>Kecurangan</option>
                                            </select>
                                            <label for="jenis_laporan">Jenis Laporan</label>
                                            @if ($errors->has('jenis_laporan'))
                                                <span class="invalid-feedback" role="alert">
                                                    <span class="helper-text red-text text-darken-1" data-error="wrong" data-success="right"><i>* {{ $errors->first('jenis_laporan') }}</i></span>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="file-field input-field col s12 m6" id="dtt_foto" style="display:none">
                                            <div class="btn bc-blue-bg">
                                                <span>Upload Bukti Foto</span>
                                                <input type="file" name="foto">
                                            </div>
                                            <div class="file-path-wrapper">
                                                <input class="file-path validate" type="text" placeholder="Upload Bukti Foto">
                                            </div>
                                            @if ($errors->has('foto'))
                                                <span class="invalid-feedback" role="alert">
                                                    <span class="helper-text red-text text-darken-1" data-error="wrong" data-success="right"><i>* {{ $errors->first('foto') }}</i></span>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="file-field input-field col s12 m6" id="kec_kk" style="display:none">
                                            <div class="btn bc-blue-bg">
                                                <span>Upload Kartu Keluarga</span>
                                                <input type="file" name="kk">
                                            </div>
                                            <div class="file-path-wrapper">
                                                <input class="file-path validate" type="text" placeholder="Upload Kartu Keluarga">
                                            </div>
                                            @if ($errors->has('kk'))
                                                <span class="invalid-feedback" role="alert">
                                                    <span class="helper-text red-text text-darken-1" data-error="wrong" data-success="right"><i>* {{ $errors->first('kk') }}</i></span>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col s12" style="padding: 0;">
                                        <div class="input-field col s12 m6">
                                            <select id="provinsi" name="provinsi">
                                                <option value="" disabled selected>Pilih Provinsi</option>
                                                <option value="1" {{ old('provinsi') == '1' ? 'selected' : '' }}>Sumatera Utara</option>
                                            </select>
                                            <label for="provinsi">Provinsi</label>
                                            @if ($errors->has('provinsi'))
                                                <span class="invalid-feedback" role="alert">
                                                    <span class="helper-text red-text text-darken-1" data-error="wrong" data-success="right"><i>* {{ $errors->first('provinsi') }}</i></span>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="file-field input-field col s12 m6" id="dtt_video" style="display:none">
                                            <div class="btn bc-blue-bg">
                                                <span>Upload Bukti Video</span>
                                                <input type="file" name="video">
                                            </div>
                                            <div class="file-path-wrapper">
                                            <input class="file-path validate" type="text" placeholder="Upload Bukti Video">
                                            </div>
                                            @if ($errors->has('video'))
                                                <span class="invalid-feedback" role="alert">
                                                    <span class="helper-text red-text text-darken-1" data-error="wrong" data-success="right"><i>* {{ $errors->first('video') }}</i></span>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="file-field input-field col s12 m6" id="kec_ktp" style="display:none">
                                            <div class="btn bc-blue-bg">
                                                <span>Upload KTP</span>
                                                <input type="file" name="ktp">
                                            </div>
                                            <div class="file-path-wrapper">
                                                <input class="file-path validate" type="text" placeholder="Upload KTP">
                                            </div>
                                            @if ($errors->has('ktp'))
                                                <span class="invalid-feedback" role="alert">
                                                    <span class="helper-text red-text text-darken-1" data-error="wrong" data-success="right"><i>* {{ $errors->first('ktp') }}</i></span>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col s12" style="padding: 0;">
                                        <div class="col s12 m6" style="padding: 0;">
                                            <div class="input-field col s12">
                                                <select id="kota" name="kota">
                                                    <option value="" disabled selected>Pilih Kabupaten/Kota</option>
                                                    <option value="1" {{ old('kota') == '1' ? 'selected' : '' }}>Medan</option>
                                                </select>
                                                <label for="kota">Kabupaten/Kota</label>
                                                @if ($errors->has('kota'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <span class="helper-text red-text text-darken-1" data-error="wrong" data-success="right"><i>* {{ $errors->first('kota') }}</i></span>
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="input-field col s12">
                                                <select id="kecamatan" name="kecamatan">
                                                    <option value="" disabled selected>Pilih Kecamatan</option>
                                                    @foreach ($kecamatans as $kecamatan)
                                                        <option value="{{$kecamatan->id}}" {{ old('kecamatan') == $kecamatan->id ? 'selected' : '' }}>{{$kecamatan->nama_kecamatan}}</option>
                                                    @endforeach
                                                </select>
                                                <label for="kecamatan">Kecamatan</label>
                                                @if ($errors->has('kecamatan'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <span class="helper-text red-text text-darken-1" data-error="wrong" data-success="right"><i>* {{ $errors->first('kecamatan') }}</i></span>
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="input-field col s12">
                                                <select id="kelurahan" name="kelurahan">
                                                    <option value="" disabled selected>Pilih Kelurahan</option>
                                                </select>
                                                <label for="kelurahan">Kelurahan</label>
                                                @if ($errors->has('kelurahan'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <span class="helper-text red-text text-darken-1" data-error="wrong" data-success="right"><i>* {{ $errors->first('kelurahan') }}</i></span>
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="input-field col s12">
                                                <select id="tps" name="tps">
                                                    <option value="" disabled selected>Pilih Lokasi TPS</option>
                                                    {{-- @foreach ($tps as $t)
                                                        <option value="{{ $t->id }}" {{ old('tps') == $t->id ? 'selected' : '' }}>{{ $t->nama_tps }}</option>
                                                    @endforeach --}}
                                                </select>
                                                <label for="tps">Lokasi TPS</label>
                                                @if ($errors->has('tps'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <span class="helper-text red-text text-darken-1" data-error="wrong" data-success="right"><i>* {{ $errors->first('tps') }}</i></span>
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="input-field col s12">
                                                <input type="text" name="tanggal_laporan" class="datepicker" id="tanggal_laporan" value="{{ old('tanggal_laporan') }}">
                                                <label for="tanggal_laporan">Tanggal Laporan</label>
                                                @if ($errors->has('tanggal_laporan'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <span class="helper-text red-text text-darken-1" data-error="wrong" data-success="right"><i>* {{ $errors->first('tanggal_laporan') }}</i></span>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="input-field col s12 m6">
                                            <textarea id="kronologi" name="kronologi" class="materialize-textarea">{{ old('kronologi') }}</textarea>
                                            <label for="kronologi">Kronologi</label>
                                            @if ($errors->has('kronologi'))
                                                <span class="invalid-feedback" role="alert">
                                                    <span class="helper-text red-text text-darken-1" data-error="wrong" data-success="right"><i>* {{ $errors->first('kronologi') }}</i></span>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="input-field">
                                        <button type="submit" class="btn bc-blue-bg right">Simpan</button>
                                        <a href="/lapor" class="btn bc-red-bg right" style="margin-right: 10px">Kembali</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection

@section('js')
<script type="text/javascript">
    $(document).ready( function () {
        if ($('#old-jenis_laporan').val() != null) {
            if ($('#old-jenis_laporan').val() == 'Kecurangan') {
                $('#kec_kk').hide();
                $('#kec_ktp').hide();
                $('#dtt_foto').show();
                $('#dtt_video').show();
            }
            else if ($('#old-jenis_laporan').val() == 'DPT Tidak Terdaftar') {
                $('#kec_kk').show();
                $('#kec_ktp').show();
                $('#dtt_foto').hide();
                $('#dtt_video').hide();
            }
        }

        $('#jenis_laporan').on('change', function() {
            if ($('#jenis_laporan').val() == 'Kecurangan') {
                $('#kec_kk').hide();
                $('#kec_ktp').hide();
                $('#dtt_foto').show();
                $('#dtt_video').show();
            }
            else {
                $('#kec_kk').show();
                $('#kec_ktp').show();
                $('#dtt_foto').hide();
                $('#dtt_video').hide();
            }
        });

        $('#kecamatan').on('change', function() {
            $('#kelurahan').children('option:not(:first)').remove();
            getKelurahans(this.value);
        });

        if ($('#kecamatan').val() != 0) {
            getKelurahans($('#kecamatan').val());
        }

        function getKelurahans(id) {
            $.ajax({
                url: 'getKelurahans/'+id,
                type: 'get',
                dataType: 'json',
                success: function(response) {
                    var length = 0;
                    if (response['data'] != null) {
                        length = response['data'].length;
                    }
                    var data = '';
                    if (length > 0) {
                        for(i = 0; i < length; i++) {
                            data = new Option(response['data'][i].nama_kelurahan, response['data'][i].id);
                            if ($('#old-kelurahan').val() == response['data'][i].id) {
                                data.setAttribute('selected', 'selected');
                            }
                            $("#kelurahan").append(data);
                        }
                    }
                    $("#kelurahan").formSelect();
                }
            });
        };

        $('#kelurahan').on('change', function() {
            $('#tps').children('option:not(:first)').remove();
            getTps(this.value);
        });

        if ($('#kelurahan').val() != 0) {
            getTps($('#kelurahan').val());
        }

        function getTps(id) {
            $.ajax({
                url: 'getTps/'+id,
                type: 'get',
                dataType: 'json',
                success: function(response) {
                    var length = 0;
                    if (response['data'] != null) {
                        length = response['data'].length;
                    }
                    var data = '';
                    if (length > 0) {
                        for(i = 0; i < length; i++) {
                            data = new Option(response['data'][i].nama_tps, response['data'][i].id);
                            if ($('#old-tps').val() == response['data'][i].id) {
                                data.setAttribute('selected', 'selected');
                            }
                            $("#tps").append(data);
                        }
                    }
                    $("#tps").formSelect();
                }
            });
        };
    });
</script>
@endsection
