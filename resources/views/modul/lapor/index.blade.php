@extends('layouts.app')

@extends('navigation.navigation')

@section('css')
<style media="screen">
    select {
        display: block;
    }

    tr {
        border-bottom: none;
    }

    input[readonly] {
        border-bottom: none !important;
        margin: 0 !important;
    }
</style>
@endsection

@section('content')
<main style="padding-left: 0px !important;">
    <div class="user">
        <div class="container">
            <div class="row">
                <div class="col s12">
                    <h5 class="left">Laporan</h5>
                    @if (Auth::user()->level_user == 'visitor')
                    <a href="/lapor/tambah" class="btn bc-blue-bg btn-add right">Tambah</a>
                    @endif
                </div>
                <div class="col s12">
                    <div class="card">
                        <div class="card-content">
                            <table id="laporan_datatable" class="display" style="width: 100%">
                                <thead>
                                    <tr>
                                        <th>No</td>
                                        <th>ID Laporan</td>
                                        <th>Pelapor</th>
                                        <th>Jenis Laporan</th>
                                        <th>Provinsi</th>
                                        <th>Kabupaten/Kota</th>
                                        <th>Kecamatan</th>
                                        <th>Kelurahan</th>
                                        <th>TPS</th>
                                        <th>Surat Domisili/Bukti Foto</th>
                                        <th>KTP/Bukti Video</th>
                                        <th class="center" style="min-width: 200px !important">Kronologi</th>
                                        <th>Status</th>
                                        <th class="center" style="min-width: 200px !important">Aksi</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<div id="detail" class="modal modal-fixed-footer">
    <form action="/lapor/perbarui" method="POST">
        {{ csrf_field() }}
        <input type="hidden" name="id_detail" id="id_detail">
        <div class="modal-content">
            <h4>Detail</h4>
            <table class="table">
                <tr>
                    <td>Pelapor</td>
                    <td>:</td>
                    <td><input type="text" readonly class="id_user"></td>
                </tr>
                <tr>
                    <td>Jenis Laporan</td>
                    <td>:</td>
                    <td><input type="text" readonly class="jenis_laporan"></td>
                </tr>
                <tr>
                    <td>Provinsi</td>
                    <td>:</td>
                    <td><input type="text" readonly class="provinsi"></td>
                </tr>
                <tr>
                    <td>Kabupaten/Kota</td>
                    <td>:</td>
                    <td><input type="text" readonly class="kota"></td>
                </tr>
                <tr>
                    <td>Kecamatan</td>
                    <td>:</td>
                    <td><input type="text" readonly class="kecamatan"></td>
                </tr>
                <tr>
                    <td>Kelurahan</td>
                    <td>:</td>
                    <td><input type="text" readonly class="kelurahan"></td>
                </tr>
                <tr>
                    <td>TPS</td>
                    <td>:</td>
                    <td><input type="text" readonly class="tps"></td>
                </tr>
                <tr>
                    <td><span class="labelA"></span></td>
                    <td>:</td>
                    <td><a href="#" class="btn bc-blue-bg foto" download>Download</a> <span class="foto">asdasd</span></td>
                </tr>
                <tr>
                    <td><span class="labelB"></span></td>
                    <td>:</td>
                    <td><a href="#" class="btn bc-blue-bg video" download>Download</a> <span class="video">asdasd</span></td>
                </tr>
                <tr>
                    <td>Kronologi</td>
                    <td>:</td>
                    <td><input type="text" readonly class="kronologi"></td>
                </tr>

                @if (Auth::user()->level_user != 'visitor')
                <tr>
                    <td>Keterangan</td>
                    <td>:</td>
                    <td>
                        <textarea id="keterangan" name="keterangan" class="materialize-textarea">{{ old('keterangan') }}</textarea>
                        @if ($errors->has('keterangan'))
                            <span class="invalid-feedback" role="alert">
                                <span class="helper-text red-text text-darken-1" data-error="wrong" data-success="right"><i>* {{ $errors->first('keterangan') }}</i></span>
                            </span>
                        @endif
                    </td>
                </tr>
                @endif

                <tr>
                    <td>Status</td>
                    <td>:</td>
                    <td><input type="text" readonly class="status"></td>
                </tr>
            </table>
        </div>
        @if (Auth::user()->level_user != 'visitor')
        <div class="modal-footer">
            <button type="submit" name="action" value="Disetujui" class="btn bc-blue-bg modal-close waves-effect waves-green">Setujui</button>
            <button type="submit" name="action" value="Ditolak" class="btn red modal-close waves-effect waves-red">Tolak</button>
        </div>
        @endif
    </form>
</div>
@endsection

@section('js')
<script type="text/javascript">
    $(document).ready( function () {
        $('#laporan_datatable').DataTable({
            processing: true,
            serverSide: true,
            scrollX: true,
            ajax: "{{ url('lapor/getLaporans') }}",
            columns: [
                        { data: 'DT_RowIndex', name: 'DT_RowIndex', className: 'center', orderable: false, searchable: false },
                        { data: 'id', name: 'id', searchable: false, defaultContent: '-'},
                        { data: 'pelapor', name: 'users.nama_user', searchable: false, defaultContent: '-'},
                        { data: 'jenis_laporan', name: 'jenis_laporan', defaultContent: '-' },
                        { data: 'provinsi', name: 'provinsi', searchable: false, defaultContent: '-'},
                        { data: 'kota', name: 'kota', searchable: false, defaultContent: '-'},
                        { data: 'kecamatan', name: 'kecamatans.nama_kecamatan', defaultContent: '-' },
                        { data: 'kelurahan', name: 'kelurahans.nama_kelurahan', defaultContent: '-' },
                        { data: 'tps', name: 'tpss.nama_tps', defaultContent: '-' },
                        { data: 'bukti_foto', searchable: false, orderable: false, defaultContent: '-'},
                        { data: 'bukti_video', searchable: false, orderable: false, defaultContent: '-'},
                        { data: 'kronologi', name: 'kronologi', defaultContent: '-' },
                        { data: 'status', name: 'status', defaultContent: '-' },
                        { data: 'aksi', name: 'aksi', className: 'center', searchable: false, orderable: false }
                    ]
        });

        // get & set data detail to modal
        $(document).on('click', 'a.btn-detail', function() {
            var id =($(this).attr("data-id"));
            $.ajax({
                type:"GET",
                url:"/lapor/getDetail/"+id,
                success : function(data) {
                    $('#id_detail').val(data.id);
                    $('.id_user').val(data.pelapor);
                    $('.jenis_laporan').val(data.jenis_laporan);
                    $('.provinsi').val('Sumatera Utara');
                    $('.kota').val('Medan');
                    $('.kecamatan').val(data.kecamatan);
                    $('.kelurahan').val(data.kelurahan);
                    $('.tps').val(data.tps);

                    if (data.jenis_laporan == 'Kecurangan') {
                        var urlA = data.bukti_foto;
                        var urlB = data.bukti_video;
                        $('span.labelA ').text('Bukti Foto');
                        $('span.labelB').text('Bukti Video');
                    }
                    else {
                        var urlA = data.kartu_keluarga;
                        var urlB = data.ktp;
                        $('span.labelA ').text('Kartu Keluarga');
                        $('span.labelB').text('KTP');
                    }

                    $('a[href^="#"].foto').attr("href", "assets/images/content/"+urlA);

                    $('a[href^="#"].video').attr("href", "assets/images/content/"+urlB);

                    $('span.foto ').text(urlA);
                    $('span.video').text(urlB);

                    $('.kronologi').val(data.kronologi);
                    $('.status').val(data.status);
                    $('textarea#keterangan').val(data.keterangan);
                }
            });
        });


    } );
</script>
@endsection
