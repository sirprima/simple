@extends('layouts.app')

@extends('navigation.navigation')

@section('css')
<style media="screen">
    /* select {
        display: block;
    } */
</style>
@endsection

@section('content')
<main style="padding-left: 0px !important;">
    <div class="user">
        <div class="container">
            <div class="row">
                <div class="col s12">
                    <h5 class="left">Tambah TPS</h5>
                </div>
                <div class="col s12">
                    <div class="card">
                        <div class="card-content">
                            <form class="" action="/tps/perbarui/{{ $data->id }}" method="post" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col s12" style="padding: 0;">
                                        <div class="input-field col s12 m6">
                                            <select id="dapil" name="dapil">
                                                <option value="" disabled selected>Pilih Dapil</option>
                                                @foreach ($dapils as $d)
                                                    <option value="{{ $d->id }}" {{ $data->id_dapil == $d->id ? 'selected' : '' }}>{{ $d->nama_dapil }}</option>
                                                @endforeach
                                            </select>
                                            <label for="dapil">Dapil</label>
                                            @if ($errors->has('dapil'))
                                                <span class="invalid-feedback" role="alert">
                                                    <span class="helper-text red-text text-darken-1" data-error="wrong" data-success="right"><i>* {{ $errors->first('dapil') }}</i></span>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col s12" style="padding: 0;">
                                        <div class="input-field col s12 m6">
                                            <input type="text" class="validate" name="nama_tps" id="nama_tps" value="{{ $data->nama_tps }}">
                                            <label for="nama_tps">Nama TPS</label>
                                            @if ($errors->has('nama_tps'))
                                                <span class="invalid-feedback" role="alert">
                                                    <span class="helper-text red-text text-darken-1" data-error="wrong" data-success="right"><i>* {{ $errors->first('nama_tps') }}</i></span>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col s12" style="padding: 0;">
                                        <div class="input-field col s12 m6">
                                            <input type="text" class="validate" name="longitude" id="longitude" value="{{ $data->longitude }}">
                                            <label for="longitude">Longitude</label>
                                            @if ($errors->has('longitude'))
                                                <span class="invalid-feedback" role="alert">
                                                    <span class="helper-text red-text text-darken-1" data-error="wrong" data-success="right"><i>* {{ $errors->first('longitude') }}</i></span>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col s12" style="padding: 0;">
                                        <div class="input-field col s12 m6">
                                            <input type="text" class="validate" name="latitude" id="latitude" value="{{ $data->latitude }}">
                                            <label for="latitude">Latitude</label>
                                            @if ($errors->has('latitude'))
                                                <span class="invalid-feedback" role="alert">
                                                    <span class="helper-text red-text text-darken-1" data-error="wrong" data-success="right"><i>* {{ $errors->first('latitude') }}</i></span>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="input-field col s12 m6">
                                        <textarea id="alamat" name="alamat" class="materialize-textarea">{{ $data->alamat }}</textarea>
                                        <label for="alamat">Alamat</label>
                                        @if ($errors->has('alamat'))
                                            <span class="invalid-feedback" role="alert">
                                                <span class="helper-text red-text text-darken-1" data-error="wrong" data-success="right"><i>* {{ $errors->first('alamat') }}</i></span>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="input-field col s12">
                                        <button type="submit" class="btn bc-blue-bg right">Simpan</button>
                                        <a href="/tps" class="btn bc-red-bg right" style="margin-right: 10px">Kembali</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection

@section('js')
<script type="text/javascript">
    $(document).ready( function () {

    });
</script>
@endsection
