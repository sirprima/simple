@extends('layouts.app')

@extends('navigation.navigation')

@section('css')
<style media="screen">
    select {
        display: block;
    }
</style>
@endsection

@section('content')
<main style="padding-left: 0px !important;">
    <div class="user">
        <div class="container">
            <div class="row">
                <div class="col s12">
                    <h5 class="left">TPS</h5>
                    <a href="/tps/tambah" class="btn bc-blue-bg btn-add right">Tambah</a>
                </div>
                <div class="col s12">
                    <div class="card">
                        <div class="card-content">
                            <table id="tps_datatable" class="display" style="width: 100%">
                                <thead>
                                    <tr>
                                        <th>No</td>
                                        <th>ID TPS</td>
                                        <th>Dapil</th>
                                        <th>ID User</th>
                                        <th>Nama</th>
                                        <th>Longitude</th>
                                        <th>Latitude</th>
                                        <th class="center" style="min-width: 200px">Alamat</th>
                                        <th class="center">Aksi</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<div id="detail" class="modal modal-fixed-footer">
        <div class="modal-content">
            <h4>Detail</h4>
            <table class="table">
                <tr>
                    <td>ID TPS</td>
                    <td>:</td>
                    <td><span class="id_tps"></span></td>
                </tr>
                <tr>
                    <td>Dapil</td>
                    <td>:</td>
                    <td><span class="dapil"></span></td>
                </tr>
                <tr>
                    <td>ID User</td>
                    <td>:</td>
                    <td><span class="id_user"></span></td>
                </tr>
                <tr>
                    <td>Nama TPS</td>
                    <td>:</td>
                    <td><span class="nama_tps"></span></td>
                </tr>
                <tr>
                    <td>Longitude</td>
                    <td>:</td>
                    <td><span class="longitude"></span></td>
                </tr>
                <tr>
                    <td>Latitude</td>
                    <td>:</td>
                    <td><span class="latitude"></span></td>
                </tr>
                <tr>
                    <td>Alamat</td>
                    <td>:</td>
                    <td><span class="alamat"></span></td>
                </tr>
            </table>
        </div>
        <div class="modal-footer">
            <button class="btn red modal-close waves-effect waves-red">Kembali</button>
        </div>
    </form>
</div>
@endsection

@section('js')
<script type="text/javascript">
    $(document).ready( function () {
        $('#tps_datatable').DataTable({
            processing: true,
            serverSide: true,
            scrollX: true,
            ajax: "{{ url('tps/getTpses') }}",
            columns: [
                        { data: 'DT_RowIndex', name: 'DT_RowIndex', className: 'center', orderable: false, searchable: false },
                        { data: 'id', name: 'id', defaultContent: '-' },
                        { data: 'dapil', name: 'dapils.nama_dapil', defaultContent: '-' },
                        { data: 'id_user', name: 'id_user', defaultContent: '-' },
                        { data: 'nama_tps', name: 'nama_tps', defaultContent: '-' },
                        { data: 'longitude', name: 'longitude', defaultContent: '-' },
                        { data: 'latitude', name: 'latitude', defaultContent: '-' },
                        { data: 'alamat', name: 'alamat', defaultContent: '-' },
                        { data: 'aksi', name: 'aksi', className: 'center', searchable: false, orderable: false, width: '200px' }
                    ]
        });

        $(document).ajaxComplete(function() {
            $('.tooltipped').tooltip();
        });

        // get & set data detail to modal
        $(document).on('click', 'a.btn-detail', function() {
            var id =($(this).attr("data-id"));
            $.ajax({
                type:"GET",
                url:"/tps/getDetail/"+id,
                success : function(data) {
                    $('.id_tps').text(data.id);
                    $('.dapil').text(data.dapil);
                    $('.id_user').text(data.id_user);
                    $('.nama_tps').text(data.nama_tps);
                    $('.longitude').text(data.longitude);
                    $('.latitude').text(data.latitude);
                    $('.alamat').text(data.alamat);
                }
            });
        });
    } );
</script>
@endsection
