@extends('layouts.app')

@extends('navigation.navigation')

@section('css')
<style media="screen">
    .center-img {
        left: 50%;
        position: relative;
        transform: translateX(-50%);
        width: 200px;
        height: auto;
    }

    .tabs .tab a {
        color: #fff;
    }

    .tabs .tab a:hover, .tabs .tab a.active {
        color: #E6E6E6;
    }

    .tabs .indicator {
        background-color: #046D93;
    }
</style>
<script type="text/javascript">
    var centreGot = false;
</script>
{!! $map['js'] !!}
@endsection

@section('content')
<ul class="sidenav sidenav-fixed" id="mobile-demo">
    <div class="row" style="margin-top: 50px;">
        <div class="col s10 offset-s1">
            <div class="row" style="margin-top: 50px;">
                <div class="col s12">
                    <ul class="tabs bc-blue-bg">
                        <li class="tab col s6"><a class="active" href="#lokasi">Lokasi</a></li>
                        <li class="tab col s6"><a href="#arah">Arah</a></li>
                    </ul>
                </div>

                <div id="lokasi" class="col s12">
                    <form id="cariTps" method="POST" action="/info-tps">
                        {{ csrf_field() }}
                        <div class="input-field">
                            <select id="provinsi" name="provinsi">
                                <option value="" disabled selected>Provinsi</option>
                                <option value="1">Sumatera Utara</option>
                            </select>
                        </div>
                        <div class="input-field">
                            <select id="kabupaten" name="kabupaten">
                                <option value="" disabled selected>Kabupaten/Kota</option>
                                <option value="1">Medan</option>
                            </select>
                        </div>
                        <div class="input-field">
                            <select id="kecamatan" name="kecamatan">
                                <option value="" disabled selected>Kecamatan</option>
                                @foreach ($kecamatans as $k)
                                    <option value="{{$k->id}}">{{$k->nama_kecamatan}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="input-field">
                            <select id="kelurahan" name="kelurahan">
                                <option value="" disabled selected>Kelurahan</option>
                            </select>
                        </div>
                        <div>
                            <button name="action" value="lokasi" type="submit" class="btn bc-blue-bg btn-cari right">Cari</button>
                        </div>
                    </form>
                </div>

                <div id="arah" class="col s12">
                    <form action="info-tps"  method="POST">
                        {{ csrf_field() }}
                        <input type="hidden" name="lat" id="lat">
                        <input type="hidden" name="long" id="long">
                        {{-- <div class="input-field">
                            <input type="text" name="lokasi_a" class="validation" id="lokasi_a">
                            <label for="lokasi_a">Lokasi Anda</label>
                        </div> --}}
                        <Label>Pindahkan marker berwarna putih untuk menentukan lokasi anda</Label>
                        <br>
                        <br>
                        <div class="input-field">
                            <select id="arahtps" name="arahtps">
                                <option value="" disabled selected>Pilih TPS</option>
                                @foreach ($tpses as $t)
                                    <option value="{{$t->id}}">{{$t->nama_tps . ', ' . $t->dapil}}</option>
                                @endforeach
                            </select>
                            <label for="arahtps">Lokasi TPS</label>
                        </div>
                        <div>
                            <button name="action" value="arah" type="submit" class="btn bc-blue-bg right">Cari</button>
                            <button name="action" value="reset" type="submit" class="btn bc-red-bg right" style="margin: 0 5px">Reset</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</ul>

<main>
    <div class="user">
        <div class="container">
            <div class="row">
                <div class="col s12">
                    <div class="card">
                        <div class="card-content card-box">
                            {!! $map['html'] !!}
                            <div id="directionsDiv"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection

@section('js')
    <script>
        $(document).ready(function(){
            $('.tabs').tabs();

            $('#kecamatan').on('change', function() {
                $('#kelurahan').children('option:not(:first)').remove();
                getKelurahans(this.value);
            });

            if ($('#kecamatan').val() != 0) {
                getKelurahans($('#kecamatan').val());
            }

            function getKelurahans(id) {
                $.ajax({
                    url: 'info-tps/getKelurahans/'+id,
                    type: 'get',
                    dataType: 'json',
                    success: function(response) {
                        var length = 0;
                        if (response['data'] != null) {
                            length = response['data'].length;
                        }
                        var data = '';
                        if (length > 0) {
                            for(i = 0; i < length; i++) {
                                data = new Option(response['data'][i].nama_kelurahan, response['data'][i].id);
                                if ($('#old-kelurahan').val() == response['data'][i].id) {
                                    data.setAttribute('selected', 'selected');
                                }
                                $("#kelurahan").append(data);
                            }
                        }
                        $("#kelurahan").formSelect();
                    }
                });
            };
        });
    </script>
@endsection
