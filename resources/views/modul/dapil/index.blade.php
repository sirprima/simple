@extends('layouts.app')

@extends('navigation.navigation')

@section('css')
<style media="screen">
    select {
        display: block;
    }
</style>
@endsection

@section('content')
<main style="padding-left: 0px !important;">
    <div class="user">
        <div class="container">
            <div class="row">
                <div class="col s12">
                    <h5 class="left">Dapil</h5>
                    <a href="/dapil/tambah" class="btn bc-blue-bg btn-add right">Tambah</a>
                </div>
                <div class="col s12">
                    <div class="card">
                        <div class="card-content">
                            <table id="dapil_datatable" class="display" style="width: 100%">
                                <thead>
                                    <tr>
                                        <th>No</td>
                                        <th>ID Dapil</td>
                                        <th>ID User</th>
                                        <th>Nama Dapil</th>
                                        <th>Provinsi</th>
                                        <th>Kabupaten/Kota</th>
                                        <th>Kecamatan</th>
                                        <th>Kelurahan</th>
                                        <th class="center" style="min-width: 200px">Aksi</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<div id="detail" class="modal modal-fixed-footer">
        <div class="modal-content">
            <h4>Detail</h4>
            <table class="table">
                <tr>
                    <td>ID Dapil</td>
                    <td>:</td>
                    <td><span class="id_dapil"></span></td>
                </tr>
                <tr>
                    <td>ID User</td>
                    <td>:</td>
                    <td><span class="id_user"></span></td>
                </tr>
                <tr>
                    <td>Nama Dapil</td>
                    <td>:</td>
                    <td><span class="nama_dapil"></span></td>
                </tr>
                <tr>
                    <td>Provinsi</td>
                    <td>:</td>
                    <td><span class="provinsi"></span></td>
                </tr>
                <tr>
                    <td>Kabupaten/Kota</td>
                    <td>:</td>
                    <td><span class="kabupaten_kota"></span></td>
                </tr>
                <tr>
                    <td>Kecamatan</td>
                    <td>:</td>
                    <td><span class="kecamatan"></span></td>
                </tr>
                <tr>
                    <td>Kelurahan</td>
                    <td>:</td>
                    <td><span class="kelurahan"></span></td>
                </tr>
            </table>
        </div>
        <div class="modal-footer">
            <button class="btn red modal-close waves-effect waves-red">Kembali</button>
        </div>
    </form>
</div>
@endsection

@section('js')
<script type="text/javascript">
    $(document).ready( function () {
        $('#dapil_datatable').DataTable({
            processing: true,
            serverSide: true,
            scrollX: true,
            ajax: "{{ url('dapil/getDapils') }}",
            columns: [
                        { data: 'DT_RowIndex', name: 'DT_RowIndex', className: 'center', orderable: false, searchable: false },
                        { data: 'id', name: 'id', defaultContent: '-' },
                        { data: 'id_user', name: 'id_user', defaultContent: '-' },
                        { data: 'nama_dapil', name: 'nama_dapil', defaultContent: '-' },
                        { data: 'provinsi', name: 'provinsi', defaultContent: '-' },
                        { data: 'kabupaten_kota', name: 'kabupaten_kota', defaultContent: '-' },
                        { data: 'nama_kecamatan', name: 'kecamatans.nama_kecamatan', defaultContent: '-' },
                        { data: 'nama_kelurahan', name: 'kelurahans.nama_kelurahan', defaultContent: '-' },
                        { data: 'aksi', name: 'aksi', className: 'center', searchable: false, orderable: false }
                    ]
        });

        $(document).ajaxComplete(function() {
            $('.tooltipped').tooltip();
        });

        // get & set data detail to modal
        $(document).on('click', 'a.btn-detail', function() {
            var id =($(this).attr("data-id"));
            $.ajax({
                type:"GET",
                url:"/dapil/getDetail/"+id,
                success : function(data) {
                    $('.id_dapil').text(data.id);
                    $('.id_user').text(data.id_user);
                    $('.nama_dapil').text(data.nama_dapil);
                    if (data.provinsi == 1) {
                        var provinsi = 'Sumatera Utara';
                    }

                    if (data.kabupaten_kota == 1) {
                        var kabupaten = 'Medan';
                    }
                    $('.provinsi').text(provinsi);
                    $('.kabupaten_kota').text(kabupaten);
                    $('.kecamatan').text(data.nama_kecamatan);
                    $('.kelurahan').text(data.nama_kelurahan);
                }
            });
        });
    } );
</script>
@endsection
