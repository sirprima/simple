@extends('layouts.app')

@extends('navigation.navigation')

@section('css')
<style media="screen">
    /* select {
        display: block;
    } */
</style>
@endsection

@section('content')
<main style="padding-left: 0px !important;">
    <div class="user">
        <div class="container">
            <div class="row">
                <div class="col s12">
                    <h5 class="left">Edit Dapil</h5>
                </div>
                <div class="col s12">
                    <div class="card">
                        <div class="card-content">
                            <form class="" action="/dapil/perbarui/{{ $data->id }}" method="post" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input type="hidden" name="old-kelurahan" id="old-kelurahan" value="{{ $data->kelurahan }}">
                                <div class="row">
                                    <div class="col s12" style="padding: 0;">
                                        <div class="input-field col s12 m6">
                                            <input type="text" class="validate" name="nama_dapil" id="nama_dapil" value="{{ $data->nama_dapil }}">
                                            <label for="nama_dapil">Nama Dapil</label>
                                            @if ($errors->has('nama_dapil'))
                                                <span class="invalid-feedback" role="alert">
                                                    <span class="helper-text red-text text-darken-1" data-error="wrong" data-success="right"><i>* {{ $errors->first('nama_dapil') }}</i></span>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col s12" style="padding: 0;">
                                        <div class="input-field col s12 m6">
                                            <select id="provinsi" name="provinsi">
                                                <option value="" disabled selected>Pilih Provinsi</option>
                                                <option value="1" {{ $data->provinsi == '1' ? 'selected' : '' }}>Sumatera Utara</option>
                                            </select>
                                            <label for="provinsi">Provinsi</label>
                                            @if ($errors->has('provinsi'))
                                                <span class="invalid-feedback" role="alert">
                                                    <span class="helper-text red-text text-darken-1" data-error="wrong" data-success="right"><i>* {{ $errors->first('provinsi') }}</i></span>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col s12" style="padding: 0;">
                                        <div class="col s12 m6" style="padding: 0;">
                                            <div class="input-field col s12">
                                                <select id="kota" name="kota">
                                                    <option value="" disabled selected>Pilih Kabupaten/Kota</option>
                                                    <option value="1" {{ $data->kabupaten_kota == '1' ? 'selected' : '' }}>Medan</option>
                                                </select>
                                                <label for="kota">Kabupaten/Kota</label>
                                                @if ($errors->has('kota'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <span class="helper-text red-text text-darken-1" data-error="wrong" data-success="right"><i>* {{ $errors->first('kota') }}</i></span>
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="input-field col s12">
                                                <select id="kecamatan" name="kecamatan">
                                                    <option value="" disabled selected>Pilih Kecamatan</option>
                                                    @foreach ($kecamatans as $kecamatan)
                                                        <option value="{{$kecamatan->id}}" {{ $data->kecamatan == $kecamatan->id ? 'selected' : '' }}>{{$kecamatan->nama_kecamatan}}</option>
                                                    @endforeach
                                                </select>
                                                <label for="kecamatan">Kecamatan</label>
                                                @if ($errors->has('kecamatan'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <span class="helper-text red-text text-darken-1" data-error="wrong" data-success="right"><i>* {{ $errors->first('kecamatan') }}</i></span>
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="input-field col s12">
                                                <select id="kelurahan" name="kelurahan">
                                                    <option value="" disabled selected>Pilih Kelurahan</option>
                                                </select>
                                                <label for="kelurahan">Kelurahan</label>
                                                @if ($errors->has('kelurahan'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <span class="helper-text red-text text-darken-1" data-error="wrong" data-success="right"><i>* {{ $errors->first('kelurahan') }}</i></span>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="input-field col s12">
                                        <button type="submit" class="btn bc-blue-bg right">Simpan</button>
                                        <a href="/dapil" class="btn bc-red-bg right" style="margin-right: 10px">Kembali</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection

@section('js')
<script type="text/javascript">
    $(document).ready( function () {

        $('#kecamatan').on('change', function() {
            $('#kelurahan').children('option:not(:first)').remove();
            getKelurahans(this.value);
        });

        if ($('#kecamatan').val() != 0) {
            getKelurahans($('#kecamatan').val());
        }

        function getKelurahans(id) {
            $.ajax({
                url: '../getKelurahans/'+id,
                type: 'get',
                dataType: 'json',
                success: function(response) {
                    var length = 0;
                    if (response['data'] != null) {
                        length = response['data'].length;
                    }
                    var data = '';
                    if (length > 0) {
                        for(i = 0; i < length; i++) {
                            data = new Option(response['data'][i].nama_kelurahan, response['data'][i].id);
                            if ($('#old-kelurahan').val() == response['data'][i].id) {
                                data.setAttribute('selected', 'selected');
                            }
                            $("#kelurahan").append(data);
                        }
                    }
                    $("#kelurahan").formSelect();
                }
            });
        };
    });
</script>
@endsection
