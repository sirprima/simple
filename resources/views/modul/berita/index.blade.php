@extends('layouts.app')

@extends('navigation.navigation')

@section('css')
<style media="screen">
    /* select {
        display: block;
    } */

    .card-image img {
        object-fit: cover;
    }
</style>
@endsection

@section('content')
<main style="padding-left: 0px !important;">
    <div class="user">
        <div class="container">
            <div class="row">
                <div class="col s12">
                    <div class="card">
                        <div class="card-content">
                            <div class="row">
                                @foreach ($data as $d)
                                <div class="col s2">
                                    <div class="card" style="max-heigt: 100px;">
                                        <div class="card-image waves-effect waves-block waves-light">
                                            <img height="210px" class="activator" src="assets/images/content/{{ $d->jenis_laporan == 'Kecurangan' ? $d->bukti_foto : $d->kk }}">
                                        </div>
                                        <div class="card-content">
                                        <p class="center"><a href="/berita/detil/{{ $d->id }}">{{ $d->jenis_laporan . ' di TPS: ' . $d->tps . ' - ' . $d->id }}</a></p>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection

@section('js')

@endsection
