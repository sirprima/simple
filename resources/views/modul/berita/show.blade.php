@extends('layouts.app')

@extends('navigation.navigation')

@section('css')
<style media="screen">
    /* select {
        display: block;
    } */
</style>
@endsection

@section('content')
<main style="padding-left: 0px !important;">
    <div class="user">
        <div class="container">
            <div class="row">
                <div class="col s12">
                    <div class="card">
                        <div class="card-content">
                            <h5>Judul: {{ $data->jenis_laporan . ' di TPS: ' . $data->tps . ' - ' . $data->id }}</h5>
                            <label><i>{{ $data->pelapor }}, {{ Carbon\Carbon::parse($data->tanggal_lapor)->format('d F Y') }}</i></label>
                            <br>
                            <br>
                            @if ($data->jenis_laporan == 'Kecurangan')
                            <video width="400" controls>
                                <source src="../../assets/images/content/{{ $data->bukti_video }}" type="video/mp4">
                                Your browser does not support HTML5 video.
                            </video>
                            @else
                            <img style="width: 250px; height: auto;" src="../../assets/images/content/{{ $data->kk }}" alt="">
                            @endif
                            <br>
                            <br>
                            <p style="width: 50%;">{{ $data->kronologi }}</p>
                            <br>
                            <br>
                            <label><i>Daftar Setuju:</i></label>
                            <ul>
                                @if ($confirm_s->toArray() == null)
                                    <label><i class="red-text">*Laporan Belum Diproses</i></label>
                                @else
                                    @foreach ($confirm_s as $s)
                                        <li>{!! $s->nama !!}</li>
                                    @endforeach
                                @endif
                            </ul>
                            <br>
                            <label><i>Daftar Ditolak:</i></label>
                            <ul>
                                @if ($confirm_s->toArray() == null)
                                    <label><i class="red-text">*Laporan Belum Diproses</i></label>
                                @else
                                    @foreach ($confirm_t as $t)
                                        <li>{!! $t->nama !!}</li>
                                    @endforeach
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection

@section('js')

@endsection
