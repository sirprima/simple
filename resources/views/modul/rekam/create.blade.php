@extends('layouts.app')

@extends('navigation.navigation')

@section('css')
<style media="screen">
    /* select {
        display: block;
    } */
</style>
@endsection

@section('content')
<main style="padding-left: 0px !important;">
    <div class="user">
        <div class="container">
            <div class="row">
                <div class="col s12">
                    <h5 class="left">Tambah Rekam Jejak</h5>
                </div>
                <div class="col s12">
                    <div class="card">
                        <div class="card-content">
                            <form class="" action="/rekam-jejak/simpan" method="post" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col s12" style="padding: 0;">
                                        <div class="input-field col s12 m6">
                                            <select id="caleg" name="caleg">
                                                <option value="" disabled selected>Pilih Caleg</option>
                                                @foreach ($caleg as $c)
                                                    <option value="{{ $c->id }}" {{ old('caleg') == $c->id ? 'selected' : '' }}>{{ $c->nama_caleg }}</option>
                                                @endforeach
                                            </select>
                                            <label for="caleg">Caleg</label>
                                            @if ($errors->has('caleg'))
                                                <span class="invalid-feedback" role="alert">
                                                    <span class="helper-text red-text text-darken-1" data-error="wrong" data-success="right"><i>* {{ $errors->first('caleg') }}</i></span>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col s12" style="padding: 0;">
                                        <div class="input-field col s6 m3">
                                            <input type="text" class="yearpicker" name="start_year" value="{{ old('start_year') }}" placeholder=" ">
                                            <label for="">Pilih Tahun Awal</label>
                                            @if ($errors->has('tahun'))
                                                <span class="invalid-feedback" role="alert">
                                                    <span class="helper-text red-text text-darken-1" data-error="wrong" data-success="right"><i>* {{ $errors->first('tahun') }}</i></span>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="input-field col s6 m3">
                                            <input type="text" class="yearpicker" name="end_year" value="{{ old('end_year') }}" placeholder=" ">

                                            <label for="end_year">Pilih Tahun Akhir</label>
                                            @if ($errors->has('end_year'))
                                                <span class="invalid-feedback" role="alert">
                                                    <span class="helper-text red-text text-darken-1" data-error="wrong" data-success="right"><i>* {{ $errors->first('end_year') }}</i></span>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="input-field col s12 m6">
                                        <textarea id="keterangan" name="keterangan" class="materialize-textarea">{{ old('keterangan') }}</textarea>
                                        <label for="keterangan">Keterangan</label>
                                        @if ($errors->has('keterangan'))
                                            <span class="invalid-feedback" role="alert">
                                                <span class="helper-text red-text text-darken-1" data-error="wrong" data-success="right"><i>* {{ $errors->first('keterangan') }}</i></span>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="input-field col s12">
                                        <button type="submit" class="btn bc-blue-bg right">Simpan</button>
                                        <a href="/rekam-jejak" class="btn bc-red-bg right" style="margin-right: 10px">Kembali</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection

@section('js')
<script type="text/javascript">
    $(document).ready( function () {
        $('.datepicker').datepicker({
            format: 'yyyy'
        });

        $('.yearpicker').yearpicker();
    });
</script>
@endsection
