@extends('layouts.app')

@extends('navigation.navigation')

@section('css')
<style media="screen">
    select {
        display: block;
    }

    /* table {
        overflow: auto hidden !important;
        display: block !important;
        width: 100% !important;
    } */
</style>
@endsection

@section('content')
<main style="padding-left: 0px !important;">
    <div class="user">
        <div class="container">
            <div class="row">
                <div class="col s12">
                    <h5 class="left">Rekam Jejak</h5>
                    <a href="/rekam-jejak/tambah" class="btn bc-blue-bg btn-add right">Tambah</a>
                </div>
                <div class="col s12">
                    <div class="card">
                        <div class="card-content">
                            <table id="rekam_datatable" class="display" style="width: 100%">
                                <thead>
                                    <tr>
                                        <th>No</td>
                                        <th>ID Rekam Jejak</td>
                                        <th>Caleg</th>
                                        <th>ID User</th>
                                        <th>Tahun</th>
                                        <th class="center" style="min-width: 200px">Keterangan</th>
                                        <th class="center" style="min-width: 200px">Aksi</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<div id="detail" class="modal modal-fixed-footer">
        <div class="modal-content">
            <h4>Detail</h4>
            <table class="table">
                <tr>
                    <td>ID Rekam Jejak</td>
                    <td>:</td>
                    <td><span class="id_rekam"></span></td>
                </tr>
                <tr>
                    <td>Caleg</td>
                    <td>:</td>
                    <td><span class="caleg"></span></td>
                </tr>
                <tr>
                    <td>ID User</td>
                    <td>:</td>
                    <td><span class="id_user"></span></td>
                </tr>
                <tr>
                    <td>Tahun</td>
                    <td>:</td>
                    <td><span class="tahun"></span></td>
                </tr>
                <span>
                    <td>Keterangan</td>
                    <td>:</td>
                    <td><span class="keterangan"></span></td>
                </tr>
            </table>
        </div>
        <div class="modal-footer">
            <button class="btn red modal-close waves-effect waves-red">Kembali</button>
        </div>
    </form>
</div>
@endsection

@section('js')
<script type="text/javascript">
    $(document).ready( function () {
        $('#rekam_datatable').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ url('rekam-jejak/getRekams') }}",
            scrollX: true,
            columns: [
                        { data: 'DT_RowIndex', name: 'DT_RowIndex', className: 'center', orderable: false, searchable: false },
                        { data: 'id', name: 'id', defaultContent: '-' },
                        { data: 'caleg', name: 'calegs.nama_caleg', defaultContent: '-' },
                        { data: 'id_user', name: 'id_user', defaultContent: '-' },
                        { data: 'tahun', name: 'tahun', defaultContent: '-' },
                        { data: 'keterangan', name: 'keterangan', defaultContent: '-' },
                        { data: 'aksi', name: 'aksi', className: 'center', searchable: false, orderable: false }
                    ]
        });

        $(document).ajaxComplete(function() {
            $('.tooltipped').tooltip();
        });

        // get & set data detail to modal
        $(document).on('click', 'a.btn-detail', function() {
            var id =($(this).attr("data-id"));
            $.ajax({
                type:"GET",
                url:"/rekam-jejak/getDetail/"+id,
                success : function(data) {
                    $('.id_rekam').text(data.id);
                    $('.caleg').text(data.caleg);
                    $('.id_user').text(data.id_user);
                    $('.tahun').text(data.tahun);
                    $('.keterangan').text(data.keterangan);
                }
            });
        });
    } );
</script>
@endsection
