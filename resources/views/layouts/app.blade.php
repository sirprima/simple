<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'SIMPLE') }} | Sistem Informasi Pileg Kota Medan</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('assets/materialize/css/materialize.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/datatables/datatables.min.css') }}">
    <link href="{{ asset('assets/yearpicker/yearpicker.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/fontawesome/css/all.css') }}" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    @yield('css')
</head>
<body>
    @yield('nav')

    @yield('content')

    @yield('footer')

    <!-- Scripts -->
    <script src="{{ asset('assets/jquery/jquery-3.3.1.min.js') }}" ></script>
    <script src="{{ asset('assets/yearpicker/yearpicker.js') }}" ></script>
    <script src="{{ asset('assets/datatables/datatables.min.js') }}" ></script>
    <script src="{{ asset('assets/nestable/jquery.nestable.js') }}" ></script>
    <script src="{{ asset('assets/materialize/js/materialize.min.js') }}" ></script>
    <script src="{{ asset('assets/fontawesome/js/all.js') }}" ></script>
    <script src="{{ asset('js/app.js') }}" ></script>
    @yield('js')
</body>
</html>
