@extends('layouts.app')

@extends('navigation.navigation')

@section('css')
<style media="screen">
    .card {
        left: 50%;
        transform: translateX(-50%);
        position: absolute;
        width: 60%;
    }
    .modal .datepicker-modal .open {
      display: inline-table !important;
    }
</style>
@endsection

@section('content')
<main style="padding-left: 0px !important;">
    <div class="user">
        <div class="container">
            <div class="card">
                <div class="card-content">
                    <form class="" action="{{ route('register') }}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col s12" style="padding: 0;">
                                <div class="input-field col s12 m6">
                                    <input type="text" name="nama_user" class="validation" id="nama_user" autofocus value="{{ old('nama_user') }}">
                                    <label for="nama_user">Nama Lengkap</label>
                                    @if ($errors->has('nama_user'))
                                        <span class="invalid-feedback" role="alert">
                                            <span class="helper-text red-text text-darken-1" data-error="wrong" data-success="right"><i>* {{ $errors->first('nama_user') }}</i></span>
                                        </span>
                                    @endif
                                </div>
                                <div class="input-field col s12 m6">
                                    <select id="agama" name="agama">
                                            <option value="" disabled selected>Pilih Agama</option>
                                            <option value="islam" {{ old('agama') == 'islam' ? 'selected' : '' }}>Islam</option>
                                            <option value="protestan" {{ old('agama') == 'protestan' ? 'selected' : '' }}>Protestan</option>
                                            <option value="katolik" {{ old('agama') == 'katolik' ? 'selected' : '' }}>Katolik</option>
                                            <option value="hindu" {{ old('agama') == 'hindu' ? 'selected' : '' }}>Hindu</option>
                                            <option value="budha" {{ old('agama') == 'budha' ? 'selected' : '' }}>Buddha</option>
                                            <option value="khonghucu" {{ old('agama') == 'khonghucu' ? 'selected' : '' }}>Khonghucu</option>
                                        </select>
                                    <label for="agama">Agama</label>
                                    @if ($errors->has('agama'))
                                        <span class="invalid-feedback" role="alert">
                                            <span class="helper-text red-text text-darken-1" data-error="wrong" data-success="right"><i>* {{ $errors->first('agama') }}</i></span>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col s12" style="padding: 0;">
                                <div class="input-field col s12 m6">
                                    <input type="text" name="tempat_lahir" class="validation" id="tempat_lahir" value="{{ old('tempat_lahir') }}">
                                    <label for="tempat_lahir">Tempat Lahir</label>
                                    @if ($errors->has('tempat_lahir'))
                                        <span class="invalid-feedback" role="alert">
                                            <span class="helper-text red-text text-darken-1" data-error="wrong" data-success="right"><i>* {{ $errors->first('tempat_lahir') }}</i></span>
                                        </span>
                                    @endif
                                </div>
                                <div class="input-field col s12 m6">
                                    <select id="status_pernikahan" name="status_pernikahan">
                                        <option value="" disabled selected>Pilih Status Pernikahan</option>
                                        <option value="lajang" {{ old('status_pernikahan') == 'lajang' ? 'selected' : '' }}>Lajang</option>
                                        <option value="menikah" {{ old('status_pernikahan') == 'menikah' ? 'selected' : '' }}>Menikah</option>
                                    </select>
                                    <label for="status_pernikahan">Status Pernikahan</label>
                                    @if ($errors->has('status_pernikahan'))
                                        <span class="invalid-feedback" role="alert">
                                            <span class="helper-text red-text text-darken-1" data-error="wrong" data-success="right"><i>* {{ $errors->first('status_pernikahan') }}</i></span>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col s12" style="padding: 0;">
                                <div class="input-field col s12 m6">
                                    <input type="text" name="tanggal_lahir" class="datepicker" id="tanggal_lahir" value="{{ old('tanggal_lahir') }}">
                                    <label for="tanggal_lahir">Tanggal Lahir</label>
                                    @if ($errors->has('tanggal_lahir'))
                                        <span class="invalid-feedback" role="alert">
                                            <span class="helper-text red-text text-darken-1" data-error="wrong" data-success="right"><i>* {{ $errors->first('tanggal_lahir') }}</i></span>
                                        </span>
                                    @endif
                                </div>
                                <div class="input-field col s12 m6">
                                    <input type="text" name="pekerjaan" class="validation" id="pekerjaan" value="{{ old('pekerjaan') }}">
                                    <label for="pekerjaan">Pekerjaan</label>
                                    @if ($errors->has('pekerjaan'))
                                        <span class="invalid-feedback" role="alert">
                                            <span class="helper-text red-text text-darken-1" data-error="wrong" data-success="right"><i>* {{ $errors->first('pekerjaan') }}</i></span>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col s12" style="padding: 0;">
                                <div class="input-field col s12 m6">
                                    <select id="jenis_kelamin" name="jenis_kelamin">
                                        <option value="" disabled selected>Pilih Jenis Kelamin</option>
                                        <option value="laki-laki" {{ old('jenis_kelamin') == 'laki-laki' ? 'selected' : '' }}>Laki - Laki</option>
                                        <option value="perempuan" {{ old('jenis_kelamin') == 'perempuan' ? 'selected' : '' }}>Perempuan</option>
                                    </select>
                                    <label for="jenis_kelamin">Jenis Kelamin</label>
                                    @if ($errors->has('jenis_kelamin'))
                                        <span class="invalid-feedback" role="alert">
                                            <span class="helper-text red-text text-darken-1" data-error="wrong" data-success="right"><i>* {{ $errors->first('jenis_kelamin') }}</i></span>
                                        </span>
                                    @endif
                                </div>
                                <div class="file-field input-field col s12 m6">
                                    <div class="btn bc-blue-bg">
                                        <span>Upload</span>
                                        <input type="file" name="ktp">
                                    </div>
                                    <div class="file-path-wrapper">
                                        <input class="file-path validate" type="text" placeholder="Upload KTP">
                                    </div>
                                    @if ($errors->has('ktp'))
                                        <span class="invalid-feedback" role="alert">
                                            <span class="helper-text red-text text-darken-1" data-error="wrong" data-success="right"><i>* {{ $errors->first('ktp') }}</i></span>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="input-field col s12">
                                <textarea id="alamat" name="alamat" class="materialize-textarea">{{ old('alamat') }}</textarea>
                                <label for="alamat">Alamat</label>
                                @if ($errors->has('alamat'))
                                    <span class="invalid-feedback" role="alert">
                                        <span class="helper-text red-text text-darken-1" data-error="wrong" data-success="right"><i>* {{ $errors->first('alamat') }}</i></span>
                                    </span>
                                @endif
                            </div>
                            <div class="col s12" style="padding: 0;">
                                <div class="input-field col s12 m6">
                                    <input type="email" name="email" class="validation" id="email" value="{{ old('email') }}">
                                    <label for="email">Email</label>
                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                            <span class="helper-text red-text text-darken-1" data-error="wrong" data-success="right"><i>* {{ $errors->first('email') }}</i></span>
                                        </span>
                                    @endif
                                </div>
                                <div class="input-field col s12 m6">
                                    <input type="text" name="username" class="validation" id="username" readonly value="{{ old('username') }}">
                                    <label for="username">Username</label>
                                    @if ($errors->has('username'))
                                        <span class="invalid-feedback" role="alert">
                                            <span class="helper-text red-text text-darken-1" data-error="wrong" data-success="right"><i>* {{ $errors->first('username') }}</i></span>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col s12" style="padding: 0;">
                                <div class="input-field col s12 m6">
                                    <input type="password" name="password" class="validation" id="password">
                                    <label for="password">Password</label>
                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                            <span class="helper-text red-text text-darken-1" data-error="wrong" data-success="right"><i>* {{ $errors->first('password') }}</i></span>
                                        </span>
                                    @endif
                                </div>
                                <div class="input-field col s12 m6">
                                    <input type="password" name="password_confirmation" class="validation" id="password_confirmation">
                                    <label for="password_confirmation">Ulangi Password</label>
                                    @if ($errors->has('password_confirmation'))
                                        <span class="invalid-feedback" role="alert">
                                            <span class="helper-text red-text text-darken-1" data-error="wrong" data-success="right"><i>* {{ $errors->first('password_confirmation') }}</i></span>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="input-field">
                                <button type="submit" class="btn bc-blue-bg right">Daftar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection

@section('js')
    <script>
        $(document).ready(function(){
            $('#email').focusout(function() {
                var val = this.value;
                val = val.substr(0, val.indexOf('@'));
                $('#username').val(val);
            });
        });
    </script>
@endsection
