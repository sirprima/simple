@extends('layouts.app')

@extends('navigation.navigation')

@section('css')
<style media="screen">
    .card {
        left: 50%;
        transform: translateX(-50%) translateY(-50%);
        position: absolute;
        width: 400px;
        top: 50%;
        height: 250px;
    }
</style>
@endsection

@section('content')
<main style="padding-left: 0px !important;">
    <div class="user">
        <div class="container">
            <div class="card">
                <div class="card-content">
                    @if(session()->has('success'))
                        <div id="message" class="light-blue darken-1 white-text" style="padding: 15px;">
                            <span id="close" class="badge white-text" style="cursor:pointer;"><i class="material-icons">clear</i></span>{{ session()->get('success') }}
                        </div>
                    @endif
                    <form action="{{ route('login') }}"  method="POST">
                        {{ csrf_field() }}
                        <div class="input-field">
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" style="color: red;">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                            <input type="text" name="email" class="validation {{ $errors->has('email') ? ' is-invalid' : '' }}" id="email" required autofocus>
                            <label for="email">Username / Email</label>
                        </div>
                        <div class="input-field">
                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" style="color: red;">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            <input type="password" name="password" class="validation {{ $errors->has('password') ? ' is-invalid' : '' }}" id="password" required>
                            <label for="password">Kata Sandi</label>
                            <span class="helper-text" data-error="wrong" data-success="right">Anda belum memiliki akun? Klik disini untuk <a href="/register">Daftar</a></span>
                        </div>
                        <div class="input-field">
                            <button type="submit" class="btn bc-blue-bg right">Login</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection

@section('js')
    <script>
        $(document).ready(function(){
            $('#close').on('click', function() {
                $('#message').hide();
            });
        });
    </script>
@endsection
