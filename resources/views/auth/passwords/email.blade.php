@extends('layouts.app')

@section('css')
<style media="screen">
    body {
        overflow: hidden;
    }
</style>
@endsection

@section('content')
<div class="login">
    <div class="row">
        <div class="col m8 backdrop-img hide-on-small-only">
            <img src="{{ asset('assets/images/template/login-backdrop.jpg') }}" alt="">
        </div>
        <div class="col s12 m4 white" style="height: 100vh;">
            <div class="card">
                <div class="card-content">
                    <span class="card-title bc-blue"><h5>{{ __('Perbaharui Kata Sandi') }}</h5></span>
                    <br>
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf

                        <div class="input-field">
                            <label for="email">{{ __('Alamat Email') }}</label>

                            <input id="email" type="email" class="{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="input-field">
                            <button type="submit" class="btn">
                                {{ __('Kirim') }}
                            </button>
                            <a href="/login" class="btn red left" style="margin-right: 10px;" name="button">Kembali</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
